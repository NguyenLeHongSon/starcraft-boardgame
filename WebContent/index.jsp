<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Starcraft Boardgame home</title>
	<link type="text/css" rel="stylesheet" href="<c:url value="/inc/form.css"/>" />
</head>

<body>
<c:import url="/inc/menu.jsp" />
<div class="title">Welcome to the Starcraft board game home page</div>

<ul class="tab">
	<li><a id="websiteLink" class="tablinks" onclick="openTab('websiteLink', 'website', 'tabcontent')">About this website</a></li>
	<li><a id="manualLink" class="tablinks" onclick="openTab('manualLink', 'manual', 'tabcontent');
	openTab('gameStartLink', 'gameStart', 'tabcontent2')">How to play</a></li>
	<li><a id="contactLink" class="tablinks" onclick="openTab('contactLink', 'contact', 'tabcontent')">Contact</a></li>
</ul>

<div id="website" class="tabcontent">
<h3>What can you do on this web site?</h3>
<div class="paragraph">On this website, you can play the Starcraft: Brood War board game online.<br/>
This game is composed of two parts :<br/>
&emsp;- the Starcraft board game<br/>
&emsp;- its extension Brood war<br/>
This board game is a strategy game inspired by the Starcraft video game.<br/>
The rules for the base game can be found here : 
<a href="https://images-cdn.fantasyflightgames.com/ffg_content/StarCraft/StarCraft_rules.pdf">Starcraft rules</a><br/>
The rules modification brought by the Brood war extension can be found here : 
<a href="https://images-cdn.fantasyflightgames.com/ffg_content/StarCraft/sc_bw_rules.pdf">Brood War rules</a><br/>
A summary of those rules can be found in the 
<a class="hrefStyle" onclick="openTab('manualLink', 'manual', 'tabcontent');
openTab('gameStartLink', 'gameStart', 'tabcontent2')">How to play</a> tab.<br/></div>

<h3>Why did I make this web site?</h3>
<div class="paragraph">
The game itself has complex rules that can be hard to follow.<br/>
Putting it online increase its accessibility while preserving the game's complexity.<br/>
As an example, the software won't allow you to make illegal actions so you don't have to keep most of the rules in mind.<br/>
Moreover, a number of steps are handled by the software so that the players don't have to worry about them.<br/>
Lastly, as the board game itself is out of print, the easiest way for new players to play it
would be through this web site.<br/>
</div>

<h3>What is the current state of the web site?</h3>
<div class="paragraph">Currently, this web site is a work in progress.<br/>
The game itself is playable but a number of rules and actions have yet to be programmed.<br/>
Details on what is currently working are in the 
<a class="hrefStyle" onclick="openTab('manualLink', 'manual', 'tabcontent')">How to play</a> tab.<br/>
The interface also needs some improvements.<br/>
</div>


<h3>How was this web site made?</h3>
<div class="paragraph">
To create this web site, I used :<br/>
&emsp;- J2EE/Java for the model and controller<br/>
&emsp;- javascript and the easeljs library for the graphical interface<br/>
&emsp;- mysql for the database<br/>
&emsp;- BoneCP and mysql connector to connect the database to the program<br/>
</div>
</div>

<!-- how to play tab -->
<div id="manual" class="tabcontent">

<ul class="tab">
	<li><a id="gameStartLink" class="tablinks" onclick="openTab('gameStartLink', 'gameStart', 'tabcontent2')">
	Beginning a game</a></li>
	<li><a id="gameplayLink" class="tablinks" onclick="openTab('gameplayLink', 'gameplay', 'tabcontent2')">
	Playing a game</a></li>
</ul>


<div id="gameStart" class="tabcontent2">
<h3>Starting a game</h3>
<div class="paragraph">&emsp;In order to play the game,
 you must first register a new account or log in to an existing account.<br/>
Afterwards, you will be redirected to a new page that will allow you to either create new games or join existing ones.<br/>
If you choose to create a new game, you may start whenever you wish to.<br/>
If you join an existing game, you must wait for the host to start a game.<br/>
While it is currently possible to start a game alone to test the game interface, 
the game is meant to be played in multiplayer.<br/>
If you wish to test all the possible actions in the game, you would need to create two accounts and to connect to both
 on different browsers.<br/>
Once the game has started, a help menu on the right side of the browser will remind you of what you can do
 at each step of the game.</div>
   
<h3>Choosing a faction</h3>
<div class="paragraph">
&emsp;Before the beginning of the game, the order of play is randomly determined by the program.<br/>
At the beginning of the game, the players must choose their starting faction and species.<br/>
There are two factions to choose from for each species and three different species :<br/>
&emsp;- the Zergs are an aggressive species that are strong at the beginning of the game and with ground units.<br/>
&emsp;- the Protoss are a defensive species that are strong at the end of the game and with flying units.<br/>
&emsp;- the Terrans are a versatile species that are moderately strong at all stages of the game with all units.<br/><br/>
&emsp;Afterwards, each player must choose a leadership card during their turn. That card determines their starting units
 and bonuses.<br/>
<div class="imageContainer">
<a class="legend">A player is choosing his faction</a>
<img src="starcraftWebResources/manualImages/chooseFaction.png" height="500" width="600">
</div>

</div>

<h3>Placing the planets</h3>
<div class="paragraph">
&emsp;During this stage of the game, the players will collectively create the game board by placing two planets each.<br/>
Players will take a turn placing a single planet according to the order of play.
 They may choose to place their base on the planet they just placed.<br/>
The placement of the base determines the starting planet of the player.<br/>
Afterward, players will take a turn placing a second planet in the opposite order.
 The players who haven't placed a base yet<br/>
must place their base on the second planet.<br/>
A base also allows you the use of the resources the planet is on. Currently, only mineral areas and gas areas give
 any benefits<br/>
When placing a planet, the first is placed at the center and the others must be connected to an existing planet.<br/>
The valid placements will be shown by a green square.<br/>
</div>

<h3>Connecting the planets</h3>
<div class="paragraph">
&emsp;During this stage, players will place additional links between planets.<br/>
They may do so by selecting two unconnected roads and clicking 'next step'.
<div class="imageContainer">
<a class="legend">A galaxy after the links are placed.</a>
<img src="starcraftWebResources/manualImages/placedRoads.png" height="500" width="500">
</div>
This map shows "Tarsonis" connected to "Paladino Belt" through the purple link and "Paladino Belt" connected to "Vyctor V"<br>
through the green links.
</div>

<h3>Placing the starting units</h3>
<div class="paragraph">
&emsp;This stage is the last setting up stage before players will be able to interact with each others.<br/>
Each player will take a turn placing his units on the planet where they placed their base.<br/>
All units must be placed before finishing a turn.<br/>
Transports must me placed on the roads leading to your planet and the other units must me placed on areas from your planet.<br/>
<div class="imageContainer">
<a class="legend">A galaxy after both sides have placed their units.</a>
<img src="starcraftWebResources/manualImages/placedUnits.png" height="500" width="500">
</div>
Transports allow a player to move units from one of the linked planet to the other.<br/>
In this example, the purple player can move from "Tarsonis" to "Paladino Belt".
 The blue player can move from "Chau Sara" to "Tarsonis".<br/>
If the players wish to move to the other linked planets, they will need to build more transports beforehand.<br/>
<a class="hrefStyle" onclick="openTab('gameplayLink', 'gameplay', 'tabcontent2')">Next page</a><br/>

</div>
</div>

<!-- gameplay tab -->
<div id="gameplay" class="tabcontent2">
<h3>Summary of a round</h3>
<div class="paragraph">
&emsp;The game is played over a series of game rounds divided into three phases : planning, execution and regrouping.<br/>
During the Planning Phase, players take turns placing order tokens facedown on planets. Players can choose among<br/>
mobilize, build, and research orders. Once each player has placed a total of four orders, play proceeds to
 the execution phase.<br/>
During the execution phase, the players take turns choosing and executing their placed orders.<br/>
During the regrouping phase, players must do two things : play event cards and dismiss their combat cards if they have cards<br/>
above their hand limit (6 for Zergs and Protoss, 8 for Terrans). The event cards haven't been added to the online game yet so<br/>
currently, players only need to dismiss their cards. The other steps described in the original game are automatically taken care
<br/>of by the software.<br/>
</div>

<h3>The planning phase</h3>
<div class="paragraph">
&emsp;During this phase, players will place the orders on planets where they will be executed during the next phase.<br/>
<div class="imageContainer">
<a class="legend">Available orders.</a>
<img src="starcraftWebResources/manualImages/allOrders.png" height="300" width="300">
</div>
The crosses are mobilize orders, the squares are build orders and the circles are research orders.<br/>
The yellow orders are special orders but haven't implemented yet. They're currently working as normal orders.<br/>
The following rules apply to the placement of orders :<br/>
&emsp;- they can only be placed on planets where you have units or neighboring planets.<br/>
&emsp;- when placing an order on a planet already containing an order, the new order is placed on top of the previous order<br/>
 making a stack of order.<br/>
</div>

<h3>The execution phase</h3>
<div class="paragraph">
&emsp;During this phase, players will execute the orders they placed during the previous phase.<br/>
The following rules apply to the execution of orders :<br/>
&emsp;- you can only execute the orders at the top of the stacks (which are the only visible orders).<br/>
&emsp;- if you try to execute an order that doesn't allow you to do anything, the order will be canceled and removed from<br/>
the order stack (example : you try to build on a planet where you have no units or base).<br/>
&emsp;- if you have at least one visible order, you must try to execute one of them even if it leads to you canceling it.<br/>
<div class="imageContainer">
<a class="legend">example : it is purple's turn to execute an order.</a>
<img src="starcraftWebResources/manualImages/placedOrders.png" height="500" width="500">
</div>
In this example, the purple player can only execute the build order on "Tarsonis" or the mobilize order on "Vyctor V".<br/>
Moreover, if the purple player tries to use the mobilize order on "Vyctor V", that order will be canceled and removed from the<br/>
as he has no transport units between "Tarsonis" and "Vyctor V". He would therefore need to build a transport first.<br/>
The two orders he placed but aren't currently visible can't be executed until the orders on top of them are executed.<br/>
</div>

<h3>The build order</h3>
<div class="paragraph">
&emsp;Executing this order allows a player to build new units, transports, workers, buildings or bases on the planet where the 
order is.<br/>
Units are used to defend or attack.<br/>
Transports are used to from a planet to another.<br/>
Buildings are used to unlock new type of units.<br/>
Bases will get you the same benefits than your starting base.<br/>
Workers are used to fetch resources. The workers can't be used the same round they are built and will be shown as unavailable<br/>
Executing a build order is done in three steps :<br/>
&emsp;- build units, workers or transports. It requires a base on the planet.<br/>
&emsp;- build a single building. It requires a base or unit on the planet.<br/>
&emsp;- build a single base. It requires a unit on the planet and can only be done if you have no other bases on it.<br/>
Using the previous example, if the purple player execute a build order on "Tarsonis", he may only build buildings, units,<br/>
workers or transports on "Tarsonis"<br/>
When you buy something, you must send workers on the required resources that are available to you.<br/>
The purple player may use the mineral or gas resource on "Tarsonis" or the resources he can find on the "base resources" area<br/>
of his game board.<br/>
<div class="imageContainer">
<a class="legend">purple's player game board</a>
<img src="starcraftWebResources/manualImages/playerBoard.png" height="250" width="800">
</div>
</div>

<h3>The research order</h3>
<div class="paragraph">
&emsp;Executing this order allows a player to buy a new card.<br/>
This order requires a base on the planet where it is executed.<br/>
When you use this order, you will draw three cards then you will be able buy a new card.<br/>
There are two types of cards :<br/>
&emsp;- combat cards, those are added to your deck and be drawn and played later on.<br/>
&emsp;- cards that give global bonuses.<br/>
The text will tell which kind of card it is.<br/>
</div>

<h3>The mobilize order</h3>
<div class="paragraph">
&emsp;The mobilize order allows a player to move units on the planet where the order is from the same planet or neighboring
 planets.<br/>
To move units from a planet to another planet, a player requires one of his transport on a link between the two planets.<br/>
To start a battle with an enemy, a player must move at least one unit on a hostile area (containing an enemy base or/and unit).
<br/>
A player may only start one battle per mobilize order.<br/>
</div>

<h3>The battle</h3>
<div class="paragraph">
&emsp;When a battle start, a new interface appears.<br/>
<div class="imageContainer">
<a class="legend">the battlefield at the beginning</a>
<img src="starcraftWebResources/manualImages/battleField.png" height="300" width="900">
</div>
The attacking side is the orange side and the other side is the defender's side.<br/>
The squares where the two sides are meeting represent the front line. The other squares are where support units are placed.<br/>
The attacking player decides how all front line squares are filled and the supporting squares(if there are any) are filled<br/>
by their respective players. The front line must be completely filled.<br/>
Each row represent a skirmish and the battle is resolved per skirmishes.<br/>
Once all units are placed, the players will choose cards from their hands starting with the attacker.<br/>
A player may use two types of cards :<br/>
&emsp;- combat cards that have numerical values on top of those cards.<br/>
&emsp;- reinforcement cards whose effects are described in its text.<br/>
A combat card has 4 numbers. From left to right, those number indicate :<br/>
&emsp;- maximum attack.<br/>
&emsp;- minimum attack.<br/>
&emsp;- minimum health.<br/>
&emsp;- maximum health.<br/>
A combat card also has unit pictures. If card used has a picture of the unit used on the front lane, the player will benefit<br/>
from its maximal values, otherwise he will use its minimal values.<br/>
A unit is destroyed when its enemy has at least as much attack as the unit has health.<br/>
At the end of the battle, an attacker wins if he killed all the enemy units. After the battle, the defeated player must move<br/>
his units away from the attacked area if he has any left.<br/>
There are more detailed rules in the official manuals but they are handled by the software without the need for players to know
 them.<br/>
 <div class="imageContainer">
<a class="legend">the battlefield after all cards and units are placed</a>
<img src="starcraftWebResources/manualImages/battleFieldEnd.png" height="300" width="750">
</div>
In this example, the blue player has 7 attack and 8 health for his first skirmish and 6 attack and 6 health for his second
 skirmish.<br/>
He can't see the cards his enemy played until the enemy confirms his choices and the battle get resolved.<br/>
</div>
</div>
</div>
<!--contact tab -->
<div id="contact" class="tabcontent">
  <h3>Contact informations :</h3>
  <div class="paragraph">Email : son15031986@gmail.com.<br/>
  Phone number : 06 01 95 88 02</div>
</div>

</body>

<script src="<c:url value='/js/tabScript.js'/>"></script>
<script>
openTab('websiteLink', 'website', 'tabcontent');
</script>
</html>