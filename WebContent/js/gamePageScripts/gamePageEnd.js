/**Récupération des évènements et appel des fonctions**/
webSocket.onmessage = function(event){
    var serverAction = JSON.parse(event.data);
    if (serverAction.action === "sendChat") {
    	writeResponse(serverAction);
    } else {
    	var fn = window[serverAction.action];
    	if(typeof fn === 'function') {
    	    fn(serverAction);
    	}else{
    		console.log(serverAction.action);
    	}
    }
};

webSocket.onclose = function(event){
};

window.onload = function(e){ 
	var gameBoardHeight = actualHeight - 16;
    document.getElementById("gameBoard").style.height = gameBoardHeight+"px";
}