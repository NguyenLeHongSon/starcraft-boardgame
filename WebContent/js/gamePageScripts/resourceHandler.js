/*actions du joueur*/
function useFreeMineral(){
	removePriceDisplay();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "useCostReduction",
			resource : "mineral"
	};
	webSocket.send(JSON.stringify(clientAction));
}

function useFreeGas(){
	removePriceDisplay();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "useCostReduction",
			resource : "gas"
	};
	webSocket.send(JSON.stringify(clientAction));
}

function endRestorationTurn(){
	clearEventAndHighlight();
	var clientAction = {
			action : "endRestorationTurn"
	};
	webSocket.send(JSON.stringify(clientAction));
}

function sendRestoreArea(area){
	clearEventAndHighlight();
	var clientAction = {
			action : "sendRestoreArea",
			areaId : area.name.toString(),
			coordinates : area.parent.parent.parent.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function sendExhaustArea(area){
	clearEventAndHighlight();
	var clientAction = {
			action : "sendExhaustArea",
			areaId : area.name.toString(),
			coordinates : area.parent.parent.parent.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function addRestoreAreaEvent(area){
	addObjectWithEvents(area);
	if (currentTurn &&  !area.hasEventListener("click")){
		area.addEventListener("mouseover", function(event) {
			area.alpha = 0.5;
			galaxyStage.update(event);
		})

		area.addEventListener("mouseout", function(event) { 
			area.alpha = 1;
			galaxyStage.update(event);
		})

		area.addEventListener("click", function(event) { 
			sendRestoreArea(area);
			area.alpha = 1;
		})
	}
}

function addExhaustAreaEvent(area){
	addObjectWithEvents(area);
	if (currentTurn &&  !area.hasEventListener("click")){
		area.addEventListener("mouseover", function(event) {
			area.alpha = 0.5;
			galaxyStage.update(event);
		})

		area.addEventListener("mouseout", function(event) { 
			area.alpha = 1;
			galaxyStage.update(event);
		})

		area.addEventListener("click", function(event) { 
			sendExhaustArea(area);
			area.alpha = 1;
		})
	}
}

/*réception des messages du serveur*/
/**ajoute les boutons correspondant aux actions possibles pendant cette étape**/
function addEndRestorationTurnButton(serverAction){
	//le nombre d'unité placées dans la barre d'action
	placedUnitChoice = 0;

	addUnitToPoolEvent(stage);
	
	var actionMenu = document.getElementById("buttonContainer");
	
	//bouton de fin de tour
	var endTurnButton =  document.createElement("button");
	endTurnButton.setAttribute("id", "endTurnButton");
	endTurnButton.setAttribute("class", "gameButton");
	endTurnButton.setAttribute("type", "button");
	endTurnButton.setAttribute("onclick", "this.disabled=true;endRestorationTurn();");
	endTurnButton.innerHTML = "Next step";
	if (!currentTurn){
		endTurnButton.disabled = true;
	}
	actionMenu.appendChild(endTurnButton);
}

function addRestoreAreasEvent(serverAction){
	var coordinates = serverAction.coordinates;
	for (var i in coordinates){
		var area = galaxyStage.getChildByName(coordinates[i].coordinate)
		.getChildAt(1).getChildByName("areaSet").getChildByName(coordinates[i].areaId);
		addRestoreAreaEvent(area);
	}
}

function addExhaustAreasEvent(serverAction){
	var coordinates = serverAction.coordinates;
	for (var i in coordinates){
		var area = galaxyStage.getChildByName(coordinates[i].coordinate)
		.getChildAt(1).getChildByName("areaSet").getChildByName(coordinates[i].areaId);
		addExhaustAreaEvent(area);
	}
}

function updateExhaustionDisplay(serverAction){
	var currentPlanet = getPlanetAtCoordinates(serverAction.coordinates);
	var exhaustionImage = currentPlanet.getChildByName("exhaustion" + serverAction.areaId);
	if (serverAction.exhaustion === 0){
		exhaustionImage.alpha = 0;
	}else if (serverAction.exhaustion === 1){
		exhaustionImage.graphics.clear();
		exhaustionImage.graphics.setStrokeStyle(3).beginStroke("yellow").drawCircle(0, 0, 23);
		exhaustionImage.alpha = 1;
	}else if (serverAction.exhaustion === 2){
		exhaustionImage.graphics.clear();
		exhaustionImage.graphics.setStrokeStyle(3).beginStroke("red").drawCircle(0, 0, 23);
		exhaustionImage.alpha = 1;
	}
	galaxyStage.update();
}

function activateCostReduction(serverAction){
	var costReductionButton = document.getElementById("costReductionButton");
	if (costReductionButton){
		costReductionButton.disabled = false;
		
		var costReductionMineral = document.getElementById("costReductionMineral");
		if (serverAction.activateMineral){
			costReductionMineral.style.display = "inline"
		}else{
			costReductionMineral.style.display = "none"
		}
		var costReductionGas = document.getElementById("costReductionGas");
		if (serverAction.activateGas){
			costReductionGas.style.display = "inline"
		}else{
			costReductionGas.style.display = "none"
		}
	}
}

function removeCostReduction(serverAction){
	var costReductionContainer = document.getElementById("costReductionContainer");
	if (costReductionContainer){
		costReductionContainer.parentNode.removeChild(costReductionContainer);
	}
}

function desactivateCostReduction(serverAction){
	var costReductionButton = document.getElementById("costReductionButton");
	if (costReductionButton){
		costReductionButton.disabled = true;
		var dropDownResources = document.getElementById("dropDownResources");
		dropDownResources.style.display = "none";
	}
}

function displayCostReduction(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	if (!document.getElementById("costReductionContainer")){
		var costReductionContainer =  document.createElement("div");
		costReductionContainer.setAttribute("id", "costReductionContainer");
		costReductionContainer.setAttribute("class", "menuItem");
		
		//bouton de réduction de coût
		var costReductionButton =  document.createElement("button");
		costReductionButton.setAttribute("id", "costReductionButton");
		costReductionButton.setAttribute("class", "gameButton");
		costReductionButton.setAttribute("type", "button");
		costReductionButton.setAttribute("onclick", "displayResourceChoice();");
		costReductionButton.innerHTML = "Use free resource";
		
		costReductionButton.disabled = true;
		
		var dropDownResources =  document.createElement("div");
		dropDownResources.setAttribute("id", "dropDownResources");
		dropDownResources.setAttribute("class", "dropDownResources");
		
		var mineralImage = document.createElement("img");
		mineralImage.setAttribute("id", "costReductionMineral");
		mineralImage.setAttribute("onclick", "useFreeMineral();");
		mineralImage.setAttribute("src", "../starcraftWebResources/mapResources/mineral.png");
		mineralImage.setAttribute("width", 40);
		mineralImage.setAttribute("height", 40);
		
		var gasImage = document.createElement("img");
		gasImage.setAttribute("id", "costReductionGas");
		gasImage.setAttribute("onclick", "useFreeGas();");
		gasImage.setAttribute("src", "../starcraftWebResources/mapResources/gas.png");
		gasImage.setAttribute("width", 40);
		gasImage.setAttribute("height", 40);
		
		dropDownResources.appendChild(mineralImage);
		dropDownResources.appendChild(gasImage);
		
		costReductionContainer.appendChild(costReductionButton);
		costReductionContainer.appendChild(dropDownResources);
		dropDownResources.style.display = "none";
		
		actionMenu.appendChild(costReductionContainer);
	}
}

function drawWorkersOnBaseResources(serverAction, playerName){
	var stingSuffix = "";
	if (playerName !== ""){
		stingSuffix = "_" + playerName;
	}
	var baseMineral = document.getElementById("baseMineral"+stingSuffix);
	
	var workerOnBaseMineral = document.getElementById("workerOnBaseMineral"+stingSuffix);
	if (!(serverAction.image in unitImages)){
		var image = new Image();
	    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
	    unitImages[serverAction.image]=image;
	}
	
	if (serverAction.workerOnMineral > 0){
		if (!workerOnBaseMineral){
			var workerOnBaseMineral = document.createElement("div");
			workerOnBaseMineral.setAttribute("id", "workerOnBaseMineral"+stingSuffix);
			
			var workerOnBaseMineralText = document.createElement("div");
			workerOnBaseMineralText.setAttribute("id", "workerOnBaseMineralText"+stingSuffix);
			workerOnBaseMineralText.innerHTML = serverAction.workerOnMineral;
			workerOnBaseMineral.appendChild(workerOnBaseMineralText);
			
			var workerOnBaseMineralImage = document.createElement("div");
			cloneImage(workerOnBaseMineralImage, unitImages[serverAction.image]);
			workerOnBaseMineral.appendChild(workerOnBaseMineralImage);
			
			baseMineral.appendChild(workerOnBaseMineral);
		}else{
			var workerOnBaseMineralText = document.getElementById("workerOnBaseMineralText"+stingSuffix);
			workerOnBaseMineralText.innerHTML = serverAction.workerOnMineral;
		}
	}else{
		if (workerOnBaseMineral){
			workerOnBaseMineral.parentElement.removeChild(workerOnBaseMineral);
		}
	}
	
	var baseGas = document.getElementById("baseGas"+stingSuffix);
	var workerOnBaseGas = document.getElementById("workerOnBaseGas"+stingSuffix);
	if (serverAction.workerOnGas > 0){
		if (!workerOnBaseGas){
			var workerOnBaseGas = document.createElement("div");
			workerOnBaseGas.setAttribute("id", "workerOnBaseGas"+stingSuffix);
			
			var workerOnBaseGasText = document.createElement("div");
			workerOnBaseGasText.setAttribute("id", "workerOnBaseGasText"+stingSuffix);
			workerOnBaseGasText.innerHTML = serverAction.workerOnGas;
			workerOnBaseGas.appendChild(workerOnBaseGasText);
			
			var workerOnBaseGasImage = document.createElement("div");
			cloneImage(workerOnBaseGasImage, unitImages[serverAction.image]);
			workerOnBaseGas.appendChild(workerOnBaseGasImage);
			
			baseGas.appendChild(workerOnBaseGas);
		}else{
			var workerOnBaseGasText = document.getElementById("workerOnBaseGasText"+stingSuffix);
			workerOnBaseGasText.innerHTML = serverAction.workerOnGas;
		}
	}else{
		if (workerOnBaseGas){
			workerOnBaseGas.parentElement.removeChild(workerOnBaseGas);
		}
	}
}

function displayWorkersOnBaseResources(serverAction){
	drawWorkersOnBaseResources(serverAction, "");
}

function displayWorkersOnBaseResources2(serverAction){
	drawWorkersOnBaseResources(serverAction, serverAction.playerName);
}

/**montre le nombre de travailleurs occupant une zone**/
function displayWorkersOnArea(serverAction){
	var workerImageName = "worker." +serverAction.coordinate + "." + serverAction.areaId.toString();
	var workerImage = galaxyStage.getChildByName("unitContainer").getChildByName(workerImageName);
	if (serverAction.number > 0){
		if (!workerImage){
			workerImage = drawWorkerWithText(serverAction);
			workerImage.set({name : workerImageName});
			var square = galaxyStage.getChildByName(serverAction.coordinate);
			var areaCount = square.getChildAt(1).getChildByName("areaSet").numChildren;
			var angle1 = (Math.PI*2/areaCount) * (serverAction.areaId-1);
			var angle2 = (Math.PI*2/areaCount) * serverAction.areaId;
			
			workerImage.x = square.x + 200 + 180 *  Math.cos((2 * angle1 + angle2)/3);
			workerImage.y = square.y + 200 + 180 *  Math.sin((2 * angle1 + angle2)/3);
			galaxyStage.getChildByName("unitContainer").addChild(workerImage);
		}else{
			workerImage.getChildByName("textWorker").text = serverAction.number;
			workerImage.getChildByName("textWorker2").text = serverAction.number;
		}
	}else{
		if (workerImage){
			galaxyStage.getChildByName("unitContainer").removeChild(workerImage);
		}
	}
	galaxyStage.update();
}


function updateBaseWorkerDisplay(serverAction){
	var worker1 = document.getElementById("availableWorkers");
	worker1.innerHTML = "Available workers : " + serverAction.availableWorkers;
	var worker2 = document.getElementById("unavailableWorkers");
	worker2.innerHTML = "Unavailable workers : " + serverAction.unavailableWorkers;
}

function updateBaseWorkerDisplay2(serverAction){
	var worker1 = document.getElementById("availableWorkers_" + serverAction.playerName);
	worker1.innerHTML = "Available workers : " + serverAction.availableWorkers;
	var worker2 = document.getElementById("unavailableWorkers_" + serverAction.playerName);
	worker2.innerHTML = "Unavailable workers : " + serverAction.unavailableWorkers;
}

function drawBaseWorkers(serverAction, availableWorkersId, unavailableWorkers){
	var resourceMenu = document.createElement("resourceMenu");
	resourceMenu.setAttribute("class", "playerInfoBoard");
	
	var resourceMenu1 = document.createElement("resourceMenu1");
	resourceMenu1.innerHTML = "Workers";
	
	var resourceMenu2 = document.createElement("resourceMenu2");
	
	var worker1 = document.createElement("div");
	worker1.innerHTML = "Available workers : " + serverAction.availableWorkers;
	worker1.setAttribute("id", availableWorkersId);
	if (!(serverAction.image in unitImages)){
		var image = new Image();
	    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
	    unitImages[serverAction.image]=image;
	}
	resourceMenu2.appendChild(worker1);
	cloneImage(resourceMenu2, unitImages[serverAction.image]);
	
	var resourceMenu3 = document.createElement("resourceMenu3");
	
	var worker2 = document.createElement("div");
	worker2.setAttribute("id", unavailableWorkers);
	worker2.innerHTML = "Unavailable workers : " + serverAction.unavailableWorkers;
	resourceMenu3.appendChild(worker2);
	cloneImage(resourceMenu3, unitImages[serverAction.image]);
	
	resourceMenu.appendChild(resourceMenu1);
	resourceMenu.appendChild(resourceMenu2);
	resourceMenu.appendChild(resourceMenu3);
	
	return resourceMenu;
}

/**montre le nombre de travailleurs disponibles**/
function displayBaseWorkers(serverAction){
	var factionInfo = document.getElementById("currentPlayerBoard");
	var resourceMenu = drawBaseWorkers(serverAction, "availableWorkers", "unavailableWorkers");
	factionInfo.appendChild(resourceMenu);
}

function displayBaseWorkers2(serverAction){
	var factionInfo = document.getElementById("info_" + serverAction.playerName);
	var resourceMenu = drawBaseWorkers(serverAction,
			"availableWorkers_" + serverAction.playerName, "unavailableWorkers_" + serverAction.playerName);
	factionInfo.appendChild(resourceMenu);
}

function drawPlayerResources(serverAction, baseMineralId, baseGasId, textTitle){
	var resourceMenu = document.createElement("resourceMenu");
	resourceMenu.setAttribute("class", "playerInfoBoard");
	
	var resourceMenu1 = document.createElement("resourceMenu1");
	resourceMenu1.innerHTML = textTitle;
	
	var resourceMenu2 = document.createElement("resourceMenu2");
	resourceMenu2.setAttribute("id", baseMineralId);
	
	var mineral = document.createElement("div");
	mineral.setAttribute("class", "imageMenu");
	var mineralImage = document.createElement("img");
	mineralImage.setAttribute("src", "../starcraftWebResources/mapResources/mineral.png");
	mineralImage.setAttribute("width", 40);
	mineralImage.setAttribute("height", 40);
	mineral.appendChild(mineralImage);
	
	var mineralText = document.createElement("div");
	mineralText.innerHTML = serverAction.mineral;
	mineralText.setAttribute("class", "textMenu");
	
	mineral.appendChild(mineralImage);
	mineral.appendChild(mineralText);
	resourceMenu2.appendChild(mineral);
	
	var resourceMenu3 = document.createElement("resourceMenu3");
	resourceMenu3.setAttribute("id", baseGasId)
	
	var gas = document.createElement("div");
	gas.setAttribute("class", "imageMenu");
	var gasImage = document.createElement("img");
	gasImage.setAttribute("src", "../starcraftWebResources/mapResources/gas.png");
	gasImage.setAttribute("width", 40);
	gasImage.setAttribute("height", 40);
	gas.appendChild(gasImage);
	
	var gasText = document.createElement("div");
	gasText.innerHTML = serverAction.gas;
	gasText.setAttribute("class", "textMenu");
	
	gas.appendChild(gasImage);
	gas.appendChild(gasText);
	resourceMenu3.appendChild(gas);
	
	resourceMenu.appendChild(resourceMenu1);
	resourceMenu.appendChild(resourceMenu2);
	resourceMenu.appendChild(resourceMenu3);
	
	return resourceMenu;
}

/** remplit la partie "faction" du champs d'information sur le joueur actuel**/
function displayPlayerResources(serverAction){
	var factionInfo = document.getElementById("currentPlayerBoard");
	var resourceMenu = drawPlayerResources(serverAction, "baseMineral", "baseGas", "Base resources");
	factionInfo.appendChild(resourceMenu);
}

/** remplit la partie "faction" du champs d'information sur le joueur actuel**/
function displayPlayerResources2(serverAction){
	var factionInfo = document.getElementById("info_" + serverAction.playerName);
	var resourceMenu = drawPlayerResources(serverAction,
			"baseMineral_"+serverAction.playerName, "baseGas_"+serverAction.playerName, "Base resources");
	factionInfo.appendChild(resourceMenu);
}


//partie s'occupant des token (Raynor)
/** remplit la partie "faction" du champs d'information sur le joueur actuel**/
function displayPlayerResourceToken(serverAction){
	var resourceTokensInfo = document.getElementById("resourceTokensInfo");
	if (!resourceTokensInfo){
		if (serverAction.mineral + serverAction.gas > 0){
			var factionInfo = document.getElementById("currentPlayerBoard");
			var resourceMenu = drawPlayerResources(serverAction, "mineralToken", "gasToken", "Resource tokens");
			resourceMenu.setAttribute("id", "resourceTokensInfo");
			factionInfo.appendChild(resourceMenu);
		}
	}else{
		if (serverAction.mineral + serverAction.gas > 0){
			var mineralToken = document.getElementById("mineralToken").childNodes[0];
			for (var i = 0; i < mineralToken.childNodes.length; i++) {
			    if (mineralToken.childNodes[i].className == "textMenu") {
			    	mineralToken.childNodes[i].innerHTML = serverAction.mineral;
			    	break;
			    }        
			}
			var gasToken = document.getElementById("gasToken").childNodes[0];
			for (var i = 0; i < gasToken.childNodes.length; i++) {
			    if (gasToken.childNodes[i].className == "textMenu") {
			    	gasToken.childNodes[i].innerHTML = serverAction.gas;
			    	break;
			    }        
			}
		}else{
			resourceTokensInfo.parentNode.removeChild(resourceTokensInfo);
		}
	}
}

function displayPlayerResourceToken2(serverAction){
	var resourceTokensInfo = document.getElementById("resourceTokensInfo_" + serverAction.playerName);
	if (!resourceTokensInfo){
		if (serverAction.mineral + serverAction.gas > 0){
			var factionInfo = document.getElementById("info_" + serverAction.playerName);
			var resourceMenu = drawPlayerResources(serverAction,
					"mineralToken_" + serverAction.playerName, "gasToken_" + serverAction.playerName, "Resource tokens");
			resourceMenu.setAttribute("id", "resourceTokensInfo_" + serverAction.playerName);
			factionInfo.appendChild(resourceMenu);
		}
	}else{
		if (serverAction.mineral + serverAction.gas > 0){
			var mineralToken = document.getElementById("mineralToken_"+ serverAction.playerName).childNodes[0];
			for (var i = 0; i < mineralToken.childNodes.length; i++) {
			    if (mineralToken.childNodes[i].className == "textMenu") {
			    	mineralToken.childNodes[i].innerHTML = serverAction.mineral;
			    	break;
			    }        
			}
			var gasToken = document.getElementById("gasToken_"+ serverAction.playerName).childNodes[0];
			for (var i = 0; i < gasToken.childNodes.length; i++) {
			    if (gasToken.childNodes[i].className == "textMenu") {
			    	gasToken.childNodes[i].innerHTML = serverAction.gas;
			    	break;
			    }        
			}
		}else{
			resourceTokensInfo.parentNode.removeChild(resourceTokensInfo);
		}
	}
}


function addTokenResource(resource){
	var clientAction = {
			action : "addTokenResource",
			resourceType : resource
	};
	webSocket.send(JSON.stringify(clientAction));
}


function addTokenResourceEvent(image, resource){
	if (currentTurn &&  !image.hasEventListener("click")){
		image.addEventListener("click", function(event) {
			addTokenResource(resource);
		})
	}
}

function displayResourcesToken(serverAction){
	var mineralToken = mineralImage.clone();
	mineralToken.x = 10;
	mineralToken.y = 10;
	mineralToken.scaleX = 2;
	mineralToken.scaleY = 2;
	addTokenResourceEvent(mineralToken, "mineral");
	var gasToken = gasImage.clone();
	gasToken.x = 110;
	gasToken.y = 10;
	gasToken.scaleX = 2;
	gasToken.scaleY = 2;
	addTokenResourceEvent(gasToken, "gas");
	stage.canvas.height = 100;
	stage.addChild(mineralToken);
	stage.addChild(gasToken);
	stage.update();
	keepTabOpen('gameBoardLink', 'gameBoardTab', 'gametabcontent');
}
/*fin réception des messages du serveur*/

//TODO
/*Fonctions de dessins*/
/**dessine l'image d'un travailleur sur une zone**/
function drawWorkerWithText(serverAction){
	var result = new createjs.Container();
	var unit = drawUnitImage(serverAction);
	var textWorker = new createjs.Text(serverAction.number, "30px Arial", textOutlineColor);
	var textWorker2 = new createjs.Text(serverAction.number, "30px Arial", textColor);
	textWorker.set({name : "textWorker"});
	textWorker2.set({name : "textWorker2"});
	textWorker.outline = 1.3;
	result.addChild(unit);
	result.addChild(textWorker);
	result.addChild(textWorker2);
	textWorker.x = - 20;
	textWorker.y = - 20;
	textWorker2.x = - 20;
	textWorker2.y = - 20;
	unit.y = - 20;
	return result;
}

function drawResourceWithText(resourceType, number, givenImage){
	var image;
	if (resourceType === "mineral"){
		image = mineralImage.clone();
	}else if (resourceType === "gas"){
		image = gasImage.clone();
	}else if (resourceType === "unit"){
		image = givenImage;
	}
	var result = new createjs.Container();
	var textResource = new createjs.Text(number, "30px Arial", textOutlineColor);
	textResource.set({name : "textResource"});
	var textResource2 = new createjs.Text(number, "30px Arial", textColor);
	textResource2.set({name : "textResource2"});
	textResource.outline = 1.3;
	textResource.x = 10;
	textResource.y = 10;
	textResource2.x = 10;
	textResource2.y = 10;
	result.addChild(image);
	result.addChild(textResource);
	result.addChild(textResource2);
	return result;
}

function disactivateBaseResources(){
	var baseMineral = document.getElementById("baseMineral");
	baseMineral.removeEventListener("click", sendSetWorkerOnBaseMineral);
	baseMineral.removeAttribute("onmouseover");
	baseMineral.removeAttribute("onmouseout");
	var baseGas = document.getElementById("baseGas");
	baseGas.removeEventListener("click", sendSetWorkerOnBaseGas);
	baseGas.removeAttribute("onmouseover");
	baseGas.removeAttribute("onmouseout");
	var mineralToken = document.getElementById("mineralToken");
	if (mineralToken){
		mineralToken.removeEventListener("click", sendUseMineralToken);
		mineralToken.removeAttribute("onmouseover");
		mineralToken.removeAttribute("onmouseout");
		var gasToken = document.getElementById("gasToken");
		gasToken.removeEventListener("click", sendUseGasToken);
		gasToken.removeAttribute("onmouseover");
		gasToken.removeAttribute("onmouseout");
	}
}

/*fonctions utilitaires*/
function sendSetWorkerOnBaseMineral(){
	var baseMineral = document.getElementById("baseMineral");
	setBackGround(baseMineral, "rgba(0, 0, 0, 0)");
	setWorkerOnBaseMineral();
}

function sendSetWorkerOnBaseGas(){
	var baseGas = document.getElementById("baseGas");
	setBackGround(baseGas, "rgba(0, 0, 0, 0)");
	setWorkerOnBaseGas();
}

function sendUseMineralToken(){
	var baseGas = document.getElementById("mineralToken");
	setBackGround(baseGas, "rgba(0, 0, 0, 0)");
	useResourceToken("mineral");
}

function sendUseGasToken(){
	var baseGas = document.getElementById("gasToken");
	setBackGround(baseGas, "rgba(0, 0, 0, 0)");
	useResourceToken("gas");
}

function displayResourceChoice(){
	var actionMenu = document.getElementById("dropDownResources");
	if (actionMenu.style.display === "none"){
		actionMenu.style.display = "block";
	}else{
		actionMenu.style.display = "none";
	}
}

function getPlanetAtCoordinates(coordinates){
	var square = galaxyStage.getChildByName(coordinates);
	var currentPlanet;
	for (var i = 0; i < square.numChildren; i++){
		if (square.getChildAt(i).type){
			if (square.getChildAt(i).type === "planet"){
				currentPlanet = square.getChildAt(i);
			}
		}
	}
	return currentPlanet;
}

