var selectedSquareOrder;

/*actions du joueur*/
function endGalaxyOrderChoiceTurn(){
	if (currentTurn){
		clearEventAndHighlight();
		var clientAction = {
				action : "endGalaxyOrderChoiceTurn",
				coordinates : selectedSquareOrder.name
		};
		webSocket.send(JSON.stringify(clientAction));
		var endTurnButton = document.getElementById("endTurnButton");
		endTurnButton.disabled = true;
		selectedSquareOrder.alpha = 1;
		selectSquareOrder(undefined);
		galaxyStage.update();
	}
}

function executeOrder(){
	if (currentTurn){
		var clientAction = {
				action : "executeOrder",
		};
		webSocket.send(JSON.stringify(clientAction));
	}
}

function cancelOrder(){
	if (currentTurn){
		var clientAction = {
				action : "cancelOrder",
		};
		webSocket.send(JSON.stringify(clientAction));
	}
}

function chooseStarOrder(order){
	if (currentTurn){
		var chooseSpecialOrder = document.getElementById("chooseSpecialOrder");
		var isSpecialOrder = "false";
		if (chooseSpecialOrder){
			isSpecialOrder = chooseSpecialOrder.value;
		}
		var clientAction = {
				action : "chooseStarOrder",
				name : order,
				special : isSpecialOrder
		};
		webSocket.send(JSON.stringify(clientAction));
	}
}

/*ajout d'évènements*/
function addOrderExecutionEvent(square){
	if (currentTurn && !square.hasEventListener("click")){
		addObjectWithEvents(square);
		square.addEventListener("mouseover", function(event) { 
			if (selectedSquareOrder === undefined){
				square.alpha = 0.5;
			}
			galaxyStage.update(event);
		})

		square.addEventListener("mouseout", function(event) { 
			if (selectedSquareOrder === undefined){
				square.alpha = 1;
			}
			galaxyStage.update(event);
		})

		square.addEventListener("click", function(event) { 
			if (selectedSquareOrder === undefined){
				selectSquareOrder(square);
				square.alpha = 0.5;
				galaxyStage.update(event);
			}else{
				selectedSquareOrder.alpha = 1;
				selectSquareOrder(undefined);
				galaxyStage.update(event);
			}
		})
	}
}

/*réception des messages du serveur*/

/**active les différentes planètes où je joueur peut exécuter un ordre**/
function activateValidSquareOrders(serverAction){
	var coordinates = serverAction.coordinates;
	for(var i in coordinates){
		var currentPlanetCoordinates = coordinates[i].coordinate;
		var currentSquare = galaxyStage.getChildByName(currentPlanetCoordinates);
		addOrderExecutionEvent(currentSquare);
	}
}

/**ajoute les boutons correspondant aux actions possibles pendant cette étape**/
function addGalaxyOrderChoiceButton(serverAction){

	var actionMenu = document.getElementById("buttonContainer");
	
	//bouton de fin de tour
	var endTurnButton =  document.createElement("button");
	endTurnButton.setAttribute("id", "endTurnButton");
	endTurnButton.setAttribute("class", "gameButton");
	endTurnButton.setAttribute("type", "button");
	endTurnButton.setAttribute("onclick", "this.disabled=true;endGalaxyOrderChoiceTurn();");
	endTurnButton.innerHTML = "Execute selected order";
	endTurnButton.disabled = true;
	
	actionMenu.appendChild(endTurnButton);
}

function addStarOrderButtons(serverAction){

	var actionMenu = document.getElementById("buttonContainer");
	
	if (serverAction.research){
		var researchButton =  document.createElement("button");
		researchButton.setAttribute("class", "gameButton");
		researchButton.setAttribute("type", "button");
		researchButton.setAttribute("onclick", "this.disabled=true;chooseStarOrder('research');");
		researchButton.innerHTML = "Execute a research order";
		actionMenu.appendChild(researchButton);
	}
	if (serverAction.build){
		var buildButton =  document.createElement("button");
		buildButton.setAttribute("class", "gameButton");
		buildButton.setAttribute("type", "button");
		buildButton.setAttribute("onclick", "this.disabled=true;chooseStarOrder('build');");
		buildButton.innerHTML = "Execute a build order";
		actionMenu.appendChild(buildButton);
	}
	if (serverAction.move){
		var moveButton =  document.createElement("button");
		moveButton.setAttribute("class", "gameButton");
		moveButton.setAttribute("type", "button");
		moveButton.setAttribute("onclick", "this.disabled=true;chooseStarOrder('move');");
		moveButton.innerHTML = "Execute a move order";
		actionMenu.appendChild(moveButton);
	}
	if (serverAction.special){
		var chooseSpecialOrder =  document.createElement("select");
		chooseSpecialOrder.setAttribute("id", "chooseSpecialOrder");
		chooseSpecialOrder.setAttribute("class", "gameButton");
		
		var normalOrder =  document.createElement("option");
		normalOrder.setAttribute("value", "false");
		normalOrder.innerHTML = "Use a normal order";
		
		var specialOrder =  document.createElement("option");
		specialOrder.setAttribute("value", "true");
		specialOrder.innerHTML = "Use a special order";
		
		chooseSpecialOrder.appendChild(normalOrder);
		chooseSpecialOrder.appendChild(specialOrder);
		actionMenu.appendChild(chooseSpecialOrder);
	}
	
}

/**ajoute les boutons correspondant aux actions possibles pendant cette étape**/
function addActivationChoiceButton(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	
	//bouton de fin de tour
	var executeOrder =  document.createElement("button");
	executeOrder.setAttribute("id", "endTurnButton");
	executeOrder.setAttribute("class", "gameButton");
	executeOrder.setAttribute("type", "button");
	executeOrder.setAttribute("onclick", "this.disabled=true;executeOrder();");
	executeOrder.innerHTML = "Execute "+ serverAction.orderName +" order on " + serverAction.planetName;
	
	var cancelOrder =  document.createElement("button");
	cancelOrder.setAttribute("id", "endTurnButton");
	cancelOrder.setAttribute("class", "gameButton");
	cancelOrder.setAttribute("type", "button");
	cancelOrder.setAttribute("onclick", "this.disabled=true;cancelOrder();");
	cancelOrder.innerHTML = "Cancel the order";
	
	actionMenu.appendChild(executeOrder);
	actionMenu.appendChild(cancelOrder);
}


/* fin réception des messages du serveur*/

/*fonctions utilitaires*/

function selectSquareOrder(value){
	if (value !== undefined){
		endTurnButton.disabled = false;
	} else {
		endTurnButton.disabled = true;
	}
	selectedSquareOrder = value;
}