var imageWidth = actualWidth * 0.2;
var imageHeight = actualWidth * 0.15;

var imageWidth2 = actualWidth * 0.1;
var imageHeight2 = actualWidth * 0.075;

function updateOwnedModules(serverAction){
	var ownedModulesInfo = document.getElementById("ownedModulesInfo");
	if (ownedModulesInfo){
		var imagePath = serverAction.faction + "/"+ serverAction.image;
		var image = new Image();
		if (!(serverAction.image in unitImages)){
		    image.src = "../starcraftWebResources/moduleImages/"+ imagePath;
		    unitImages[imagePath]=image;
		}else{
			image = unitImages[imagePath];
		}
		cloneImage(ownedModulesInfo, image);
	}
}

function updateOwnedModules2(serverAction){
	var ownedModulesInfo = document.getElementById("ownedModulesInfo_" + serverAction.playerName);
	if (ownedModulesInfo){
		var imagePath = serverAction.faction + "/"+ serverAction.image;
		var image = new Image();
		if (!(serverAction.image in unitImages)){
		    image.src = "../starcraftWebResources/moduleImages/"+ imagePath;
		    unitImages[imagePath]=image;
		}else{
			image = unitImages[imagePath];
		}
		cloneImage(ownedModulesInfo, image);
	}
}

function drawOwnedModules(serverAction, ownedModulesId){
	var playerItemDisplay = document.createElement("playerItemDisplay");
	playerItemDisplay.setAttribute("class", "playerInfoBoard");
	var playerItemTitle = document.createElement("playerItemTitle");
	playerItemTitle.innerHTML = "Owned modules";
	playerItemDisplay.appendChild(playerItemTitle);
	
	var playerItemList = document.createElement("playerItemList");
	playerItemList.setAttribute("id", ownedModulesId);
	playerItemDisplay.appendChild(playerItemList);
	var moduleList = serverAction.moduleList;
	for (var i in moduleList){
		var imagePath = moduleList[i].faction + "/"+ moduleList[i].image;
		var image = new Image();
		if (!(imagePath in unitImages)){
		    image.src = "../starcraftWebResources/moduleImages/"+ imagePath;
		    unitImages[imagePath]=image;
		}else{
			image = unitImages[imagePath];
		}
		cloneImage(playerItemList, image);
	}
	return playerItemDisplay;
}

function showOwnedModules(serverAction){
	if (!document.getElementById("ownedModulesInfo")){
		var factionInfo = document.getElementById("currentPlayerBoard");
		var ownedModulesInfo = drawOwnedModules(serverAction, "ownedModulesInfo");
		factionInfo.appendChild(ownedModulesInfo);
	}
}

function showOwnedModules2(serverAction){
	if (!document.getElementById("ownedModulesInfo_" + serverAction.playerName)){
		var factionInfo = document.getElementById("info_" + serverAction.playerName);
		var ownedModulesInfo = drawOwnedModules(serverAction, "ownedModulesInfo_" + serverAction.playerName);
		factionInfo.appendChild(ownedModulesInfo);
	}
}

function updateUnlockedUnits(serverAction){
	var unlockedUnitsInfo = document.getElementById("unlockedUnitsInfo");
	if (unlockedUnitsInfo){
		var image = new Image();
		if (!(serverAction.image in unitImages)){
		    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
		    unitImages[serverAction.name]=image;
		}else{
			image = unitImages[serverAction.image];
		}
		cloneImage(unlockedUnitsInfo, image);
	}
}

function updateUnlockedUnits2(serverAction){
	var unlockedUnitsInfo = document.getElementById("unlockedUnitsInfo_" + serverAction.playerName);
	if (unlockedUnitsInfo){
		var image = new Image();
		if (!(serverAction.image in unitImages)){
		    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
		    unitImages[serverAction.name]=image;
		}else{
			image = unitImages[serverAction.image];
		}
		cloneImage(unlockedUnitsInfo, image);
	}
}

function showUnlockedUnitsInfo(serverAction){
	if (!document.getElementById("unlockedUnitsInfo")){
		var factionInfo = document.getElementById("currentPlayerBoard");
		var unlockedUnitsInfo = drawUnlockedUnits(serverAction, "unlockedUnitsInfo");
		factionInfo.appendChild(unlockedUnitsInfo);
	}
}

function showUnlockedUnitsInfo2(serverAction){
	if (!document.getElementById("unlockedUnitsInfo_" + serverAction.playerName)){
		var factionInfo = document.getElementById("info_" + serverAction.playerName);
		var unlockedUnitsInfo = drawUnlockedUnits(serverAction, "unlockedUnitsInfo_" + serverAction.playerName);
		factionInfo.appendChild(unlockedUnitsInfo);
	}
}

function drawUnlockedUnits(serverAction, unlockedUnitsInfoId){
	var playerItemDisplay = document.createElement("playerItemDisplay");
	playerItemDisplay.setAttribute("class", "playerInfoBoard");
	var playerItemTitle = document.createElement("playerItemTitle");
	playerItemTitle.innerHTML = "Unlocked units";
	playerItemDisplay.appendChild(playerItemTitle);
	
	var playerItemList = document.createElement("playerItemList");
	playerItemList.setAttribute("id", unlockedUnitsInfoId);
	playerItemDisplay.appendChild(playerItemList);
	var unitList = serverAction.unitList;
	for (var i in unitList){
		var image = new Image();
		if (!(unitList[i].image in unitImages)){
		    image.src = "../starcraftWebResources/unitImages/"+ unitList[i].species + "/"+ unitList[i].image;
		    unitImages[unitList[i].name]=image;
		}else{
			image = unitImages[unitList[i].image];
		}
		cloneImage(playerItemList, image);
	}
	return playerItemDisplay;
}


function addReserveUnitDisplay(serverAction){
	var playerItemList = document.getElementById("reservedUnitsInfo");
	if (playerItemList){
		var image = new Image();
		if (!(serverAction.image in unitImages)){
		    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
		    unitImages[serverAction.image]=image;
		}else{
			image = unitImages[serverAction.image];
		}
		imageClone = new Image();
	    imageClone.src = image.src;
	    imageClone.id = "unit" +serverAction.unitId;
	    playerItemList.appendChild(imageClone);
	}
}

function addReserveUnitDisplay2(serverAction){
	var playerItemList = document.getElementById("reservedUnitsInfo_"+ serverAction.playerName);
	if (playerItemList){
		var image = new Image();
		if (!(serverAction.image in unitImages)){
		    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
		    unitImages[serverAction.image]=image;
		}else{
			image = unitImages[serverAction.image];
		}
		imageClone = new Image();
	    imageClone.src = image.src;
	    imageClone.id = serverAction.playerName+"_unit" +serverAction.unitId;
	    playerItemList.appendChild(imageClone);
	}
}

function removeReserveUnitDisplay(serverAction){
	var unit = document.getElementById("unit" + serverAction.unitId);
	if (unit){
		unit.parentNode.removeChild(unit);
	}
}

function removeReserveUnitDisplay2(serverAction){
	var unit = document.getElementById(serverAction.playerName+"_unit" + serverAction.unitId);
	if (unit){
		unit.parentNode.removeChild(unit);
	}
}

function reservedUnitsInfo2(serverAction){
	if (!document.getElementById("reservedUnitsInfo_"+ serverAction.playerName)){
		var factionInfo = document.getElementById("info_"+ serverAction.playerName);
		var unlockedUnitsInfo = drawUnitReserve(serverAction,
				"reservedUnitsInfo_"+serverAction.playerName, serverAction.playerName+"_");
		factionInfo.appendChild(unlockedUnitsInfo);
	}
}

function reservedUnitsInfo(serverAction){
	if (!document.getElementById("reservedUnitsInfo")){
		var factionInfo = document.getElementById("currentPlayerBoard");
		var unlockedUnitsInfo = drawUnitReserve(serverAction, "reservedUnitsInfo", "");
		factionInfo.appendChild(unlockedUnitsInfo);
	}
}

function drawUnitReserve(serverAction, unlockedUnitsInfoId, unitPrefix){
	var playerItemDisplay = document.createElement("playerItemDisplay");
	playerItemDisplay.setAttribute("class", "playerInfoBoard");
	var playerItemTitle = document.createElement("playerItemTitle");
	playerItemTitle.innerHTML = "Units reserve";
	playerItemDisplay.appendChild(playerItemTitle);
	
	var playerItemList = document.createElement("playerItemList");
	playerItemList.setAttribute("id", unlockedUnitsInfoId);
	playerItemDisplay.appendChild(playerItemList);
	var unitList = serverAction.unitList;
	for (var i in unitList){
		var image = new Image();
		if (!(unitList[i].image in unitImages)){
		    image.src = "../starcraftWebResources/unitImages/"+ unitList[i].species + "/"+ unitList[i].image;
		    unitImages[unitList[i].image]=image;
		}else{
			image = unitImages[unitList[i].image];
		}
		imageClone = new Image();
	    imageClone.src = image.src;
	    imageClone.id = unitPrefix+"unit" +unitList[i].name;
	    playerItemList.appendChild(imageClone);
	}
	return playerItemDisplay;
}

//affiche les différentes factions que le joueur peut choisir
function printFactionChoice(serverAction){

	var gameBoard = document.getElementById("gameBoard");
	
	var addedFactionChoice = document.createElement("div");
	addedFactionChoice.setAttribute("class", "factionChoices");
	addedFactionChoice.setAttribute("speciesName", serverAction.speciesName);
	addedFactionChoice.setAttribute("factionName", serverAction.factionName);
	addedFactionChoice.setAttribute("onclick", "sendFactionChoice(this)");
	
	var addedSpeciesInfo = document.createElement("p");
	addedSpeciesInfo.innerHTML = "Species : " + serverAction.speciesName;
	
	var addedFactionInfo = document.createElement("p");
	addedFactionInfo.innerHTML = "Faction : " + serverAction.factionName;
	
	var image = document.createElement("img");
	
	image.setAttribute("src", "../starcraftWebResources/factionChoices/" + serverAction.image);
	image.setAttribute("width", imageWidth);
	image.setAttribute("height", imageHeight);
	
	addedFactionChoice.appendChild(addedSpeciesInfo);
	addedFactionChoice.appendChild(addedFactionInfo);
	addedFactionChoice.appendChild(image);
	addedFactionChoice.style.backgroundColor = serverAction.factionColor;
	
	gameBoard.appendChild(addedFactionChoice);
}

function displayFirstPlayerChoice(serverAction){
	var gameBoard = document.getElementById("gameBoard");
	
	var addedFactionChoice = document.createElement("div");
	addedFactionChoice.setAttribute("class", "factionChoices");
	addedFactionChoice.setAttribute("onclick", "sendFirstPlayerChoice('"+ serverAction.playerName +"')");
	
	var playerName = document.createElement("p");
	playerName.innerHTML = "Player : " + serverAction.playerName;
	
	var addedSpeciesInfo = document.createElement("p");
	addedSpeciesInfo.innerHTML = "Species : " + serverAction.speciesName;
	
	var addedFactionInfo = document.createElement("p");
	addedFactionInfo.innerHTML = "Faction : " + serverAction.factionName;
	
	var image = document.createElement("img");
	
	image.setAttribute("src", "../starcraftWebResources/factionChoices/" + serverAction.factionImage);
	image.setAttribute("width", imageWidth);
	image.setAttribute("height", imageHeight);
	
	addedFactionChoice.appendChild(playerName);
	addedFactionChoice.appendChild(addedSpeciesInfo);
	addedFactionChoice.appendChild(addedFactionInfo);
	addedFactionChoice.appendChild(image);
	addedFactionChoice.style.backgroundColor = serverAction.factionColor;
	
	gameBoard.appendChild(addedFactionChoice);
}

// affiche les différentes factions déjà choisies
function playerFactionChoice(serverAction){
	var factionInfo = document.getElementById("factionInfo");
	
	var playerFaction = document.createElement("div");
	playerFaction.setAttribute("class", "activeFactionChoices");
	playerFaction.setAttribute("speciesName", serverAction.speciesName);
	playerFaction.setAttribute("factionName", serverAction.factionName);
	
	var playerNameInfo = document.createElement("p");
	playerNameInfo.innerHTML = "Player name : " + serverAction.playerName;
	
	var addedSpeciesInfo = document.createElement("p");
	addedSpeciesInfo.innerHTML = "Species : " + serverAction.speciesName;
	
	var addedFactionInfo = document.createElement("p");
	addedFactionInfo.innerHTML = "Faction : " + serverAction.factionName;
	
	var conquestPoints = document.createElement("p");
	conquestPoints.setAttribute("id", "conquestPoints");
	conquestPoints.innerHTML = "Conquest points : " + serverAction.conquest;
	
	var image = document.createElement("img");
	image.setAttribute("src", "../starcraftWebResources/factionChoices/" + serverAction.factionImage);
	image.setAttribute("width", imageWidth2);
	image.setAttribute("height", imageHeight2);
	
	playerFaction.appendChild(playerNameInfo);
	playerFaction.appendChild(addedSpeciesInfo);
	playerFaction.appendChild(addedFactionInfo);
	playerFaction.appendChild(conquestPoints);
	playerFaction.appendChild(image);
	
	
	if (serverAction.playerName === currentPlayer){
		var activeFaction = document.createElement("p");
		if (currentTurn){
			activeFaction.innerHTML = "It\'s your turn";
			playerFaction.style.background = 'green';
		}else{
			activeFaction.innerHTML = "Currently playing faction";
			playerFaction.style.background = 'red';
		}
		playerFaction.appendChild(activeFaction);
		
	}else{
		playerFaction.style.background = 'white';
	}
	factionInfo.appendChild(playerFaction);
}

function drawFactionInfo(serverAction, conquestId){
	var playerFaction = document.createElement("div");
	playerFaction.setAttribute("class", "playerInfoBoard");
	playerFaction.setAttribute("speciesName", serverAction.speciesName);
	playerFaction.setAttribute("factionName", serverAction.factionName);
	
	var addedSpeciesInfo = document.createElement("p");
	addedSpeciesInfo.innerHTML = "Species : " + serverAction.speciesName;
	
	var addedFactionInfo = document.createElement("p");
	addedFactionInfo.innerHTML = "Faction : " + serverAction.factionName;
	
	var conquestPoints = document.createElement("p");
	conquestPoints.setAttribute("id", conquestId);
	conquestPoints.innerHTML = "Conquest points : " + serverAction.conquest;
	
	var image = document.createElement("img");
	image.setAttribute("src", "../starcraftWebResources/factionChoices/" + serverAction.factionImage);
	image.setAttribute("width", imageWidth2);
	image.setAttribute("height", imageHeight2);
	
	playerFaction.appendChild(addedSpeciesInfo);
	playerFaction.appendChild(addedFactionInfo);
	playerFaction.appendChild(conquestPoints);
	playerFaction.appendChild(image);
	
	playerFaction.style.backgroundColor = serverAction.factionColor;
	playerColor = serverAction.factionColor;
	return playerFaction;
}

// remplit la partie "faction" du champs d'information sur le joueur actuel
function playerFactionInfo(serverAction){
	var factionInfo = document.getElementById("currentPlayerBoard");
	
	var playerFaction = drawFactionInfo(serverAction, "conquestPoints");
	
	factionInfo.appendChild(playerFaction);
}

function printPlayerFactionInfo2(serverAction){
	var factionInfo = document.getElementById("info_" + serverAction.playerName);
	
	var playerFaction = drawFactionInfo(serverAction, "conquestPoints_"+serverAction.playerName);
	playerFaction.setAttribute("id", "factionInfo_" + serverAction.playerName);
	
	factionInfo.appendChild(playerFaction);
}

function drawCardNumber(serverAction, idSuffix, activateButton){
	var combatCardInfo = document.createElement("div");
	combatCardInfo.setAttribute("class", "playerInfoBoard");

	combatCardInfo.style.height = "300px";
	combatCardInfo.style.width ="200px";
	combatCardInfo.style.border = "8px + serverAction.color + solid";
	combatCardInfo.style.margin = "5px";
	combatCardInfo.style.borderRadius = "20px";
	
	var cardTitle = document.createElement("playerCardsTitle");
	cardTitle.innerHTML = "Player Cards";

	var cardButton = document.createElement("button");
	cardButton.innerHTML = "Combat Cards in Hand : ";
	cardButton.setAttribute("class", "cardButton");
	if (activateButton){
		cardButton.setAttribute("onclick", "askCombatCardHand()");
	}else{
		cardButton.disabled = true;
	}
	
	var cardNumber = document.createElement("div");
	cardNumber.setAttribute("id", "combatCardNumber" + idSuffix);
	cardNumber.style.display = "inline";
	cardNumber.innerHTML = serverAction.cardNumber;
	cardButton.appendChild(cardNumber);
	
	var activeCardButton = document.createElement("button");
	activeCardButton.innerHTML = "Active Combat Cards : ";
	activeCardButton.setAttribute("id", "activeCardButton" + idSuffix);
	activeCardButton.setAttribute("class", "cardButton");
	if (idSuffix === ""){
		activeCardButton.setAttribute("onclick", "askActiveCard()");
	}else{
		activeCardButton.setAttribute("onclick", "askActiveCard2('" + serverAction.playerName + "')");
	}
	activeCardButton.disabled = true;
	var activeCardNumber = document.createElement("div");
	activeCardNumber.setAttribute("id", "activeCardNumber" + idSuffix);
	activeCardNumber.style.display = "inline";
	activeCardNumber.innerHTML = 0;
	activeCardButton.appendChild(activeCardNumber);
	
	var drawnEventCards = document.createElement("button");
	drawnEventCards.innerHTML = "Drawn Event Cards : ";
	drawnEventCards.setAttribute("class", "cardButton");
	drawnEventCards.disabled = true;
	
	var eventCardNumber = document.createElement("div");
	eventCardNumber.setAttribute("id", "eventCardNumber" + idSuffix);
	eventCardNumber.style.display = "inline";
	eventCardNumber.innerHTML = 0;
	drawnEventCards.appendChild(eventCardNumber);
	
	var activeEventCards = document.createElement("button");
	activeEventCards.innerHTML = "Active Event Cards : ";
	activeEventCards.setAttribute("id", "activeEventCards" + idSuffix);
	activeEventCards.setAttribute("class", "cardButton");
	if (idSuffix === ""){
		activeEventCards.setAttribute("onclick", "askActiveEventCard()");
	}else{
		activeEventCards.setAttribute("onclick", "askActiveEventCard2('" + serverAction.playerName + "')");
	}
	activeEventCards.disabled = true;
	var activeEventCardNumber = document.createElement("div");
	activeEventCardNumber.setAttribute("id", "activeEventCardNumber" + idSuffix);
	activeEventCardNumber.style.display = "inline";
	activeEventCardNumber.innerHTML = 0;
	activeEventCards.appendChild(activeEventCardNumber);
	
	combatCardInfo.appendChild(cardTitle);
	combatCardInfo.appendChild(cardButton);
	combatCardInfo.appendChild(activeCardButton);
	combatCardInfo.appendChild(drawnEventCards);
	combatCardInfo.appendChild(activeEventCards);
	
	return combatCardInfo;
}

/**indique le nombre de carte en mains pour le joueur et lui permet d'afficher ces cartes**/
function displayCardNumber(serverAction){
	var factionInfo = document.getElementById("currentPlayerBoard");
	var combatCardInfo = drawCardNumber(serverAction, "", true);
	factionInfo.appendChild(combatCardInfo);
}

function displayCardNumber2(serverAction){
	var factionInfo = document.getElementById("info_" + serverAction.playerName);
	var combatCardInfo = drawCardNumber(serverAction, "_"+ serverAction.playerName, false);
	factionInfo.appendChild(combatCardInfo);
}


/**indique le nombre de carte actives pour le joueur et lui permet d'afficher ces cartes**/
function displayActiveCardNumber(serverAction){
	var text = document.getElementById("activeCardNumber");
	if (text){
		text.innerHTML = serverAction.cardNumber;
		var button = document.getElementById("activeCardButton");
		if (serverAction.cardNumber === 0){
			button.disabled = true;
		}else{
			button.disabled = false;
		}
	}
}

/**indique le nombre de carte actives pour le joueur et lui permet d'afficher ces cartes**/
function displayActiveCardNumber2(serverAction){
	var text = document.getElementById("activeCardNumber_" + serverAction.playerName);
	if (text){
		text.innerHTML = serverAction.cardNumber;
		var button = document.getElementById("activeCardButton_" + serverAction.playerName);
		if (serverAction.cardNumber === 0){
			button.disabled = true;
		}else{
			button.disabled = false;
		}
	}
}

function updateCardNumber(serverAction){
	var cardNumber = document.getElementById("combatCardNumber");
	if (cardNumber){
		cardNumber.innerHTML = serverAction.cardNumber;
	}
}

function updateCardNumber2(serverAction){
	var cardNumber = document.getElementById("combatCardNumber_"+ serverAction.playerName);
	if (cardNumber){
		cardNumber.innerHTML = serverAction.cardNumber;
	}
}


function displayCardInHand(serverAction){
	var cardNumber = 0;
	if (modalStage.numChildren < 2){
		cardNumber = 1;
	}else{
		cardNumber = modalStage.numChildren;
	}
	var card = drawCombatCard(serverAction);
	card.y = 5;
	card.x = (cardNumber - 1) * 210 + 5;
	setModalMenuSize(cardNumber * 210, 320);
	modalStage.addChild(card);
	modalStage.update();
}

/**partie gérant les actions faites par le joueur pendant le tour**/
function sendFactionChoice(element){
	if (currentTurn){
		var clientAction = {
				action : "chooseFaction",
				speciesName: element.getAttribute("speciesName"),
				factionName: element.getAttribute("factionName")
		};
		webSocket.send(JSON.stringify(clientAction));
	}
}

function sendFirstPlayerChoice(playerName){
	if (currentTurn){
		var clientAction = {
				action : "sendFirstPlayerChoice",
				name: playerName
		};
		webSocket.send(JSON.stringify(clientAction));
	}
}


function askActiveCard(){
	clearModalMenu();
	var clientAction = {
			action : "askActiveCard",
	};
	webSocket.send(JSON.stringify(clientAction));
	showModalMenu();
}

function askActiveCard2(playerName){
	clearModalMenu();
	var clientAction = {
			action : "askActiveCard2",
			name : playerName
	};
	webSocket.send(JSON.stringify(clientAction));
	showModalMenu();
}

function askCombatCardHand(){
	clearModalMenu();
	var clientAction = {
			action : "askCombatCardHand",
	};
	webSocket.send(JSON.stringify(clientAction));
	showModalMenu();
}

/*dessins*/
function drawHiddenCard(serverAction){
	var card = new createjs.Container();
	var backGround = new createjs.Shape();
	backGround.graphics.beginFill(serverAction.color).beginStroke("black").setStrokeStyle(8).drawRect(0, 0, cardWidth, cardHeight);
	backGround.graphics.endStroke().endFill();
	card.addChild(backGround);
	return card;
}

function drawCombatCard(serverAction){
	var card = new createjs.Container();
	card.set({name : "card"+serverAction.id});
	//image d'un point d'ordre spécial
	var backGround = new createjs.Shape();
	backGround.graphics.beginFill("#F5F5DC").beginStroke(serverAction.color).setStrokeStyle(8).drawRect(0, 0, cardWidth, cardHeight);
	backGround.graphics.endStroke().endFill();
	
	card.addChild(backGround);
	
	var margin1 = 10;
	
	if (serverAction.maxAttack || serverAction.maxDefense){
		margin1 = 50;
		//affichage des statistiques de combats si elles sont présentes
		var maxAttack = new createjs.Text(serverAction.maxAttack, "40px Arial", "black");
		maxAttack.set({
		    textAlign: 'center',
		    x: 20,
		    y: 0 
		});
		card.addChild(maxAttack);
		
		var maxDefense = new createjs.Text(serverAction.maxDefense, "40px Arial", "black");
		maxDefense.set({
		    textAlign: 'center',
		    x: 180,
		    y: 0 
		});
		card.addChild(maxDefense);
		
		var minAttack = new createjs.Text(serverAction.minAttack, "30px Arial", "black");
		minAttack.set({
		    textAlign: 'center',
		    x: 50,
		    y: 10 
		});
		card.addChild(minAttack);
		
		var minDefense = new createjs.Text(serverAction.minDefense, "30px Arial", "black");
		minDefense.set({
		    textAlign: 'center',
		    x: 150,
		    y: 10 
		});
		card.addChild(minDefense);
	}
	if (serverAction.images){
		var imageList = serverAction.images;
		
		for(var i in imageList){
			var imageName = imageList[i].image;
			if (imageName !== ""){
				if (!(imageName in unitImages)){
					var image = new Image();
				    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ imageName;
				    image.onload = function() {
				    	//console.log("image loaded");
				    	modalStage.update();
				    	if (stage){
				    		stage.update();
				    		if (combatStage){
				    			combatStage.update();
				    		}
				    	}
					}
				    unitImages[imageName]=image;
				}
				var unitImage = new createjs.Bitmap(unitImages[imageName]);
				unitImage.x = 25 + i%3 * 50;
				unitImage.y = margin1 + Math.floor(i/3) * 50;
				unitImage.set({tooltip : imageList[i].tooltip});
				addTooltipEvent(unitImage);
				card.addChild(unitImage);
			}
		}
	}
	var margin2 = 0;
	if (imageList){
		margin2 = (Math.floor((imageList.length - 1)/3) + 1) * 50;
	}
	
	var margin3 = 0;
	if (serverAction.name){
		margin3 = 50
		var cardTitle = new createjs.Text(serverAction.name, "22px Arial", "black");
		cardTitle.set({
		    textAlign: 'center',
		    x: 100,
		    y: margin1 + margin2,
		    lineWidth : 180
		});
		card.addChild(cardTitle);
		card.set({type : serverAction.name});
	}
	
	var cardText = new createjs.Text(serverAction.text, "18px Arial", "black");
	cardText.set({
	    textAlign: 'center',
	    x: 100,
	    y: margin1 + margin2 + margin3,
	    lineWidth : 180
	});
	card.addChild(cardText);
	
	if (serverAction.mineralCost){
		var mineralCost = drawResourceWithText("mineral", serverAction.mineralCost);
		mineralCost.x = 5;
		mineralCost.y = 255;
		card.addChild(mineralCost);
	}
	
	if (serverAction.gasCost){
		var gasCost = drawResourceWithText("gas", serverAction.gasCost);
		gasCost.x = 155;
		gasCost.y = 255;
		card.addChild(gasCost);
	}
	
	return card;
}