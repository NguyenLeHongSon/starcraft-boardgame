


/*actions joueurs*/
//---------------------------------------------------------------------------------------
function askActiveEventCard(){
	clearModalMenu();
	var clientAction = {
			action : "askActiveEventCard",
	};
	webSocket.send(JSON.stringify(clientAction));
	showModalMenu();
}

function askActiveEventCard2(playerName){
	clearModalMenu();
	var clientAction = {
			action : "askActiveEventCard2",
			name : playerName
	};
	webSocket.send(JSON.stringify(clientAction));
	showModalMenu();
}

function addBuildBuidingTurn(){
	var clientAction = {
			action : "addBuildBuidingTurn",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function addBuildModuleTurn(){
	var clientAction = {
			action : "addBuildModuleTurn",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function drawAllEventCards(){
	clearModalMenu();
	var clientAction = {
			action : "drawAllEventCards",
	};
	webSocket.send(JSON.stringify(clientAction));
	showModalMenu();
}

function askEventCardUse(card){
	clearEventAndHighlight();
	var clientAction = {
			action : "askEventCardUse",
			cardId : card.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function askStartOfBattleCardUse(card){
	clearEventAndHighlight();
	var clientAction = {
			action : "askStartOfBattleCardUse",
			cardId : card.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function askDrawEventCard(card){
	clearEventAndHighlight();
	var clientAction = {
			action : "askDrawEventCard",
			cardId : card.name
	};
	webSocket.send(JSON.stringify(clientAction));
	hideModalMenu();
}

function addFreeUnits(area){
	clearEventAndHighlight();
	var clientAction = {
			action : "addFreeUnits",
			areaId : area.name.toString(),
			coordinates : area.parent.parent.parent.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

//---------------------------------------------------------------------------------------
/*fin actions joueurs*/

/*ajout d'évènements*/
//---------------------------------------------------------------------------------------
function addEventCardUse(card){
	if (currentTurn &&  !card.hasEventListener("click")){
		addObjectWithEvents(card);
		card.addEventListener("click", function(event) {
			askEventCardUse(card);
		})
	}
}

function addStartOfBattleCardUse(card){
	if (currentTurn &&  !card.hasEventListener("click")){
		addObjectWithEvents(card);
		card.addEventListener("click", function(event) {
			askStartOfBattleCardUse(card);
		})
	}
}

function addDrawEventCardEvent(card){
	if (currentTurn &&  !card.hasEventListener("click")){
		addObjectWithEvents(card);
		card.addEventListener("click", function(event) {
			askDrawEventCard(card);
		})
	}
}

function addFreeUnitsEvent(area){
	addObjectWithEvents(area);
	if (currentTurn &&  !area.hasEventListener("click")){
		area.addEventListener("mouseover", function(event) {
			area.alpha = 0.5;
			galaxyStage.update(event);
		})

		area.addEventListener("mouseout", function(event) { 
			area.alpha = 1;
			galaxyStage.update(event);
		})

		area.addEventListener("click", function(event) { 
			addFreeUnits(area);
			area.alpha = 1;
		})
	}
}

//---------------------------------------------------------------------------------------
/*fin ajout d'évènements*/

/*réception des messages du serveur*/
//---------------------------------------------------------------------------------------
function activeDrawingEventCards(serverAction){
	for (var i =0; i < modalStage.numChildren; i++){
		var card = modalStage.getChildAt(i);
		if (card.name.toString().indexOf("card") > -1){
			addDrawEventCardEvent(card);
		}
	}
}

/**indique le nombre de carte actives pour le joueur et lui permet d'afficher ces cartes**/
function displayActiveEventCardNumber(serverAction){
	var text = document.getElementById("activeEventCardNumber");
	if (text){
		text.innerHTML = serverAction.cardNumber;
		var button = document.getElementById("activeEventCards");
		if (serverAction.cardNumber === 0){
			button.disabled = true;
		}else{
			button.disabled = false;
		}
	}
}

/**indique le nombre de carte actives pour le joueur et lui permet d'afficher ces cartes**/
function displayActiveEventCardNumber2(serverAction){
	var text = document.getElementById("activeEventCardNumber_" + serverAction.playerName);
	if (text){
		text.innerHTML = serverAction.cardNumber;
		var button = document.getElementById("activeEventCards_" + serverAction.playerName);
		if (serverAction.cardNumber === 0){
			button.disabled = true;
		}else{
			button.disabled = false;
		}
	}
}

function updateDrawnEventCardNumber(serverAction){
	var text = document.getElementById("eventCardNumber");
	if (text){
		text.innerHTML = serverAction.cardNumber;
	}
}

function updateDrawnEventCardNumber2(serverAction){
	var text = document.getElementById("eventCardNumber_" + serverAction.playerName);
	if (text){
		text.innerHTML = serverAction.cardNumber;
	}
}


function activateEventCards(serverAction){
	for (var i =0; i < stage.numChildren; i++){
		var card = stage.getChildAt(i);
		if (card.name.toString().indexOf("card") > -1){
			addEventCardUse(card);
		}
	}
}

function activateStartOfBattleCards(serverAction){
	for (var i =0; i < stage.numChildren; i++){
		var card = stage.getChildAt(i);
		if (card.name.toString().indexOf("card") > -1){
			addStartOfBattleCardUse(card);
		}
	}
}

function addSkipTurnButton(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	//bouton de fin de tour
	var endTurnButton =  document.createElement("button");
	endTurnButton.setAttribute("id", "endTurnButton");
	endTurnButton.setAttribute("class", "gameButton");
	endTurnButton.setAttribute("type", "button");
	endTurnButton.setAttribute("onclick", "this.disabled=true;skipTurn();");
	endTurnButton.innerHTML = "skip step";
	
	actionMenu.appendChild(endTurnButton);
}

function addChooseBuildingButton(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	//bouton de fin de tour
	var buildBuidingButton =  document.createElement("button");
	buildBuidingButton.setAttribute("id", "endTurnButton");
	buildBuidingButton.setAttribute("class", "gameButton");
	buildBuidingButton.setAttribute("type", "button");
	buildBuidingButton.setAttribute("onclick", "this.disabled=true;addBuildBuidingTurn();");
	buildBuidingButton.innerHTML = "unlock new units";
	
	var buildModuleButton =  document.createElement("button");
	buildModuleButton.setAttribute("id", "endTurnButton");
	buildModuleButton.setAttribute("class", "gameButton");
	buildModuleButton.setAttribute("type", "button");
	buildModuleButton.setAttribute("onclick", "this.disabled=true;addBuildModuleTurn();");
	buildModuleButton.innerHTML = "build module";
	
	actionMenu.appendChild(buildBuidingButton);
	actionMenu.appendChild(buildModuleButton);
}

function askFreeUnitsPlacements(serverAction){
	var coordinates = serverAction.coordinates;
	for (var i in coordinates){
		var area = galaxyStage.getChildByName(coordinates[i].coordinate)
		.getChildAt(1).getChildByName("areaSet").getChildByName(coordinates[i].areaId);
		addFreeUnitsEvent(area);
	}
}

//---------------------------------------------------------------------------------------
/*fin réception des messages du serveur*/

/*dessins*/
//---------------------------------------------------------------------------------------

function drawHTMLEventCard(serverAction){
	var factionInfo = document.getElementById("currentPlayerBoard");
	
	var strategyCard = document.createElement("strategyCard");
	strategyCard.setAttribute("id", "strategyCard");
	strategyCard.setAttribute("class", "playerInfoBoard");
	strategyCard.style.borderColor = serverAction.color;
	
	var strategyCardTitle = document.createElement("strategyCardTitle");
	strategyCardTitle.setAttribute("id", "strategyCardTitle");
	strategyCardTitle.innerHTML = serverAction.name;
	
	var strategyCardText = document.createElement("strategyCardText");
	strategyCardText.setAttribute("id", "strategyCardText");
	strategyCardText.innerHTML = serverAction.text;
	
	strategyCard.appendChild(strategyCardTitle);
	strategyCard.appendChild(strategyCardText);
	factionInfo.appendChild(strategyCard);
}