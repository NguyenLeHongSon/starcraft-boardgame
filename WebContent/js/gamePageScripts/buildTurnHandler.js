
/*actions joueurs*/
//---------------------------------------------------------------------------------------

function cancelBuyOrder(){
	placedUnitChoice = 0;
	validAreas.length = 0;
	disactivateBaseResources();
	removePriceDisplay();
	var clientAction = {
			action : "cancelBuyOrder",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function endBuildingUnitTurn(){
	clearEventAndHighlight();
	validAreas.length = 0;
	var clientAction = {
			action : "endBuildingUnitTurn",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function setWorkerOnArea(area){
	removePriceDisplay();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "setWorkerOnArea",
			areaId : area.name.toString(),
			coordinates : area.parent.parent.parent.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function sacrificeUnit(unit){
	removePriceDisplay();
	clearEventAndHighlight();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "sacrificeUnit",
			unitId : unit.name.toString()
	};
	webSocket.send(JSON.stringify(clientAction));
}


function useResourceToken(resource){
	removePriceDisplay();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "useResourceToken",
			resourceType : resource
	};
	webSocket.send(JSON.stringify(clientAction));
}

function setWorkerOnBaseMineral(){
	removePriceDisplay();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "setWorkerBaseMineral"
	};
	webSocket.send(JSON.stringify(clientAction));
}

function setWorkerOnBaseGas(){
	removePriceDisplay();
	validAreas.length = 0;
	disactivateBaseResources();
	var clientAction = {
			action : "setWorkerBaseGas"
	};
	webSocket.send(JSON.stringify(clientAction));
}


function askBuyingOrder(unit){
	clearEventAndHighlight();
	var clientAction = {
			action : "askBuyingOrder",
			name : unit.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function askBuyingBuildingOrder(building){
	clearEventAndHighlight();
	var clientAction = {
			action : "askBuyingBuildingOrder",
			name : building.name
	};
	webSocket.send(JSON.stringify(clientAction));
}

function askBuyingModuleOrder(module){
	clearEventAndHighlight();
	var clientAction = {
			action : "askBuyingModuleOrder",
			name : module.name
	};
	webSocket.send(JSON.stringify(clientAction));
}
//---------------------------------------------------------------------------------------
/*fin actions joueurs*/

/*ajout d'évènements*/
//---------------------------------------------------------------------------------------

/**sélectionne une zone où l'on va chercher des ressources**/
function addGetResource(area){
	addObjectWithEvents(area);
	if (currentTurn &&  !area.hasEventListener("click")){
		area.addEventListener("mouseover", function(event) {
			if (isValidResourcePlace(area)){
				area.alpha = 0.5;
				galaxyStage.update(event);
			}
		})

		area.addEventListener("mouseout", function(event) { 
			if (isValidResourcePlace(area)){
				area.alpha = 1;
				galaxyStage.update(event);
			}
		})

		area.addEventListener("click", function(event) { 
			if (isValidResourcePlace(area)){
				setWorkerOnArea(area);
				area.alpha = 1;
			}
		})
	}
}

/**sélectionne une unité à acheter**/
function addBuyUnitSelectionEvent(unit){
	if (currentTurn &&  !unit.hasEventListener("click")){
		addObjectWithEvents(unit);
		unit.addEventListener("click", function(event) {
			askBuyingOrder(unit);
		})
	}
}

/**sacrifie une unité**/
function addSacrificeEvent(unit){
	if (currentTurn &&  !unit.hasEventListener("click")){
		addObjectWithEvents(unit);
		unit.addEventListener("click", function(event) {
			sacrificeUnit(unit);
		})
	}
}

/**sélectionne un batiment à acheter**/
function addBuyBuildingEvent(building){
	if (currentTurn &&  !building.hasEventListener("click")){
		addObjectWithEvents(building);
		building.addEventListener("click", function(event) {
			askBuyingBuildingOrder(building);
		})
	}
}

/**sélectionne un batiment à acheter**/
function addBuyModuleEvent(module){
	if (currentTurn &&  !module.hasEventListener("click")){
		addObjectWithEvents(module);
		module.addEventListener("click", function(event) {
			askBuyingModuleOrder(module);
		})
	}
}
//---------------------------------------------------------------------------------------
/*fin ajout d'évènements*/


/*réception des messages du serveur*/
//---------------------------------------------------------------------------------------


function displayUnlockedModules(serverAction){
	if (stage.getChildByName("backGround")){
		stage.removeChild(stage.getChildByName("backGround"));
	}
	var moduleList = serverAction.moduleList;
	stage.canvas.height = (Math.floor((moduleList.length-1) / 3) + 2) * 100;
	var backGround = new createjs.Shape();
	backGround.set({name : "backGround"});
	backGround.graphics.beginFill("#CBFDCB").drawRect(0, 0, 300, stage.canvas.height - 100);
	backGround.y = 100;
	stage.addChild(backGround);
	for (var i in moduleList){
		var moduleImage = drawModule(moduleList[i]);
		moduleImage.x = (i%3) * 100 + 10;
		moduleImage.y = Math.floor(i / 3) * 100 + 10 + 100;
		stage.addChild(moduleImage);
	}
	stage.update();
}

function activateResourceUnits(serverAction){
	var unitList = serverAction.unitList;
	for (var i in unitList){
		var unit = galaxyStage.getChildByName("unitContainer").getChildByName(unitList[i].unitId);
		addSacrificeEvent(unit);
	}
}

function showRequiredResources(serverAction){
	var requiredMineral = drawResourceWithText("mineral", serverAction.mineral);
	var requiredGas = drawResourceWithText("gas", serverAction.gas);
	requiredMineral.set({name : "requiredMineral"});
	requiredGas.set({name : "requiredGas"});
	requiredMineral.x = 10;
	requiredGas.x = 110;
	requiredMineral.y = 10;
	requiredGas.y = 10;
	stage.addChild(requiredMineral);
	stage.addChild(requiredGas);
	if (serverAction.image){
		var image = drawUnitImage(serverAction);
		var requiredUnits = drawResourceWithText("unit", serverAction.unitCost, image);
		requiredUnits.set({name : "requiredUnits"});
		requiredUnits.x = 210;
		requiredUnits.y = 10;
		stage.addChild(requiredUnits);
	}
	stage.update();
}

function activateUnlockedBuildings(serverAction){
	for (var i =0; i < stage.numChildren; i++){
		var building = stage.getChildAt(i);
		if (building.type === "building"){
			addBuyBuildingEvent(building);
		}
	}
}

function activateUnlockedModules(serverAction){
	for (var i =0; i < stage.numChildren; i++){
		var module = stage.getChildAt(i);
		if (module.name.indexOf("module") > -1){
			addBuyModuleEvent(module);
		}
	}
}

function displayUnlockedBuildings(serverAction){
	if (stage.getChildByName("backGround")){
		stage.removeChild(stage.getChildByName("backGround"));
	}
	//mise en place du fond du canvas
	var canvasHeight = 3;
	stage.canvas.height = canvasHeight * 90 + 10 + 100;
	var backGround = new createjs.Shape();
	backGround.set({name : "backGround"});
	backGround.graphics.beginFill("#CBFDCB").drawRect(0, 0, 300, stage.canvas.height - 100);
	backGround.y = 100;
	stage.addChild(backGround);
	//affichage des batiments
	var buildingList = serverAction.buildingList;
	for (var i in buildingList){
		var building = displayBuilding(buildingList[i]);
		building.x = 5 + 100 * i;
		building.y = 5 + 100;
		stage.addChild(building);
	}
	stage.update();
}


function setOnOffCancelBuyOrder(serverAction){
	var cancelBuyOrder = document.getElementById("cancelBuyOrder");
	if (cancelBuyOrder){
		cancelBuyOrder.disabled = serverAction.disabledButton;
	}
}


/**ajoute les boutons correspondant aux actions possibles pendant cette étape**/
function addBuildingUnitTurnButton(serverAction){
	
	var actionMenu = document.getElementById("buttonContainer");
	
	//bouton de fin de tour
	var endTurnButton =  document.createElement("button");
	endTurnButton.setAttribute("id", "endTurnButton");
	endTurnButton.setAttribute("class", "gameButton");
	endTurnButton.setAttribute("type", "button");
	endTurnButton.setAttribute("onclick", "this.disabled=true;endBuildingUnitTurn();");
	endTurnButton.innerHTML = "Next step";
	if (!currentTurn){
		endTurnButton.disabled = true;
	}
	actionMenu.appendChild(endTurnButton);
	
	var cancelBuyOrder =  document.createElement("button");
	cancelBuyOrder.setAttribute("id", "cancelBuyOrder");
	cancelBuyOrder.setAttribute("class", "gameButton");
	cancelBuyOrder.setAttribute("type", "button");
	cancelBuyOrder.setAttribute("onclick", "this.disabled=true;cancelBuyOrder();");
	cancelBuyOrder.innerHTML = "Cancel order";
	cancelBuyOrder.disabled = true;
	actionMenu.appendChild(cancelBuyOrder);
}

function setUnitAlpha(serverAction){
	var unit = stage.getChildByName(serverAction.name);
	if (unit){
		unit.alpha = serverAction.alpha;
		stage.update();
	}
}

function activateMineralToken(serverAction){
	var baseMineral = document.getElementById("mineralToken");
	baseMineral.addEventListener("click", sendUseMineralToken);
	baseMineral.setAttribute("onmouseover", "setBackGround(this, 'green')");
	baseMineral.setAttribute("onmouseout", "setBackGround(this, 'rgba(0, 0, 0, 0)')");
}

function activateGasToken(serverAction){
	var baseMineral = document.getElementById("gasToken");
	baseMineral.addEventListener("click", sendUseGasToken);
	baseMineral.setAttribute("onmouseover", "setBackGround(this, 'green')");
	baseMineral.setAttribute("onmouseout", "setBackGround(this, 'rgba(0, 0, 0, 0)')");
}

function activateBaseMineral(serverAction){
	var baseMineral = document.getElementById("baseMineral");
	baseMineral.addEventListener("click", sendSetWorkerOnBaseMineral);
	baseMineral.setAttribute("onmouseover", "setBackGround(this, 'green')");
	baseMineral.setAttribute("onmouseout", "setBackGround(this, 'rgba(0, 0, 0, 0)')");
	keepTabOpen('gameBoardLink', 'gameBoardTab', 'gametabcontent');
}

function activateBaseGas(serverAction){
	var baseGas = document.getElementById("baseGas");
	baseGas.addEventListener("click", sendSetWorkerOnBaseGas);
	baseGas.setAttribute("onmouseover", "setBackGround(this, 'green')");
	baseGas.setAttribute("onmouseout", "setBackGround(this, 'rgba(0, 0, 0, 0)')");
	keepTabOpen('gameBoardLink', 'gameBoardTab', 'gametabcontent');
}

function addResourcePlacesEvent(serverAction){
	var coordinates = serverAction.coordinates;
	for (var i in coordinates){
		var area = galaxyStage.getChildByName(coordinates[i].coordinate)
		.getChildAt(1).getChildByName("areaSet").getChildByName(coordinates[i].areaId);
		addGetResource(area);
	}
}


function activateResourceAreas(serverAction){
	var coordinates = serverAction.coordinates;
	for(var i in coordinates){
		var connexion  = {
				coordinates: coordinates[i].coordinate,
				areaId: coordinates[i].areaId
				};
		validAreas.push(connexion);
	}
}

/**ajoute les boutons correspondant aux actions possibles pendant cette étape**/
function displayUnlockedUnits(serverAction){
	if (stage.getChildByName("backGround")){
		stage.removeChild(stage.getChildByName("backGround"));
	}
	var unitList = serverAction.unitList;
	stage.canvas.height = (Math.floor((unitList.length-1) / 3) + 2) * 100;
	var backGround = new createjs.Shape();
	backGround.set({name : "backGround"});
	backGround.graphics.beginFill("#CBFDCB").drawRect(0, 0, 300, stage.canvas.height - 100);
	backGround.y = 100;
	stage.addChild(backGround);
	for (var i in unitList){
		var unit = drawUnitImage(unitList[i]);
		unit.scaleX = 2;
		unit.scaleY = 2;
		unit.x = (i%3) * 100 + 10;
		unit.y = Math.floor(i / 3) * 100 + 10 + 100;
		stage.addChild(unit);
	}
	stage.update();
}

function activateUnlockedUnits(serverAction){
	var unitList = serverAction.unitList;
	for (var i in unitList){
		var unit = stage.getChildByName(unitList[i].name);
		addBuyUnitSelectionEvent(unit);
	}
}
//---------------------------------------------------------------------------------------
/*fin réception des messages du serveur*/


//TODO
/*fonctions de dessin*/
//---------------------------------------------------------------------------------------
/**dessine l'image d'une unité**/
function drawUnitImage(serverAction){
	if (!(serverAction.image in unitImages)){
		var image = new Image();
	    image.src = "../starcraftWebResources/unitImages/"+ serverAction.species + "/"+ serverAction.image;
	    image.onload = function() {
	    	//console.log("image loaded");
			stage.update();
			galaxyStage.update();
		}
	    unitImages[serverAction.image]=image;
	}
	var unit = new createjs.Container();
    
	var unitImage = new createjs.Bitmap(unitImages[serverAction.image]);

	unit.set({name : serverAction.name});
	unit.set({displayType : "unit"});
	//position dans une zone ou route
	
	var circle = new createjs.Shape();
	circle.graphics.beginFill(serverAction.color).drawCircle(20, 20, 20);
	unit.addChild(circle);
	unit.addChild(unitImage);
	if (serverAction.tooltip){
		unit.set({tooltip : serverAction.tooltip});
	}else{
		unit.set({tooltip : serverAction.name});
	}
	addTooltipEvent(unit);
	return unit;
}

function displayBuilding(building){
	var buildingImage = new createjs.Container();
	buildingImage.set({name : building.number.toString() + "." +  building.level.toString()});
	buildingImage.set({type : "building"});
	var unitList = building.unitList;
	var height = unitList.length;
	//fond d'écran
	var square = new createjs.Shape();
	square.graphics.beginFill(building.color).drawRect(0, 0, 90, 90 * height);
	buildingImage.addChild(square);
	//unités débloquées
	for (var i in unitList){
		if (!(unitList[i].image in unitImages)){
			var image = new Image();
		    image.src = "../starcraftWebResources/unitImages/"+ building.species + "/"+ unitList[i].image;
		    image.onload = function() {
		    	//console.log("image loaded");
				stage.update();
			}
		    unitImages[unitList[i].image]=image;
		}
	    var unitImage = new createjs.Bitmap(unitImages[unitList[i].image]);
	    unitImage.x = 5;
	    unitImage.y = 5 + 90* i;
	    unitImage.scaleX = 2;
	    unitImage.scaleY = 2;
	    unitImage.set({tooltip : unitList[i].tooltip});
	    addTooltipEvent(unitImage);
	    buildingImage.addChild(unitImage);
	}
	return buildingImage;
}

function drawModule(serverAction){
	var imagePath = serverAction.faction + "/"+ serverAction.image;
	
	if (!(imagePath in unitImages)){
		var image = new Image();
	    image.src = "../starcraftWebResources/moduleImages/"+ imagePath;
	    image.onload = function() {
	    	//console.log("image loaded");
			stage.update();
			galaxyStage.update();
		}
	    unitImages[imagePath]=image;
	}
	var unit = new createjs.Container();
    
	var unitImage = new createjs.Bitmap(unitImages[imagePath]);

	unit.set({name : "module" + serverAction.id});
	
	unitImage.set({displayType : "module"});
	unitImage.set({tooltip : serverAction.tooltip});
	addTooltipEvent(unitImage);
	
	unit.addChild(unitImage);
	return unit;
}


//TODO
/**regarde si la route ou la zone sont des endroits valides pour placer une unité**/
function isValidResourcePlace(area){
	var result = false;
	for (var i = 0; i < validAreas.length; i++){
		if (validAreas[i].coordinates === area.parent.parent.parent.name && validAreas[i].areaId === area.name){
			result=true;
			break;
		}
	}
	return result;
}

function removePriceDisplay(){
	if (stage.getChildByName("requiredMineral")){
		stage.removeChild(stage.getChildByName("requiredMineral"));
	}
	if (stage.getChildByName("requiredGas")){
		stage.removeChild(stage.getChildByName("requiredGas"));
	}
	if (stage.getChildByName("requiredUnits")){
		stage.removeChild(stage.getChildByName("requiredUnits"));
	}
	stage.update();
}