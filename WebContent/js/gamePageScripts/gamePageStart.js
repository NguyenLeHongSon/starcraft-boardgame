var webSocket = new WebSocket("wss://" + location.host + "/starcraftBoardGame/gamePageLobby");
var currentTurn = false;
var currentPlayer = "";
var playerColor = "";
var startTimer;
var endTimer;
var unitImages = [];
var modalStage = new createjs.Stage("infoStage");

var cardHeight = 300;
var cardWidth = 200;
//taille de la fenêtre en pixels
var actualWidth = window.innerWidth ||
document.documentElement.clientWidth ||
document.body.clientWidth ||
document.body.offsetWidth;

var actualHeight = window.innerHeight ||
document.documentElement.clientHeight ||
document.body.clientHeight ||
document.body.offsetHeight;

//couleur des textes
var textOutlineColor = "#7A5230";
var textColor = "#BDBDBD";

function skipTurn(){
	var clientAction = {
			action : "skipTurn",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function endNormalTurn(){
	var clientAction = {
			action : "endNormalTurn",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function addEndNormalTurnButton(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	//bouton de fin de tour
	var endTurnButton =  document.createElement("button");
	endTurnButton.setAttribute("id", "endTurnButton");
	endTurnButton.setAttribute("class", "gameButton");
	endTurnButton.setAttribute("type", "button");
	endTurnButton.setAttribute("onclick", "this.disabled=true;endNormalTurn();");
	endTurnButton.innerHTML = "skip step";
	
	actionMenu.appendChild(endTurnButton);
}

function setupPlayerInfoOptions(serverAction){
	var factionInfo = document.getElementById("playerInfoSelection");
	
	var factionOption = document.createElement("option");
	factionOption.setAttribute("value", serverAction.playerName);
	factionOption.innerHTML = serverAction.playerName;
	
	factionInfo.appendChild(factionOption);
	
	var otherPlayerBoards = document.getElementById("otherPlayerBoards");
	
	var otherPlayerInfo = document.createElement("div");
	otherPlayerInfo.setAttribute("class", " otherPlayerInfo");
	otherPlayerInfo.setAttribute("id", "info_"+ serverAction.playerName);
	
	otherPlayerBoards.appendChild(otherPlayerInfo);
	displayOtherPlayerInfo(serverAction.playerName);
	factionInfo.value = serverAction.playerName;
}

function displaySelectedPlayerInfo(elem){
	displayOtherPlayerInfo(elem.value);
}

function displayOtherPlayerInfo(playerName){
	var playerInfo = document.getElementsByClassName("otherPlayerInfo");
    for (i = 0; i < playerInfo.length; i++) {
    	if ("info_"+playerName === playerInfo[i].id){
    		playerInfo[i].style.display = "block";
    	}else{
    		playerInfo[i].style.display = "none";
    	}
    }
}

function keepTabOpen(newElement, elementName, className) {
    // Declare all variables
    var i, tabcontent, tablinks;
    
	// Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    tabcontent = document.getElementsByClassName(className);
    for (i = 0; i < tabcontent.length; i++) {
    	if (tabcontent[i].id === elementName){
    		var element = tabcontent[i];
    		element.style.display = "block";
    		document.getElementById(newElement).className += " active";
    	}else{
    		tabcontent[i].style.display = "none";
    	} 
    } 
}

function openTab(newElement, elementName, className) {
    // Declare all variables
    var i, tabcontent, tablinks;
    
	// Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    tabcontent = document.getElementsByClassName(className);
    for (i = 0; i < tabcontent.length; i++) {
    	if (tabcontent[i].id === elementName){
    		var element = tabcontent[i];
    	    if (element.style.display === "block"){
    	    	element.style.display = "none";
    	    }else{
    	    	element.style.display = "block";
    	        document.getElementById(newElement).className += " active";
    	    }
    	}else{
    		tabcontent[i].style.display = "none";
    	} 
    } 
}

var gameChatFlashing;

function gameChatFlash() {
    var gameChatLink = document.getElementById("gameChatLink");
    gameChatLink.style.backgroundColor = (gameChatLink.style.backgroundColor == "red") ? "#f1f1f1" : "red";
}

function openGameChatTab() {
	openTab('gameChatLink', 'gameChatTab', 'gametabcontent');
	clearInterval(gameChatFlashing);
	gameChatFlashing = undefined;
	var gameChatLink = document.getElementById("gameChatLink");
    gameChatLink.style.backgroundColor = "#f1f1f1";
}


function cloneImage(object, image) {
    imageClone = new Image();
    imageClone.src = image.src;
    object.appendChild(imageClone);
}

function setBackGround(object, color){
	object.style.background = color;
}

function setModaBackground(width, length){
modalStage.removeChild(modalStage.getChildByName("backGround"));
var backGround;
backGround = new createjs.Shape();
backGround.set({name : "backGround"});
backGround.graphics.beginFill("#CBFDCB").drawRect(0, 0, width,length);
modalStage.addChild(backGround);
modalStage.setChildIndex( backGround, 0);
}

function sendHelpMessage(serverAction) {
	var helpDiv = document.getElementById("helpText");
	helpDiv.innerHTML = serverAction.message;
	helpDiv.style.background = serverAction.color;
	helpDiv.style.display = "block";
}

function switchHelpMenuView() {
	var helpDiv = document.getElementById("helpText");
	if (helpDiv.style.display === "block"){
		helpDiv.style.display = "none";
	}else{
		helpDiv.style.display = "block";
	}
}



function startTimeMeasure(serverAction){
	startTimer = new Date().getTime();
}

function endTimeMeasure(serverAction){
	endTimer = new Date().getTime();
	var time = endTimer - startTimer;
	console.log(time);
}

function clearModalMenu(){
	for (var i = 0; i < modalStage.numChildren; i++){
		if (modalStage.getChildAt(i).name !== "backGround"){
			modalStage.removeChildAt(i);
			i--;
		}
	}
}

function clearViewByClass(serverAction){
	var currentGameList = document.getElementsByClassName(serverAction.name);
    while(currentGameList.length > 0){
    	currentGameList[0].parentNode.removeChild(currentGameList[0]);
    }
}


function deleteElement(serverAction){
	var element = document.evaluate( serverAction.xpath ,document.body , null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;
	if (element != null) {
	  element.parentNode.removeChild(element);
	}
}

function playerTurnUpdate(serverAction){
	currentPlayer = serverAction.currentPlayer;
    if (serverAction.isCurrentPlayerTurn === "true") {
    	if (currentTurn !== true){
    		currentTurn = true;
    		alert("It's your turn to play");
    	}
    } else {
    	currentTurn = false;
    }
}

function showWinner(serverAction){
	alert("The winner is " + serverAction.name);
}

function setModalMenuSize(width, height){
	if (width !== modalStage.canvas.width || height !== modalStage.canvas.height){
		var infoStage = document.getElementById("infoStageContainer");
		modalStage.canvas.width = width;
		modalStage.canvas.height = height;
		setModaBackground(width, height);
		if (actualWidth*0.8 <width){
			infoStage.style.width = actualWidth*0.8 +"px";
		}else{
			infoStage.style.width = width + "px";
		}
	}
}

/****/
function hideModalMenu(){
	var modalBackGround = document.getElementById("modalBackGround");
	var infoStage = document.getElementById("infoStageContainer");
	modalBackGround.style.display = "none";
	infoStage.style.display = "none";
}

/****/
function showModalMenu(){
	var modalBackGround = document.getElementById("modalBackGround");
	var infoStage = document.getElementById("infoStageContainer");
	modalBackGround.style.display = "block";
	infoStage.style.display = "block";
}

//fonction faisant apparaitre le menu des actions et la le plateau de la galaxie où on place planètes et unités
function updateBoardView(serverAction){
	
	var gameBoard = document.getElementById("gameBoard");
	var boardHeight = parseInt(gameBoard.offsetHeight, 10);
	var boardWidth = parseInt(gameBoard.offsetWidth, 10);
	
	var actionMenu = document.createElement("div");
	actionMenu.setAttribute("id", "actionMenu");
	actionMenu.setAttribute("class", "actionMenu");
	var actionMenuHeight = boardHeight - 50;
	actionMenu.style.height = actionMenuHeight + "px";
	
	gameBoard.appendChild(actionMenu);
	
	var actionMenuWidth = parseInt(actionMenu.offsetWidth, 10);
	
	var galaxyBoard = document.createElement("div");
	galaxyBoard.setAttribute("id", "galaxyBoard");
	galaxyBoard.setAttribute("class", "galaxyBoard");
	var galaxyBoardWidth = boardWidth - 30 - actionMenuWidth;
	var galaxyBoardHeight = boardHeight - 50 ;
	galaxyBoard.style.width = galaxyBoardWidth + "px";
	galaxyBoard.style.height = galaxyBoardHeight + "px";
	
	
	gameBoard.appendChild(galaxyBoard);
}

//ajoute les éléments où l'on place les boutons d'action et le canvas
//cela permet d'avoir une mise en page consistante
function upgradeActionMenu(serverAction){
	//menu d'action
	var actionMenu = document.getElementById("actionMenu");
	var actionMenuHeight = parseInt(actionMenu.offsetHeight, 10);
	var actionMenuWidth = parseInt(actionMenu.offsetWidth, 10);
	
	var canvasContainerHeight = actionMenuHeight - 50 ;
	var canvasContainerWidth = actionMenuWidth - 10 ;
	
	//bouton de fin de tour
	var buttonContainer =  document.createElement("div");
	buttonContainer.setAttribute("id", "buttonContainer");
	buttonContainer.setAttribute("class", "buttonContainer");
	buttonContainer.style.width = actionMenuWidth + "px";

	
	//bouton de rotation de planète
	var canvasContainer =  document.createElement("div");
	canvasContainer.setAttribute("id", "canvasContainer");
	canvasContainer.setAttribute("class", "canvasContainer");
	canvasContainer.style.width = canvasContainerWidth + "px";
	canvasContainer.style.height = canvasContainerHeight + "px";
	
	actionMenu.appendChild(buttonContainer);
	actionMenu.appendChild(canvasContainer);

}

function leaveStarcraftGame(){
	var clientAction = {
			action : "leaveStarcraftGame",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function saveStarcraftGame(){
	var clientAction = {
			action : "saveStarcraftGame",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function surrenderStarcraftGame(){
	var clientAction = {
			action : "surrenderStarcraftGame",
	};
	webSocket.send(JSON.stringify(clientAction));
}

//TODO  pioche toutes les cartes, fonction à enlever dans version finale
function drawAllCards(){
	var clientAction = {
			action : "drawAllCards",
	};
	webSocket.send(JSON.stringify(clientAction));
}

function leaveGame(serverAction){
	window.location.replace("https://" + location.host + "/starcraftBoardGame/accesMembre/gameServerList");
}


