/*actions joueurs*/
//---------------------------------------------------------------------------------------
function rechargeCard(card){
	clearEventAndHighlight();
	var clientAction = {
			action : "rechargeCard",
			name : card.name
	};
	webSocket.send(JSON.stringify(clientAction));
	var clientAction2 = {
			action : "endSupportLineTurn",
	};
	webSocket.send(JSON.stringify(clientAction2));
}

function askBuyingCardOrder(card){
	clearEventAndHighlight();
	var clientAction = {
			action : "askBuyingCardOrder",
			name : card.type
	};
	webSocket.send(JSON.stringify(clientAction));
}
//---------------------------------------------------------------------------------------
/*fin actions joueurs*/

/*ajout d'évènements*/
//---------------------------------------------------------------------------------------
/**sélectionne un batiment à acheter**/
function addBuyCardEvent(card){
	if (currentTurn &&  !card.hasEventListener("click")){
		addObjectWithEvents(card);
		card.addEventListener("click", function(event) {
			askBuyingCardOrder(card);
		})
	}
}

/**sélectionne un batiment à acheter**/
function addRechargeCardEvent(card){
	if (currentTurn &&  !card.hasEventListener("click")){
		addObjectWithEvents(card);
		card.addEventListener("click", function(event) {
			rechargeCard(card);
		})
	}
}
//---------------------------------------------------------------------------------------
/*fin ajout d'évènements*/

/*réception des messages du serveur*/
//---------------------------------------------------------------------------------------


function askResearchBonus(serverAction){
	var result = "eventCardBonus";
	var selectedBonus = document.getElementById("selectedBonus");
	if (selectedBonus){
		result = selectedBonus.value;
	}
	var clientAction = {
			action : "applyResearchBonus",
			name : result
	};
	webSocket.send(JSON.stringify(clientAction));
}

function displayResearchBonus(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	if (!document.getElementById("dropDownContainer")){
		var dropDownContainer =  document.createElement("div");
		dropDownContainer.setAttribute("id", "dropDownContainer");
		dropDownContainer.setAttribute("class", "menuItem");
		
		var selectedBonus =  document.createElement("select");
		selectedBonus.setAttribute("id", "selectedBonus");
		
		var eventCardBonus =  document.createElement("option");
		eventCardBonus.setAttribute("value", "eventCardBonus");
		eventCardBonus.innerHTML = "Draw a new event card";
		
		var combatCardBonus =  document.createElement("option");
		combatCardBonus.setAttribute("value", "combatCardBonus");
		combatCardBonus.innerHTML = "Put a bought card in your hand";
		
		selectedBonus.appendChild(eventCardBonus);
		selectedBonus.appendChild(combatCardBonus);
		
		dropDownContainer.appendChild(selectedBonus);
		
		actionMenu.appendChild(dropDownContainer);
	}
}

function displayResearchBonus2(serverAction){
	var actionMenu = document.getElementById("buttonContainer");
	if (!document.getElementById("dropDownContainer")){
		var dropDownContainer =  document.createElement("div");
		dropDownContainer.setAttribute("id", "dropDownContainer");
		dropDownContainer.setAttribute("class", "menuItem");
		
		var selectedBonus =  document.createElement("select");
		selectedBonus.setAttribute("id", "selectedBonus");
		
		var combatCardBonus =  document.createElement("option");
		combatCardBonus.setAttribute("value", "combatCardBonus");
		combatCardBonus.innerHTML = "Put a bought card in your hand";
		
		var noBonus =  document.createElement("option");
		noBonus.setAttribute("value", "noBonus");
		noBonus.innerHTML = "Don't use any bonuses";
		
		selectedBonus.appendChild(combatCardBonus);
		selectedBonus.appendChild(noBonus);
		
		dropDownContainer.appendChild(selectedBonus);
		
		actionMenu.appendChild(dropDownContainer);
	}
}

function displayCardsToRecharge(serverAction){
	var cardList = serverAction.cardList
	setCanvasPixelHeight((Math.floor(cardList.length/2) + 1) * (0.75 * cardHeight + 10));
	for (var i in cardList){
		var card = drawCombatCard(cardList[i]);
		card.scaleX = 0.75;
		card.scaleY = 0.75;
		card.y = 5 + Math.floor(i/2) * (0.75 * cardHeight + 10);
		card.x = (i%2) * (0.75 * cardWidth + 10) + 5;
		stage.addChild(card);
		addRechargeCardEvent(card)
		stage.update();
	}
}

function activateBuyableCards(serverAction){
	for (var i =0; i < stage.numChildren; i++){
		var card = stage.getChildAt(i);
		if (card.name.toString().indexOf("card") > -1){
			addBuyCardEvent(card);
		}
	}
}

function displayBuyableCards(serverAction){
	
	var cardList = serverAction.cardList
	setCanvasPixelHeight((Math.floor(cardList.length/2) + 1) * (0.75 * cardHeight + 10) + 100);
	for (var i in cardList){
		var card = drawCombatCard(cardList[i]);
		card.scaleX = 0.75;
		card.scaleY = 0.75;
		card.y = 105 + Math.floor(i/2) * (0.75 * cardHeight + 10);
		card.x = (i%2) * (0.75 * cardWidth + 10) + 5;
		stage.addChild(card);
		stage.update();
	}
}

//---------------------------------------------------------------------------------------
/*réception des messages du serveur*/
