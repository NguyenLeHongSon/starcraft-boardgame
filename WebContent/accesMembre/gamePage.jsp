<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="<c:url value="/inc/form.css"/>" />
	<link type="text/css" rel="stylesheet" href="<c:url value="/inc/gameStyle.css"/>" />
	<title>Starcraft : the boardgame</title>
</head>
<body>
	<!-- <button type="button" onclick="drawAllCards();" >Draw all cards</button>
	<button type="button" onclick="drawAllEventCards();" >Draw all event cards</button> -->
	<div id="gameBoard" class="gameBoard">
	</div>
	<div id="modalBackGround" class="modalBackGround" onClick="hideModalMenu();">
	</div>
	<div id ="infoStageContainer" class="infoStage">
	<canvas id="infoStage">
	</canvas>
	</div>
	<div id="helpButton" class="helpButton" onClick="switchHelpMenuView();">
	<img src="../starcraftWebResources/variousImages/help.png"/>
	</div>
	<div id="helpText" class="helpText">
	</div>
	
	<ul class="gameTab" id="gameTab">
	<li><a id="menuLink" class="tablinks" onclick="openTab('menuLink', 'menuTab', 'gametabcontent')">
	Menu</a></li>
	<li><a id="gameTurnLink" class="tablinks" onclick="openTab('gameTurnLink', 'gameTurnTab', 'gametabcontent')">
	Game turns</a></li>
	<li><a id="gameBoardLink" class="tablinks" onclick="openTab('gameBoardLink', 'gameBoardTab', 'gametabcontent')">
	Your game board</a></li>
	<li><a id="otherPlayersLink" class="tablinks" onclick="openTab('otherPlayersLink', 'otherPlayersTab', 'gametabcontent')">
	Other players info</a></li>
	<li><a id="gameChatLink" class="tablinks" onclick="openGameChatTab()">
	Game Chat</a></li>
	</ul>
	
	<div id="menuTab" class="gametabcontent">
		<div id="menuSlider" class="xSlider">
		<c:import url="/inc/menu.jsp" />
		</div>
	</div>
	
	<div id="gameTurnTab" class="gametabcontent">
		<div id="factionInfo" class="xSlider">
		</div>
	</div>
	
	<div id="gameBoardTab" class="gametabcontent">
		<div id="currentPlayerBoard" class="xSlider">
		</div>
	</div>
	
	<div id="otherPlayersTab" class="gametabcontent">
		<div id="otherPlayerBoards" class="xSlider">
			<select id="playerInfoSelection" onchange="displaySelectedPlayerInfo(this)">
			</select>
		</div>
	</div>
	
	<div id="gameChatTab" class="gametabcontent">
		<div>
			<input type="text" id="messageinput" autocomplete="off"/>
		</div>
		<div>
			<button type="button" onclick="send();" >Send a message</button>
		</div>
		<textarea rows="7" cols="75" id="messages" style="resize: none;" readonly></textarea>
	</div>
	
	<div id="gameTip" class="gameTip">
	</div>

</body>

<c:set var="version" scope="application" value="?127"/>

<script src="<c:url value='/js/lib/easeljs.js'/>"></script>
<script src="<c:url value='/js/gamePageScripts/gamePageStart.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/resourceHandler.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/factionChoice.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/leadershipChoice.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/galaxyScripts.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/placePlanet.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/placeZRoad.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/placeUnit.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/buildTurnHandler.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/buyResearch.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/planningPhase.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/galaxyOrderChoice.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/battleHandler.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/regroupingPhase.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/eventCardHandler.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/chatScript.js${version}'/>"></script>
<script src="<c:url value='/js/gamePageScripts/gamePageEnd.js${version}'/>"></script>

</html>