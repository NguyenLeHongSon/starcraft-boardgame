package entities;


public class Utilisateur {
    private Long      id;
    private String    motDePasse;
    private String nomUtilisateur;
    
    public Long getId() {
        return id;
    }
    public void setId( Long id ) {
        this.id = id;
    }
    
    public void setName( String nom ) {
        this.nomUtilisateur = nom;
    }
    public String getName() {
        return this.nomUtilisateur;
    }
    
    public void setPassword( String motDePasse ) {
        this.motDePasse = motDePasse;
    }
    public String getPassword() {
        return motDePasse;
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nomUtilisateur == null) ? 0 : nomUtilisateur.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utilisateur other = (Utilisateur) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nomUtilisateur == null) {
			if (other.nomUtilisateur != null)
				return false;
		} else if (!nomUtilisateur.equals(other.nomUtilisateur))
			return false;
		return true;
	}
    


}
