package forms;

public class FormValidationException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6411534935814744380L;

	/**
	 * 
	 */

	/*
     * Constructeur
     */
    public FormValidationException( String message ) {
        super( message );
    }
}