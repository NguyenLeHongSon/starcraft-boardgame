package gameEntities;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import gameEntities.gameMap.Planet;
import gameEntities.gameMap.PlanetArea;
import gameEntities.methods.GameTurnHandler;
import gameEntities.playerItems.BuyableItem;
import gameEntities.playerItems.BuyingOrder;
import gameEntities.playerItems.CombatCard;
import gameEntities.playerItems.CombatCardAbility;
import gameEntities.playerItems.OrderToken;
import gameEntities.playerItems.PlayerEventCards;
import gameEntities.playerItems.PlayerModules;
import gameEntities.playerItems.PlayerSpecialActions;
import gameEntities.playerItems.StarcraftBuilding;
import gameEntities.playerItems.StarcraftModule;
import gameEntities.playerItems.StarcraftUnit;
import gameEntities.playerItems.UnitPool;


public class StarcraftPlayer implements java.io.Serializable {
	private static final long serialVersionUID = -1695540066938569849L;
	// le nom d'une pool contenant les unités à placer au départ
	private String playerName;
	private String species;
	private String faction;
	private String factionImage;
	private String playerColor;
	private Set<String> leadershipcards = new HashSet<String>();
	private Map<String, Planet> planetDeck = new HashMap<String, Planet>();
	private Map<String, UnitPool> unitPools = new HashMap<String, UnitPool>();
	private int availableWorkers = 0;
	private int unavailableWorkers = 0;
	private ArrayList<OrderToken> ownedOrders = new ArrayList<OrderToken>();
	private Map<Integer, OrderToken> availableOrders = new HashMap<Integer, OrderToken>();
	private Boolean activeSpecialOrderBonus = false;
	private ArrayList<CombatCard> combatCardDeck = new ArrayList<CombatCard>();
	private Map<Integer, CombatCard> combatCardsInHand = new HashMap<Integer, CombatCard>();
	private Map<String, ArrayList<CombatCard>> buyableCards = new HashMap<String, ArrayList<CombatCard>>();
	private ArrayList<CombatCard> dismissedCombatCards = new ArrayList<CombatCard>();
	private int mineralResources = 4;
	private int gasResources = 2;
	private int mineralToken = 0;
	private int gasToken = 0;
	private Map<List<Integer>, PlanetArea> areaResourceList = new HashMap<List<Integer>, PlanetArea>();
	private Set<String> unlockedUnits = new HashSet<String>();
	private ArrayList<StarcraftBuilding> availableBuildings = new ArrayList<StarcraftBuilding>();
	private ArrayList<StarcraftBuilding> ownedBuildings = new ArrayList<StarcraftBuilding>();
	private BuyingOrder buyingOrder;
	private int workerOnMineral = 0;
	private int workerOnGas = 0;
	private StarcraftUnit worker;
	private int unitBuildLimit = 2;
	private int unitBuilt = 0;
	private int cardLimit = 0;
	private Map<String, CombatCard> globalCards = new HashMap<String, CombatCard>();
	private Set<String> bonusList = new HashSet<String>();
	private Set<String> tempBonusList = new HashSet<String>();
	private Boolean hasRecharge = false;
	private byte conquestPoint = 0;
	private PlayerEventCards eventCards = new PlayerEventCards(this);
	private PlayerModules playerModules;
	private int specialOrderLimit = 0;
	private int specialOrderUsed = 0;
	private StarcraftGame game;
	private int resourcesTokenToChose = 0;
	private PlayerSpecialActions specialActions = new PlayerSpecialActions(this);
	
	public void addResourceToken(String resourceType){
		this.resourcesTokenToChose--;
		if (resourceType.equals("mineral")){
			this.mineralToken++;
		}else{
			this.gasToken++;
		}
	}
	
	public void initializeResourcesTokenToChose(){
		this.resourcesTokenToChose = Math.min(this.availableWorkers, 3);
	}
	
	public JSONObject getOwnedModulesJS(String action){
		JSONObject result = null;
		if (this.playerModules != null){
			result = this.playerModules.getOwnedModulesJS(action);
		}
		return result;
	}
	
	public ArrayList<StarcraftModule> getOwnedModules() {
		ArrayList<StarcraftModule> result = null;
		if (this.playerModules != null){
			result = this.playerModules.getOwnedModules();
		}
		return result;
	}
	
	public StarcraftPlayer(StarcraftGame game){
		this.game = game;
	}
	
	public  ArrayList<CombatCard> getNamedCards(String cardName) {
		ArrayList<CombatCard> result = new ArrayList<CombatCard>();
		for (CombatCard card:this.dismissedCombatCards){
			if (card.getName() != null){
				if (card.getName().equals(cardName)){
					result.add(card);
				}
			}
		}
		return result;
	}
	
	public void buyModule(){
		this.playerModules.buyModule();
	}
	
	public StarcraftModule getStarcraftModule(int moduleId){
		return this.playerModules.getBuyableStarcraftModule(moduleId);
	}
	
	public Boolean canBuyModule(){
		return this.playerModules.canBuyModule();
	}
	
	public JSONObject getUnlockedModulesJS(String action){
		return this.playerModules.getUnlockedModulesJS(action);
	}
	
	public ArrayList<StarcraftModule> getBuyableModules() {
		return this.playerModules.getBuyableModules();
	}
	
	public void initializeBuyableModules(CardIdGenerator generator){
		this.playerModules = new PlayerModules(this, generator);
		this.playerModules.initializeBuyableModules();
	}
	
	/**ajout d'un bonus temporaire**/
	public void addTempBonus(String bonus){
		this.tempBonusList.add(bonus);
	}
	
	/**dépense d'un bonus temporaire**/
	public void useUpTempBonus(String bonus){
		this.tempBonusList.remove(bonus);
	}
	
	/**on enlève tous les bonus temporaires du joueurs**/
	public void resetTempBonuses(){
		this.tempBonusList = new HashSet<String>();
	}
	
	
	public JSONObject getUnlockedBuildingsJS(String action){
		JSONObject result = null;
		if (this.unitBuildLimit > this.unitBuilt){
			try {
				ArrayList<StarcraftBuilding> unlockedBuildings = new ArrayList<StarcraftBuilding>();
				for (StarcraftBuilding building:this.availableBuildings){
					if (building.getLevel() == 1){
						unlockedBuildings.add(building);
					}else{
						for (StarcraftBuilding builtBuilding:this.ownedBuildings){
							if (builtBuilding.getNumber() == building.getNumber()
									&& builtBuilding.getLevel() + 1 == building.getLevel()){
								unlockedBuildings.add(building);
								break;
							}
						}
					}
				}
				if (unlockedBuildings.size() > 0){
					JSONArray buildingListJS = new JSONArray();
					for (StarcraftBuilding building:unlockedBuildings){
						JSONArray unitListJS = new JSONArray();
						for (String unitName:building.getUnlockedUnits()){
							JSONObject unitJS =  new JSONObject()
									.put("image", GameConstants.getUnitImage(unitName))
									.put("tooltip", GameConstants.getUnitTooltip(unitName));
							unitListJS.put(unitJS);
						}
						//batiment à rajouter
						JSONObject buildingJS = new JSONObject()
								.put("number", building.getNumber())
								.put("level", building.getLevel())
								.put("species", this.species)
								.put("color", this.playerColor)
								.put("unitList", unitListJS);
						buildingListJS.put(buildingJS);
					}
					result = new JSONObject()
							.put("action", action)
							.put("buildingList", buildingListJS);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public void buyAndDrawCombatCard(String cardName, Boolean drawAll){
		if (drawAll){
			//on rajoute les cartes technologies puis on les mélange dans le deck avec les cartes défaussées
			this.combatCardDeck.addAll(this.buyableCards.get(cardName));
			this.combatCardDeck.addAll(this.dismissedCombatCards);
			this.dismissedCombatCards.removeAll(this.dismissedCombatCards);
			Collections.shuffle(this.combatCardDeck);
		}else{
			ArrayList<CombatCard> cardsToBuy = this.buyableCards.get(cardName);
			this.combatCardsInHand.put(cardsToBuy.get(0).getId(), cardsToBuy.get(0));
			for (int i = 1; i < cardsToBuy.size(); i++){
				this.combatCardDeck.add(cardsToBuy.get(i));
			}
			this.combatCardDeck.addAll(this.dismissedCombatCards);
			this.dismissedCombatCards.removeAll(this.dismissedCombatCards);
			Collections.shuffle(this.combatCardDeck);
			GameTurnHandler.callUpdateCardNumber(this);
		}
		this.buyableCards.remove(cardName);
	}
	
	//TODO rajouter les divers bonus
	/**achète une carte**/
	public void buyCard(StarcraftGame game) {
		if (this.buyingOrder.getItem().getClass().getName().endsWith("CombatCard")){
			Boolean dismiss = true;
			CombatCard card = (CombatCard) this.buyingOrder.getItem();
			if (card.getGlobalBonus() != null){
				if (this.activeSpecialOrderBonus){
					this.eventCards.drawEventCard(game);
				}
				//on rajoute ces cartes dans la liste des cartes bonus
				this.globalCards.put(card.getName(), card);
				GameTurnHandler.updateActiveCombatCards(this.playerName, game);
				//on met en place leurs effets ici et dans le reste du jeu
				if (card.getGlobalBonus().startsWith("unlock")){
					if (card.getGlobalBonus().contains("_")){
						int index = card.getGlobalBonus().indexOf("_");
						String unitName1 = card.getGlobalBonus().substring(7, index);
						String unitName2 = card.getGlobalBonus().substring(index+1);
						this.unlockedUnits.add(unitName1);
						GameTurnHandler.updateUnlockedUnits(this, unitName1);
						this.unlockedUnits.add(unitName2);
						GameTurnHandler.updateUnlockedUnits(this, unitName2);
					}else{
						String unitName = card.getGlobalBonus().substring(7);
						this.unlockedUnits.add(unitName);
						GameTurnHandler.updateUnlockedUnits(this, unitName);
					}
				}else if (card.getGlobalBonus().equals("increase_hand_and_recharge")){
					this.cardLimit++;
					this.bonusList.add("recharge");
					this.hasRecharge = true;
				}else{
					//les bonus restants
					this.bonusList.add(card.getGlobalBonus());
				}
			}else{
				if (this.activeSpecialOrderBonus || this.tempBonusList.contains("buyBonusResearch")){
					GameTurnHandler.setResearchCardName(card.getName());
					GameTurnHandler.askResearchBonus(this.playerName, game);
					dismiss = false;
				}else{
					//on rajoute les cartes technologies puis on les mélange dans le deck avec les cartes défaussées
					this.combatCardDeck.addAll(this.buyableCards.get(card.getName()));
					this.combatCardDeck.addAll(this.dismissedCombatCards);
					this.dismissedCombatCards.removeAll(this.dismissedCombatCards);
					Collections.shuffle(this.combatCardDeck);
				}
			}
			if (dismiss){
				this.buyableCards.remove(card.getName());
			}
		}
	}
	
	public void buildBuilding(){
		if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftBuilding")){
			StarcraftBuilding building = (StarcraftBuilding) this.buyingOrder.getItem();
			//si le joueur est zerg, on regarde si le batiment permet des constructions supplémentaires d'unités
			if (this.species.equals("Zerg")){
				if (building.getLevel() == 1){
					this.unitBuildLimit +=2;
				}
			}
			for (String unitName:building.getUnlockedUnits()){
				this.unlockedUnits.add(unitName);
				GameTurnHandler.updateUnlockedUnits(this, unitName);
			}
			this.ownedBuildings.add(building);
			for (int i = 0; i < this.availableBuildings.size(); i++){
				if (this.availableBuildings.get(i).getNumber() == building.getNumber()
						&& this.availableBuildings.get(i).getLevel() == building.getLevel()){
					this.availableBuildings.remove(i);
					break;
				}
			}
		}
	}
	
	public StarcraftBuilding getBuilding(int number, int level){
		StarcraftBuilding result = null;
		for (StarcraftBuilding building:this.availableBuildings){
			if (building.getNumber() == number && building.getLevel() == level){
				result = building;
				break;
			}
		}
		return result;
	}
	
	public JSONObject getUnitsFromPoolJS(StarcraftGame game, String action, String poolName){
		JSONObject result = null;
		try {
			JSONArray UnitListJS = new JSONArray();
			for (long unitId:this.unitPools.get(poolName).getUnitList().keySet()){
				StarcraftUnit unitToAdd = this.unitPools.get(poolName).getUnitList().get(unitId);
				JSONObject unitJS = null;
				if (!unitToAdd.getType().equals("base")){
					unitJS = new JSONObject()
							.put("name", unitToAdd.getId())
							.put("image", unitToAdd.getImage())
							.put("species", unitToAdd.getSpecies());
					UnitListJS.put(unitJS);
				}
			}
			result = new JSONObject()
					.put("action", action)
					.put("unitList", UnitListJS);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**envoie la liste des unités débloquées sous forme JS**/
	public JSONObject getUnlockedUnitsJS2(StarcraftGame game, String action){
		JSONObject result = null;
		try {
			JSONArray UnitListJS = new JSONArray();
			for (String unitName:this.unlockedUnits){
				StarcraftUnit unitToAdd = new StarcraftUnit(unitName, game.unitIdGenerator);
				JSONObject unitJS = null;
				if (!unitToAdd.getType().equals("base")){
					unitJS = new JSONObject()
							.put("name", unitName)
							.put("image", unitToAdd.getImage())
							.put("species", unitToAdd.getSpecies())
							.put("color", this.playerColor);
					UnitListJS.put(unitJS);
				}
			}
			result = new JSONObject()
					.put("action", action)
					.put("unitList", UnitListJS);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**envoie la liste des unités disponibles pour la construction sous forme JS**/
	public JSONObject getUnlockedUnitsJS(StarcraftGame game, String action){
		JSONObject result = null;
		try {
			JSONArray UnitListJS = new JSONArray();
			for (String unitName:this.unlockedUnits){
				StarcraftUnit unitToAdd = new StarcraftUnit(unitName, game.unitIdGenerator);
				JSONObject unitJS = null;
				String tooltip = unitToAdd.getTooltip();
				if (this.getUnitBuildLimit() > this.unitBuilt){
					if (game.getTurnPart().equals(GameConstants.buildUnitsTurnName)){
						if (!unitToAdd.getType().equals("base")){
							unitJS = new JSONObject()
									.put("name", unitName)
									.put("image", unitToAdd.getImage())
									.put("species", unitToAdd.getSpecies())
									.put("color", this.playerColor)
									.put("tooltip", tooltip);
							UnitListJS.put(unitJS);
						}
					}else if (game.getTurnPart().equals(GameConstants.buildBaseTurnName)){
						if (unitToAdd.getType().equals("base")){
							unitJS = new JSONObject()
									.put("name", unitName)
									.put("image", unitToAdd.getImage())
									.put("species", unitToAdd.getSpecies())
									.put("color", this.playerColor)
									.put("tooltip", tooltip);
							UnitListJS.put(unitJS);
							break;
						}
					}
				}else if (!unitToAdd.getType().equals("base") && !unitToAdd.getType().equals("mobile")
						&& game.getTurnPart().equals(GameConstants.buildUnitsTurnName)){
					unitJS = new JSONObject()
							.put("name", unitName)
							.put("image", unitToAdd.getImage())
							.put("species", unitToAdd.getSpecies())
							.put("color", this.playerColor)
							.put("tooltip", tooltip);
					UnitListJS.put(unitJS);
				}
			}
			result = new JSONObject()
					.put("action", action)
					.put("unitList", UnitListJS);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void executeBuyingOrder(){
		if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftUnit")){
			StarcraftUnit unit = (StarcraftUnit) this.buyingOrder.getItem();
			if (unit.getType().equals("mobile")){
				this.unitBuilt++;
			}else if (unit.getType().equals("base")){
				this.unitBuilt = this.getUnitBuildLimit();
			}
		}/*else if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftBuilding")){
			this.unitBuilt = this.getUnitBuildLimit();
		}else if (this.buyingOrder.getItem().getClass().getName().endsWith("CombatCard")){
			this.unitBuilt = this.getUnitBuildLimit();
		}else if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftModule")){
			this.unitBuilt = this.getUnitBuildLimit();
		}*/
		this.buyingOrder = null;
	}
	
	/**annule la commande de construction et récupère les travailleurs déjà utilisés **/
	public void cancelBuyingOrder(StarcraftGame game){
		this.workerOnMineral -= this.buyingOrder.getWorkerOnBaseMineral();
		this.workerOnGas -= this.buyingOrder.getWorkerOnBaseGas();
		//récupération des travailleurs affectés à la base
		this.availableWorkers += this.buyingOrder.getWorkerOnBaseMineral() + this.buyingOrder.getWorkerOnBaseGas();
		//TODO mettre à jour l'affichage
		if (this.buyingOrder.getMineralTokens() + this.buyingOrder.getGasTokens() > 0){
			this.mineralToken += this.buyingOrder.getMineralTokens();
			this.gasToken += this.buyingOrder.getGasTokens();
			GameTurnHandler.updateResourceTokenDisplay(this.playerName, game);
		}
		GameTurnHandler.callDisplayWorkersOnBaseResources(this);
		for (List<Integer> coordinates:this.buyingOrder.getAffectedWorkers().keySet()){
			PlanetArea area = this.areaResourceList.get(coordinates);
			int returnedWorkers = this.buyingOrder.getAffectedWorkers().get(coordinates);
			int resourceAmount = Math.max(area.getGasResources(), area.getMineralResources());
			if (resourceAmount < area.getWorkerAmount()){
				area.setExhaustion(area.getExhaustion() - Math.min(returnedWorkers, area.getExhaustion()));
				GameTurnHandler.updateExhaustionDisplay(
						new int[]{coordinates.get(0), coordinates.get(1), coordinates.get(2)}, area.getExhaustion(), game);
			}
			area.setWorkerAmount(area.getWorkerAmount() - returnedWorkers);
			//récupération des travailleurs affectés à la zone
			this.availableWorkers += returnedWorkers;
			GameTurnHandler.updateAreaWorkerDisplay(coordinates, this, game);
		}
		String itemType = "";
		if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftUnit")){
			this.unitPools.remove(game.getTurnPart());
			itemType = "StarcraftUnit";
		}else if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftBuilding")){
			itemType = "StarcraftBuilding";
		}else if (this.buyingOrder.getItem().getClass().getName().endsWith("CombatCard")){
			itemType = "CombatCard";
		}else if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftModule")){
			itemType = "StarcraftModule";
		}
		for (StarcraftUnit unit:this.buyingOrder.getSpentUnits()){
			int xCoord = unit.getCoordinates()[0];
			int yCoord = unit.getCoordinates()[1];
			int areaId = unit.getCoordinates()[2];
			unit.setCoordinates(new int[]{xCoord, yCoord, areaId}, game);
			game.getGalaxy().addUnit(unit);
			GameTurnHandler.updateUnitDisplay(unit.getId(), game);
		}
		if (this.buyingOrder.getUsedReductions() > 0){
			this.tempBonusList.add("costReduction");
		}
		this.buyingOrder = null;
		GameTurnHandler.updateBaseWorkerDisplay(this);
		if (itemType.equals("StarcraftUnit")){
			GameTurnHandler.setBuildUnitStage(this.playerName, game);
		}else if (itemType.equals("StarcraftBuilding")){
			GameTurnHandler.setBuildBuildingStage(this.playerName, game);
		}else if (itemType.equals("CombatCard")){
			GameTurnHandler.setBuyResearchStage(this.playerName, game);
		}else if (itemType.equals("StarcraftModule")){
			GameTurnHandler.setBuyModuleStage(this.playerName, game);
		}
		
	}
	
	public void useCostReduction(String resource){
		if (this.tempBonusList.contains("costReduction")){
			this.buyingOrder.setUsedReductions(this.buyingOrder.getUsedReductions() + 1);
			if (resource.equals("mineral")){
				this.buyingOrder.setSpentMineral(this.buyingOrder.getSpentMineral() + 1);
			}else{
				this.buyingOrder.setSpentGas(this.buyingOrder.getSpentGas() + 1);
			}
			this.tempBonusList.remove("costReduction");
		}
	}
	
	public void sendWorkerOnBaseMineral(){
		this.availableWorkers--;
		this.workerOnMineral++;
		this.buyingOrder.setWorkerOnBaseMineral(this.buyingOrder.getWorkerOnBaseMineral() + 1);
		this.buyingOrder.setSpentMineral(this.buyingOrder.getSpentMineral() + 1);
	}
	
	public void sendWorkerOnBaseGas(){
		this.availableWorkers--;
		this.workerOnGas++;
		this.buyingOrder.setWorkerOnBaseGas(this.buyingOrder.getWorkerOnBaseGas() + 1);
		this.buyingOrder.setSpentGas(this.buyingOrder.getSpentGas() + 1);
	}
	
	public void sendWorkerToArea(int x, int y, int z){
		this.availableWorkers--;
		PlanetArea area = this.areaResourceList.get(Arrays.asList(x, y, z));
		area.setWorkerAmount(area.getWorkerAmount() + 1);
		this.buyingOrder.addWorkerToArea(Arrays.asList(x, y, z), 1);
		if (area.getMineralResources() > 0){
			if (area.getMineralResources() < area.getWorkerAmount()){
				area.setExhaustion(area.getExhaustion() + 1);
				GameTurnHandler.updateExhaustionDisplay(new int[]{x, y, z}, area.getExhaustion(), game);
			}
			this.buyingOrder.setSpentMineral(this.buyingOrder.getSpentMineral() + 1);
		}else{
			if (area.getGasResources() < area.getWorkerAmount()){
				area.setExhaustion(area.getExhaustion() + 1);
				GameTurnHandler.updateExhaustionDisplay(new int[]{x, y, z}, area.getExhaustion(), game);
			}
			this.buyingOrder.setSpentGas(this.buyingOrder.getSpentGas() + 1);
		}
	}
	
	/**dans le cas où l'unité en construction nécessite un sacrifice, on indique quelles unités sont sacrifiables**/
	public Set<Long> getUsableResourceUnits(StarcraftGame game){
		Set<Long> result = new HashSet<Long>();
		if (this.buyingOrder.getItem().getClass().getName().endsWith("StarcraftUnit")){
			StarcraftUnit unit = (StarcraftUnit) this.getBuyingOrder().getItem();
			if (this.getBuyingOrder().getSpentUnits().size() < unit.getUnitCostNumber()){
				Planet planet = game.getGalaxy().getAllPlanets().get(game.getGalaxy().getPlanetEvent());
				for (PlanetArea area:planet.getAreaList()){
					for (long unitId:area.getUnitIdList()){
						StarcraftUnit unitInArea = game.getGalaxy().getUnitList().get(unitId);
						if (unitInArea.getOwner().equals(this.playerName) && unitInArea.getName().equals(unit.getUnitCost())){
							result.add(unitId);
						}
					}
				}
			}
		}
		return result;
	}
	
	public Set<List<Integer>> getUsableResourceAreas(){
		Set<List<Integer>> result = new HashSet<List<Integer>>();
		if (this.availableWorkers > 0){
			if (this.buyingOrder.requireMineral()){
				for (List<Integer> coordinates:this.areaResourceList.keySet()){
					PlanetArea area = this.areaResourceList.get(coordinates);
					if (area.getMineralResources() > 0 && area.getExhaustion() < 2){
						result.add(coordinates);
					}
				}
			}
			if (this.buyingOrder.requireGas()){
				for (List<Integer> coordinates:this.areaResourceList.keySet()){
					PlanetArea area = this.areaResourceList.get(coordinates);
					if (area.getGasResources() > 0 && area.getExhaustion() < 2){
						result.add(coordinates);
					}
				}
			}
		}
		return result;
	}
	
	/**vérifie si il est possible d'utiliser un jeton de minéral**/
	public Boolean canUseMineralToken(){
		Boolean result = false;
		if (buyingOrder!= null && this.buyingOrder.requireMineral()){
			if (this.mineralToken > 0){
					result = true;
			}
		}
		return result;
	}
	
	/**vérifie si il est possible d'utiliser un jeton de gas**/
	public Boolean canUseGasToken(){
		Boolean result = false;
		if (buyingOrder!= null && this.buyingOrder.requireGas()){
			if (this.gasToken > 0){
					result = true;
			}
		}
		return result;
	}
	
	/**vérifie si il est possible d'affecter des travailleurs supplémentaires sur le minéral de base**/
	public Boolean canSetWorkerOnBaseMineral(){
		Boolean result = false;
		if (buyingOrder!= null && this.buyingOrder.requireMineral()){
			if (this.availableWorkers > 0){
				if (this.workerOnMineral < this.mineralResources){
					result = true;
				}
			}
		}
		return result;
	}
	
	/**vérifie si il est possible d'affecter des travailleurs supplémentaires sur le gas de base**/
	public Boolean canSetWorkerOnBaseGas(){
		Boolean result = false;
		if (buyingOrder!= null && this.buyingOrder.requireGas()){
			if (this.availableWorkers > 0){
				if (this.workerOnGas < this.gasResources){
					result = true;
				}
			}
		}
		return result;
	}
	
	//TODO
	public void setBuyingOrder(BuyableItem item){
		this.buyingOrder = new BuyingOrder();
		buyingOrder.setItem(item);
	}
	
	public BuyingOrder getBuyingOrder(){
		return this.buyingOrder;
	}
	
	
	
	/**pioche des cartes de combat**/
	public void drawCombatCards(int number){
		for (int i = 0; i < number; i++){
			if (this.combatCardDeck.size() > 0){
				this.combatCardsInHand.put(this.combatCardDeck.get(0).getId(), this.combatCardDeck.get(0));
				this.combatCardDeck.remove(0);
			}else if (this.dismissedCombatCards.size()> 0){
				this.combatCardDeck.addAll(this.dismissedCombatCards);
				Collections.shuffle(this.combatCardDeck);
				this.dismissedCombatCards.removeAll(this.dismissedCombatCards);
				i--;
			}else{
				break;
			}
		}
		GameTurnHandler.callUpdateCardNumber(this);
		System.out.println("Card situation : "
				+ Integer.toString(this.dismissedCombatCards.size()) + " ; "
				+ Integer.toString(this.combatCardDeck.size()) + " ; "
				+ Integer.toString(this.combatCardsInHand.size()));
	}
	
	/**initialise la liste des batiments constructibles**/
	private void initializeBuildings(){
		URL resources = getClass().getClassLoader().getResource("../../starcraftResources/buildingList.xml");
		try {
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile("/root/faction[@species =\""+ this.species +"\"]");
			Object o = expr.evaluate(doc, XPathConstants.NODE);
			Node selectedNode = (Node) o;
			NodeList list = selectedNode.getChildNodes();
			
			for (int i = 0; i < list.getLength(); i++){
				
				Node building = (Node) list.item(i);
				String nodeName = building.getNodeName();
				NamedNodeMap buildingAttributes = building.getAttributes();
				if (nodeName.equals("building")){
					StarcraftBuilding buildingToAdd = new StarcraftBuilding();
					NodeList propertyList = building.getChildNodes();
					buildingToAdd.setNumber(Integer.parseInt(buildingAttributes.getNamedItem("number").getNodeValue()));
					buildingToAdd.setLevel(Integer.parseInt(buildingAttributes.getNamedItem("level").getNodeValue()));
					for (int j = 0; j < propertyList.getLength(); j++){
						Node property = (Node) propertyList.item(j);
						String propertyName = property.getNodeName();
						NamedNodeMap propertyAttributes = property.getAttributes();
						if (propertyName.equals("cost")){
							if (propertyAttributes.getNamedItem("mineral")!=null){
								buildingToAdd.setMineralCost(Integer.parseInt(propertyAttributes.getNamedItem("mineral").getNodeValue()));
							}
							if (propertyAttributes.getNamedItem("gas")!=null){
								buildingToAdd.setGasCost(Integer.parseInt(propertyAttributes.getNamedItem("gas").getNodeValue()));
							}
						} else if (propertyName.equals("unlock")){
							buildingToAdd.getUnlockedUnits().add(propertyAttributes.getNamedItem("name").getNodeValue());
						}
					}
					if (buildingToAdd.getMineralCost() == 0 && buildingToAdd.getGasCost() == 0){
						this.ownedBuildings.add(buildingToAdd);
						this.unlockedUnits.addAll(buildingToAdd.getUnlockedUnits());
					}else{
						this.availableBuildings.add(buildingToAdd);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**met les cartes de combats de base dans le deck du joueur**/
	public void initializeCombatCardDeck(CardIdGenerator generator){
		URL resources = getClass().getClassLoader().getResource("../../starcraftResources/combatCards.xml");
		try {
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile("/root/faction[@species =\""+ this.species +"\"]");
			Object o = expr.evaluate(doc, XPathConstants.NODE);
			Node selectedNode = (Node) o;
			NodeList list = selectedNode.getChildNodes();
			
			for (int i = 0; i < list.getLength(); i++){
				//TODO
				Node card = (Node) list.item(i);
				String nodeName = card.getNodeName();
				if (nodeName.equals("card")){
					CombatCard cardToAdd = new CombatCard(generator);
					NamedNodeMap cardAttributes = card.getAttributes();
					if (cardAttributes.getNamedItem("name") != null){
						cardToAdd.setName(cardAttributes.getNamedItem("name").getNodeValue());
					}
					if (cardAttributes.getNamedItem("scope") != null){
						cardToAdd.setScope(cardAttributes.getNamedItem("scope").getNodeValue());
					}
					cardToAdd.setSpecies(this.species);
					cardToAdd.setColor(this.playerColor);
					cardToAdd.setOwner(this.playerName);
					NodeList propertyList = card.getChildNodes();
					for (int j = 0; j < propertyList.getLength(); j++){
						Node property = (Node) propertyList.item(j);
						String propertyName = property.getNodeName();
						NamedNodeMap propertyAttributes = property.getAttributes();
						if (propertyName.equals("unit")){
							cardToAdd.getUnitNames().add(propertyAttributes.getNamedItem("name").getNodeValue());
						} else if (propertyName.equals("text")){
							cardToAdd.setText(property.getTextContent());
						} else if (propertyName.equals("globalBonus")){
							cardToAdd.setGlobalBonus(propertyAttributes.getNamedItem("name").getNodeValue());
						} else if (propertyName.equals("trigger")){
							cardToAdd.setTriggerName(propertyAttributes.getNamedItem("name").getNodeValue());
						} else if (propertyName.equals("maxStat")){
							cardToAdd.setMaxAttack(Integer.parseInt(propertyAttributes.getNamedItem("attack").getNodeValue()));
							cardToAdd.setMaxDefense(Integer.parseInt(propertyAttributes.getNamedItem("health").getNodeValue()));
						} else if (propertyName.equals("minStat")){
							cardToAdd.setMinAttack(Integer.parseInt(propertyAttributes.getNamedItem("attack").getNodeValue()));
							cardToAdd.setMinDefense(Integer.parseInt(propertyAttributes.getNamedItem("health").getNodeValue()));
						} else if (propertyName.equals("condition")){
							cardToAdd.getRequirements().add(propertyAttributes.getNamedItem("name").getNodeValue());
						}else if (propertyName.equals("cost")){
							if (propertyAttributes.getNamedItem("mineral")!=null){
								cardToAdd.setMineralCost
								(Integer.parseInt(propertyAttributes.getNamedItem("mineral").getNodeValue()));
							}
							if (propertyAttributes.getNamedItem("gas")!=null){
								cardToAdd.setGasCost(Integer.parseInt(propertyAttributes.getNamedItem("gas").getNodeValue()));
							}
						} else if (propertyName.equals("ability")){
							CombatCardAbility abilityToAdd = new CombatCardAbility();
							abilityToAdd.setName(propertyAttributes.getNamedItem("name").getNodeValue());
							if (propertyAttributes.getNamedItem("amount") != null){
								abilityToAdd.setAmount(Integer.parseInt(propertyAttributes.getNamedItem("amount").getNodeValue()));
							}
							NodeList abilityPropertyList = property.getChildNodes();
							for (int k = 0; k < abilityPropertyList.getLength(); k++){
								Node abilityProperty = (Node) abilityPropertyList.item(k);
								String abilityPropertyName = abilityProperty.getNodeName();
								NamedNodeMap abilityPropertyAttributes = abilityProperty.getAttributes();
								if (abilityPropertyName.equals("ally")){
									abilityToAdd.getAlliedUnitNames()
									.add(abilityPropertyAttributes.getNamedItem("name").getNodeValue());
								} else if (abilityPropertyName.equals("enemy")){
									abilityToAdd.getHostileUnitNames()
									.add(abilityPropertyAttributes.getNamedItem("name").getNodeValue());
								}
							}
							cardToAdd.addAbility(abilityToAdd);
							//vérifie que les cartes sont crées correctement
							/*System.out.println(abilityToAdd.getName());
							for (String unitName:abilityToAdd.getAlliedUnitNames()){
								System.out.println(unitName);
							}
							for (String unitName:abilityToAdd.getHostileUnitNames()){
								System.out.println(unitName);
							}*/
						}
					}
					if (cardToAdd.getName().equals("")){
						//seules les cartes du paquet technologie ont un nom
						this.combatCardDeck.add(cardToAdd);
						//System.out.println(cardToAdd.getCardJS("test").toString());
					}else if(this.buyableCards.containsKey(cardToAdd.getName())){
						this.buyableCards.get(cardToAdd.getName()).add(cardToAdd);
					}else {
						ArrayList<CombatCard> combatCardListToAdd = new ArrayList<CombatCard>();
						combatCardListToAdd.add(cardToAdd);
						this.buyableCards.put(cardToAdd.getName(), combatCardListToAdd);
					}
				}
			}
			Collections.shuffle(this.combatCardDeck);
			if (this.species.equals("Terran")){
				this.drawCombatCards(8);
				this.cardLimit = 8;
			}else{
				this.drawCombatCards(6);
				this.cardLimit = 6;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void removeUnitFromPools(Long unitId){
		for (String poolName:this.unitPools.keySet()){
			if (this.unitPools.get(poolName).getUnitList().containsKey(unitId)){
				this.unitPools.get(poolName).removeUnit(unitId);
				if (poolName.equals(GameConstants.moveUnitTurnName) || poolName.equals("conclaveFleetTurn")){
					GameTurnHandler.removeReservedUnitDisplay(this.playerName, game, unitId);
				}
				break;
			}
		}
	}
	
	public String getName(){
		return this.playerName;
	}
	
	public void setName(String newName){
		this.playerName = newName;
	}
	
	public String getSpecies(){
		return this.species;
	}
	
	public void setSpecies(String species){
		this.species = species;
	}
	
	public String getFaction(){
		return this.faction;
	}
	
	public void setFaction(String faction){
		this.faction = faction;
		initializeBuildings();
	}
	
	public String getFactionImage(){
		return this.factionImage;
	}
	
	public void setFactionImage(String factionImage){
		this.factionImage = factionImage;
	}

	public Set<String> getLeadershipcards() {
		return leadershipcards;
	}
	
	public void initializeOwnedOrders(){
		this.ownedOrders.add(new OrderToken("move", this.playerName, false));
		this.ownedOrders.add(new OrderToken("move", this.playerName, false));
		this.ownedOrders.add(new OrderToken("move", this.playerName, true));
		this.ownedOrders.add(new OrderToken("build", this.playerName, false));
		this.ownedOrders.add(new OrderToken("build", this.playerName, false));
		this.ownedOrders.add(new OrderToken("build", this.playerName, true));
		this.ownedOrders.add(new OrderToken("research", this.playerName, false));
		this.ownedOrders.add(new OrderToken("research", this.playerName, false));
		this.ownedOrders.add(new OrderToken("research", this.playerName, true));
		if (this.bonusList.contains("starOrder")){
			this.ownedOrders.add(new OrderToken("starOrder", this.playerName, false));
		}
	}
	
	public void loadAvailableOrders(){
		this.availableOrders = new HashMap<Integer, OrderToken>();
		int i = 0;
		for (OrderToken order:this.ownedOrders){
			OrderToken clone = order.cloneOrder();
			clone.setId(i);
			i++;
			this.availableOrders.put(clone.getId(), clone);
		}
	}
	
	public Map<Integer, OrderToken> getAvailableOrders(){
		return this.availableOrders;
	}
	
	public void removeOrder(int id){
		this.availableOrders.remove(id);
	}
	
	/****/
	public void addUnitToPlayerPool(String poolTurn, StarcraftUnit unit){
		if (this.unitPools.containsKey(poolTurn)){
			this.unitPools.get(poolTurn).addUnit(unit);
		}else{
			UnitPool unitPool = new UnitPool();
			unitPool.setTurnPart(poolTurn);
			unitPool.addUnit(unit);
			this.unitPools.put(poolTurn, unitPool);
		}
		if (poolTurn.equals(GameConstants.moveUnitTurnName) && this.bonusList.contains("covertOperations")){
			GameTurnHandler.addReservedUnitDisplay(this.playerName, game, unit);
		}else if (poolTurn.equals("conclaveFleetTurn") && this.bonusList.contains("conclaveFleet")){
			GameTurnHandler.addReservedUnitDisplay(this.playerName, game, unit);
		}
	}

	/**ajoute la carte de leadership puis met en place ses effets (ajout d'unités et effets spéciaux)**/
	public void addLeadershipcards(String leadershipcard, StarcraftGame game) {
		//TODO mettre les effets spéciaux en place
		//augmentation des ressources de base
		if (leadershipcard.equals("Endless hunger")){
			this.mineralResources += 1;
			this.gasResources += 1;
		}else if (leadershipcard.equals("Orbital platform")){
			this.mineralResources += 2;
		}
		this.leadershipcards.add(leadershipcard);
		URL resources = getClass().getClassLoader().getResource("../../starcraftResources/leadershipCards.xml");
		try {
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile("/root/faction/card[@name =\""+ leadershipcard +"\"]");
			Object o = expr.evaluate(doc, XPathConstants.NODE);
			Node selectedNode = (Node) o;
			NodeList list = selectedNode.getChildNodes();
			
			for (int i = 0; i < list.getLength(); i++){
				Node property = (Node) list.item(i);
				String propertyName = property.getNodeName();
				if (propertyName.equals("itemBatch")){
					int unitNumber = Integer.parseInt(property.getAttributes().getNamedItem("number").getNodeValue());
					String unitName = property.getAttributes().getNamedItem("name").getNodeValue();
					StarcraftUnit starcraftunit = new StarcraftUnit(unitName, game.unitIdGenerator);
					starcraftunit.setOwner(this.playerName);
					starcraftunit.setColor(this.playerColor);
					starcraftunit.setStartingSituation(GameConstants.startingUnitSituation);
					String poolTurn = "";
					if (starcraftunit.getType().equals("base")){
						poolTurn = GameConstants.placeBaseTurnName;
					}else if(starcraftunit.getType().equals("worker")){
						this.availableWorkers += unitNumber;
						this.worker = starcraftunit;
					}else{
						poolTurn = "placeUnit";
					}
					if (!poolTurn.equals("")){
						if (this.unitPools.containsKey(poolTurn)){
							this.unitPools.get(poolTurn).addUnit(starcraftunit);
							for (int j = 1; j < unitNumber; j++){
								StarcraftUnit starcraftunitToAdd = new StarcraftUnit(unitName, game.unitIdGenerator);
								starcraftunitToAdd.setOwner(this.playerName);
								starcraftunitToAdd.setColor(this.playerColor);
								starcraftunitToAdd.setStartingSituation(GameConstants.startingUnitSituation);
								this.unitPools.get(poolTurn).addUnit(starcraftunitToAdd);
							}
						}else{
							UnitPool unitPool = new UnitPool();
							unitPool.setTurnPart(poolTurn);
							unitPool.addUnit(starcraftunit);
							for (int j = 1; j < unitNumber; j++){
								StarcraftUnit starcraftunitToAdd = new StarcraftUnit(unitName, game.unitIdGenerator);
								starcraftunitToAdd.setOwner(this.playerName);
								starcraftunitToAdd.setColor(this.playerColor);
								starcraftunitToAdd.setStartingSituation(GameConstants.startingUnitSituation);
								unitPool.addUnit(starcraftunitToAdd);
							}
							this.unitPools.put(poolTurn, unitPool);
						}
					}
				}else if (propertyName.equals("specialEffect")){
					this.applyLeadershipBonus(leadershipcard);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		initializeOwnedOrders();
		loadAvailableOrders();
	}

	/**utilise une carte de la main du joueur**/
	public void useUpCombatCard(CombatCard card){
		if (this.combatCardsInHand.containsKey(card.getId())){
			this.combatCardsInHand.remove(card.getId());
		}
		if (card.sendToTech()){
			if(this.buyableCards.containsKey(card.getName())){
				this.buyableCards.get(card.getName()).add(card);
			}else {
				ArrayList<CombatCard> combatCardListToAdd = new ArrayList<CombatCard>();
				combatCardListToAdd.add(card);
				this.buyableCards.put(card.getName(), combatCardListToAdd);
			}
		}else{
			this.dismissedCombatCards.add(card);
		}
	}
	
	public Map<String, UnitPool> getUnitPools() {
		return this.unitPools;
	}
	
	public Map<String, Planet> getPlanetDeck() {
		return this.planetDeck;
	}

	public void addPlanet(Planet planet) {
		this.planetDeck.put(planet.getName(), planet);
	}
	
	public void removePlanet(String planetName){
		this.planetDeck.remove(planetName);
	}

	public int getAvailableWorkers() {
		return availableWorkers;
	}

	public void setAvailableWorkers(int availableWorkers) {
		this.availableWorkers = availableWorkers;
	}

	public int getUnavailableWorkers() {
		return unavailableWorkers;
	}

	public void setUnavailableWorkers(int unavailableWorkers) {
		this.unavailableWorkers = unavailableWorkers;
	}

	public String getPlayerColor() {
		return playerColor;
	}

	public void setPlayerColor(String playerColor) {
		this.playerColor = playerColor;
	}

	public Boolean getActiveSpecialOrderBonus() {
		return activeSpecialOrderBonus;
	}

	public void setActiveSpecialOrderBonus(Boolean activeSpecialOrderBonus) {
		this.activeSpecialOrderBonus = activeSpecialOrderBonus;
	}

	public ArrayList<CombatCard> getDismissedCombatCards() {
		return dismissedCombatCards;
	}

	public Map<Integer, CombatCard> getCombatCardsInHand() {
		return combatCardsInHand;
	}

	public ArrayList<CombatCard> getCombatCardDeck() {
		return combatCardDeck;
	}

	public int getMineralResources() {
		return mineralResources;
	}

	public void setMineralResources(int crystalResources) {
		this.mineralResources = crystalResources;
	}

	public int getGasResources() {
		return gasResources;
	}

	public void setGasResources(int gasResources) {
		this.gasResources = gasResources;
	}

	public int getMineralToken() {
		return mineralToken;
	}

	public void setMineralToken(int crystalToken) {
		this.mineralToken = crystalToken;
	}

	public int getGasToken() {
		return gasToken;
	}

	public void setGasToken(int gasToken) {
		this.gasToken = gasToken;
	}

	public Map<List<Integer>, PlanetArea> getAreaResourceList() {
		return areaResourceList;
	}

	public void addResourceArea(int x, int y, PlanetArea area){
		List<Integer> coordinates = Arrays.asList(x, y, area.getId());
		this.areaResourceList.put(coordinates, area);
	}

	public Set<String> getUnlockedUnits() {
		return unlockedUnits;
	}

	public ArrayList<StarcraftBuilding> getAvailableBuildings() {
		return availableBuildings;
	}

	public ArrayList<StarcraftBuilding> getOwnedBuildings() {
		return ownedBuildings;
	}

	public int getWorkerOnMineral() {
		return workerOnMineral;
	}

	public void setWorkerOnMineral(int workerOnMineral) {
		this.workerOnMineral = workerOnMineral;
	}

	public int getWorkerOnGas() {
		return workerOnGas;
	}

	public void setWorkerOnGas(int workerOnGas) {
		this.workerOnGas = workerOnGas;
	}

	public StarcraftUnit getWorker() {
		return worker;
	}

	public int getUnitBuildLimit() {
		int result = unitBuildLimit;
		if (this.activeSpecialOrderBonus){
			result++;
		}
		return result;
	}

	public int getUnitBuilt() {
		return unitBuilt;
	}

	public void setUnitBuilt(int unitBuilt) {
		this.unitBuilt = unitBuilt;
	}
	
	public void increaseUnitBuildLimit(){
		this.unitBuildLimit++;
	}

	public int getCardLimit() {
		int result = this.cardLimit;
		if (this.bonusList.contains("aggressiveBonus")){
			result++;
		}
		return result;
	}
	
	public Map<String, ArrayList<CombatCard>> getBuyableCards() {
		return this.buyableCards;
	}

	public void setCardLimit(int cardLimit) {
		this.cardLimit = cardLimit;
	}

	public Map<String, CombatCard> getGlobalCards() {
		return globalCards;
	}
	
	public void addAvailableWorker(int number){
		this.availableWorkers += number;
	}
	
	public void addUnavailableWorker(int number){
		this.unavailableWorkers += number;
	}

	public Set<String> getBonusList() {
		return bonusList;
	}

	public Boolean getHasRecharge() {
		return hasRecharge;
	}

	public void setHasRecharge(Boolean hasRecharge) {
		this.hasRecharge = hasRecharge;
	}

	public byte getConquestPoint() {
		return conquestPoint;
	}

	public void setConquestPoint(byte conquestPoint) {
		this.conquestPoint = conquestPoint;
	}

	public Set<String> getTempBonusList() {
		return tempBonusList;
	}

	public int getSpecialOrderLimit() {
		return specialOrderLimit;
	}

	public void setSpecialOrderLimit(int specialOrderLimit) {
		this.specialOrderLimit = specialOrderLimit;
	}

	public int getSpecialOrderUsed() {
		return specialOrderUsed;
	}

	public void setSpecialOrderUsed(int specialOrderUsed) {
		this.specialOrderUsed = specialOrderUsed;
	}
	
	public Boolean canUseSpecialOrder(){
		Boolean result = false;
		if (this.game.getGalaxy().getsSpecialBenefit(this)){
			result = true;
		}else if (this.specialOrderLimit > this.specialOrderUsed){
			result = true;
		}
		return result;
	}
	
	public void useSpecialOrder(){
		this.specialOrderUsed++;
		this.activeSpecialOrderBonus = true;
	}

	public StarcraftGame getGame() {
		return game;
	}
	
	private void applyLeadershipBonus(String bonusName){
		if (bonusName.equals("Glimpse the future")){
			SpecialTurnEvent specialTurnEvent = new SpecialTurnEvent();
			specialTurnEvent.setTriggeringPlayer("");
			specialTurnEvent.setTriggeringTurn(GameConstants.placeZRoadTurnName);
			specialTurnEvent.setSpecialTurnName(GameConstants.placeZRoadTurnName);
			for (int i = 0; i < game.getPlayerList().size(); i++){
				specialTurnEvent.addNewPlayerTurn(0, this.playerName);
			}
			this.game.addSpecialEvent(specialTurnEvent);
			SpecialTurnEvent specialTurnEvent2 = new SpecialTurnEvent();
			specialTurnEvent2.setTriggeringPlayer("");
			specialTurnEvent2.setTriggeringTurn(GameConstants.placeZRoadTurnName);
			specialTurnEvent2.setSpecialTurnName("glimpseTheFuture");
			specialTurnEvent2.addNewPlayerTurn(0, this.playerName);
			this.game.addSpecialEvent(specialTurnEvent2);
		}else if (bonusName.equals("The Overmind")){
			this.bonusList.add("Overmind");
		}else if (bonusName.equals("Emperor Arcturus I")){
			SpecialTurnEvent specialTurnEvent = new SpecialTurnEvent();
			specialTurnEvent.setTriggeringPlayer("");
			specialTurnEvent.setTriggeringTurn("planetChoice");
			specialTurnEvent.setSpecialTurnName("choosePlanetToDraw");
			specialTurnEvent.addNewPlayerTurn(0, this.playerName);
			this.game.addSpecialEvent(specialTurnEvent);
			SpecialTurnEvent specialTurnEvent2 = new SpecialTurnEvent();
			specialTurnEvent2.setTriggeringPlayer("");
			specialTurnEvent2.setTriggeringTurn("planetChoice");
			specialTurnEvent2.setSpecialTurnName("chooseFirstPlayer");
			specialTurnEvent2.addNewPlayerTurn(0, this.playerName);
			this.game.addSpecialEvent(specialTurnEvent2);
		}else if (bonusName.equals("The sons of Korhal")){
			this.bonusList.add("starOrder");
		}else if (bonusName.equals("Storage facilities")){
			this.bonusList.add("storageFacilities");
			SpecialTurnEvent specialTurnEvent = new SpecialTurnEvent();
			specialTurnEvent.setTriggeringPlayer("");
			specialTurnEvent.setTriggeringTurn(GameConstants.placeZRoadTurnName);
			specialTurnEvent.setSpecialTurnName("initializeResourcesToken");
			specialTurnEvent.addNewPlayerTurn(0, this.playerName);
			this.game.addSpecialEvent(specialTurnEvent);
			SpecialTurnEvent specialTurnEvent2 = new SpecialTurnEvent();
			specialTurnEvent2.setTriggeringPlayer("");
			specialTurnEvent2.setTriggeringTurn(GameConstants.placeZRoadTurnName);
			specialTurnEvent2.setSpecialTurnName("getResourcesToken");
			specialTurnEvent2.addNewPlayerTurn(0, this.playerName);
			this.game.addSpecialEvent(specialTurnEvent2);
		}else if (bonusName.equals("Covert operations")){
			this.bonusList.add("covertOperations");
			String poolTurn = GameConstants.moveUnitTurnName;
			for (int i = 0; i < 3; i++){
				StarcraftUnit starcraftunit = new StarcraftUnit("ghost", this.game.unitIdGenerator);
				starcraftunit.setOwner(this.playerName);
				starcraftunit.setColor(this.playerColor);
				starcraftunit.setStartingSituation("covertOperations");
				this.addUnitToPlayerPool(poolTurn, starcraftunit);
			}
			GameTurnHandler.reservedUnitsInfo(this.playerName, game);
		}else if (bonusName.equals("Warp gate")){
			this.bonusList.add("warpgate");
		}else if (bonusName.equals("Riches of Aiur")){
			this.bonusList.add("RichesOfAiur");
		}else if (bonusName.equals("Conclave fleet")){
			this.bonusList.add("conclaveFleet");
			this.unitPools.put("conclaveFleetTurn", new UnitPool());
			GameTurnHandler.reservedUnitsInfo(this.playerName, game);
		}
	}

	public int getResourcesTokenToChose() {
		return resourcesTokenToChose;
	}

	
	public void useTokenResource(String resourceType) {
		if (resourceType.equals("mineral")){
			this.mineralToken--;
			this.buyingOrder.setMineralTokens(this.buyingOrder.getMineralTokens() + 1);
			this.buyingOrder.setSpentMineral(this.buyingOrder.getSpentMineral() + 1);
		}else{
			this.gasToken--;
			this.buyingOrder.setGasTokens(this.buyingOrder.getGasTokens() + 1);
			this.buyingOrder.setSpentGas(this.buyingOrder.getSpentGas() + 1);
		}
	}

	public PlayerSpecialActions getSpecialActions() {
		return specialActions;
	}
	
	public PlayerEventCards getEventCards() {
		return this.eventCards;
	}
	

}
