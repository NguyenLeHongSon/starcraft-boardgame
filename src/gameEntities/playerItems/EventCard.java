package gameEntities.playerItems;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.CardIdGenerator;
import gameEntities.GameConstants;
import gameEntities.StarcraftPlayer;

public class EventCard implements java.io.Serializable {

	private static final long serialVersionUID = -5373677024145277257L;

	private int id;
	private String name;
	private String type;
	private String turnName;
	private String bonusName;
	private String text;
	private ArrayList<String> unitListZerg = new ArrayList<String>();
	private ArrayList<String> unitListTerran = new ArrayList<String>();
	private ArrayList<String> unitListPross = new ArrayList<String>();
	
	public EventCard(CardIdGenerator generator){
		this.id = generator.getNextValue();
	}
	
	public JSONObject getCardJS(String action){
		JSONObject result = null;
		try {
			result = new JSONObject();
			result.put("name", this.name);
			if (!action.equals("")){
				result.put("action", action);
			}
			result.put("id", this.id)
			.put("text", this.text);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTurnName() {
		return turnName;
	}
	public void setTurnName(String turnName) {
		this.turnName = turnName;
	}
	public String getBonusName() {
		return bonusName;
	}
	public void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getId() {
		return id;
	}

	public ArrayList<String> getUnitListZerg() {
		return unitListZerg;
	}

	public ArrayList<String> getUnitListTerran() {
		return unitListTerran;
	}

	public ArrayList<String> getUnitListPross() {
		return unitListPross;
	}
	
	public void addUnitToCard(String species, String unitName){
		if (species.equals("Zerg")){
			this.unitListZerg.add(unitName);
		}else if (species.equals("Terran")){
			this.unitListTerran.add(unitName);
		}else if (species.equals("Protoss")){
			this.unitListPross.add(unitName);
		}
	}
	
	public ArrayList<StarcraftUnit> getUnitList(StarcraftPlayer player) {
		ArrayList<StarcraftUnit> result = new ArrayList<StarcraftUnit>();
		ArrayList<String> unitNames = new ArrayList<String>();
		if (player.getSpecies().equals("Zerg")){
			unitNames = this.unitListZerg;
		}else if (player.getSpecies().equals("Terran")){
			unitNames = this.unitListTerran;
		}else if (player.getSpecies().equals("Protoss")){
			unitNames = this.unitListPross;
		}
		for (String unitName:unitNames){
			StarcraftUnit unitToAdd = new StarcraftUnit(unitName, player.getGame().unitIdGenerator);
			unitToAdd.setOwner(player.getName());
			unitToAdd.setStartingSituation(GameConstants.inGalaxySituation);
			unitToAdd.setColor(player.getPlayerColor());
			result.add(unitToAdd);
		}
		return result;
	}

}
