package gameEntities.playerItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BuyingOrder implements java.io.Serializable {

	private static final long serialVersionUID = 8964381431299922946L;
	
	private BuyableItem item;
	private int spentMineral = 0;
	private int spentGas = 0;
	private Map<List<Integer>, Integer> affectedWorkers = new HashMap<List<Integer>, Integer>();
	private int workerOnBaseMineral = 0;
	private int workerOnBaseGas = 0;
	private ArrayList<StarcraftUnit> spentUnits = new ArrayList<StarcraftUnit>();
	private int usedReductions = 0;
	private int mineralTokens = 0;
	private int gasTokens = 0;

	public Boolean requireGas(){
		Boolean result = false;
		if (this.item.getGasCost() > this.spentGas){
			result = true;
		}
		return result;
	}
	
	public Boolean requireMineral(){
		Boolean result = false;
		if  (this.item.getMineralCost() > this.spentMineral){
			result = true;
		}
		return result;
	}
	
	public Boolean isReady(){
		Boolean result = false;
		if (this.item.getGasCost() == this.spentGas && this.item.getMineralCost() == this.spentMineral){
			if (this.item.getClass().getName().endsWith("StarcraftUnit")){
				StarcraftUnit unit = (StarcraftUnit) this.item;
				if (unit.getUnitCostNumber() == spentUnits.size()){
					result = true;
				}
			}else{
				result = true;
			}
		}
		return result;
	}
	
	public BuyableItem getItem() {
		return item;
	}

	public void setItem(BuyableItem item) {
		this.item = item;
	}

	public int getSpentMineral() {
		return spentMineral;
	}

	public void setSpentMineral(int spentMineral) {
		this.spentMineral = spentMineral;
	}

	public int getSpentGas() {
		return spentGas;
	}

	public void setSpentGas(int spentGas) {
		this.spentGas = spentGas;
	}

	public Map<List<Integer>, Integer> getAffectedWorkers() {
		return affectedWorkers;
	}
	
	//TODO
	public void addWorkerToArea(List<Integer> coordinates, int worker){
		if (this.affectedWorkers.containsKey(coordinates)){
			int current = this.affectedWorkers.get(coordinates);
			current += worker;
			this.affectedWorkers.put(coordinates, current);
		}else{
			this.affectedWorkers.put(coordinates, worker);
		}
	}
	
	public void removeWorkerFromArea(List<Integer> coordinates, int worker){
		if (this.affectedWorkers.containsKey(coordinates)){
			int current = this.affectedWorkers.get(coordinates);
			current -= worker;
			if (current == 0){
				this.affectedWorkers.remove(coordinates);
			}else{
				this.affectedWorkers.put(coordinates, current);
			}
		}else{
			System.out.println("buying order error");
		}
	}

	public int getWorkerOnBaseMineral() {
		return workerOnBaseMineral;
	}

	public void setWorkerOnBaseMineral(int workerOnBaseMineral) {
		this.workerOnBaseMineral = workerOnBaseMineral;
	}

	public int getWorkerOnBaseGas() {
		return workerOnBaseGas;
	}

	public void setWorkerOnBaseGas(int workerOnBaseGas) {
		this.workerOnBaseGas = workerOnBaseGas;
	}

	public ArrayList<StarcraftUnit> getSpentUnits() {
		return spentUnits;
	}

	public int getUsedReductions() {
		return usedReductions;
	}

	public void setUsedReductions(int usedReductions) {
		this.usedReductions = usedReductions;
	}

	public int getGasTokens() {
		return gasTokens;
	}

	public void setGasTokens(int gasTokens) {
		this.gasTokens = gasTokens;
	}

	public int getMineralTokens() {
		return mineralTokens;
	}

	public void setMineralTokens(int mineralTokens) {
		this.mineralTokens = mineralTokens;
	}

}
