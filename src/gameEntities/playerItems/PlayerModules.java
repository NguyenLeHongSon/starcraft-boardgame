package gameEntities.playerItems;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import gameEntities.CardIdGenerator;
import gameEntities.StarcraftPlayer;
import gameEntities.methods.GameTurnHandler;

public class PlayerModules implements java.io.Serializable {

	private static final long serialVersionUID = -4938261890812157871L;
	StarcraftPlayer player;
	private Map<Integer, StarcraftModule> buyableModules = new HashMap<Integer, StarcraftModule>();
	private ArrayList<StarcraftModule> ownedModules = new ArrayList<StarcraftModule>();
	private CardIdGenerator moduleIdGenerator;
	private int moduleLimit;
	
	public void buyModule(){
		if (this.player.getBuyingOrder().getItem().getClass().getName().endsWith("StarcraftModule")){
			StarcraftModule module = (StarcraftModule) this.player.getBuyingOrder().getItem();
			this.ownedModules.add(module);
			this.buyableModules.remove(module.getId());
			if (module.getName().equals("supply")){
				this.player.increaseUnitBuildLimit();
			}else if (module.getName().equals("research")){
				this.player.setSpecialOrderLimit(this.player.getSpecialOrderLimit() + 1);
			}else if (module.getName().equals("antiAir")){
				this.player.getBonusList().add(module.getName());
			}else if (module.getName().equals("assist")){
				this.player.getBonusList().add("assistModule");
			}else if (module.getName().equals("offensive")){
				this.player.getBonusList().add("offensiveModule");
			}else if (module.getName().equals("defensive")){
				this.player.getBonusList().add("defensiveModule");
			}
			if (this.ownedModules.size() == 1){
				GameTurnHandler.showOwnedModules(this.player.getName(), this.player.getGame());
			}else{
				GameTurnHandler.updateOwnedModules(this.player, module);
			}
		}
	}
	
	public StarcraftModule getBuyableStarcraftModule(int moduleId){
		StarcraftModule result = null;
		if (this.buyableModules.containsKey(moduleId)){
			result = this.buyableModules.get(moduleId);
		}
		return result;
	}
	
	public JSONObject getUnlockedModulesJS(String action){
		JSONObject result = null;
		if (this.canBuyModule()){
			try {
				JSONArray moduleJSListJS = new JSONArray();
				for (StarcraftModule module:this.getBuyableModules()){
					//batiment à rajouter
					JSONObject moduleJS = module.getModulesJS(this.player.getFaction());
					moduleJSListJS.put(moduleJS);
				}
				result = new JSONObject()
						.put("action", action)
						.put("moduleList", moduleJSListJS);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public JSONObject getOwnedModulesJS(String action){
		JSONObject result = null;
		if (this.ownedModules.size() > 0){
			try {
				JSONArray moduleJSListJS = new JSONArray();
				for (StarcraftModule module:this.ownedModules){
					//batiment à rajouter
					JSONObject moduleJS = module.getModulesJS(this.player.getFaction());
					moduleJSListJS.put(moduleJS);
				}
				result = new JSONObject()
						.put("action", action)
						.put("moduleList", moduleJSListJS);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	
	public Boolean canBuyModule(){
		Boolean result = false;
		if (this.ownedModules.size() < this.moduleLimit && this.player.getUnitBuildLimit() > this.player.getUnitBuilt()){
			result = true;
		}
		return result;
	}
	
	public PlayerModules(StarcraftPlayer player, CardIdGenerator generator){
		this.player = player;
		this.moduleIdGenerator = generator;
	}

	
	public void initializeBuyableModules(){
		if (this.player.getSpecies().equals("Zerg")){
			this.moduleLimit = 5;
		}else{
			this.moduleLimit = 6;
		}
		try {
			URL resources = getClass().getClassLoader().getResource("../../starcraftResources/moduleList.xml");
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile("/root/faction[@name =\""+ this.player.getFaction() +"\"]");
			Object o = expr.evaluate(doc, XPathConstants.NODE);
			Node selectedNode = (Node) o;
			NodeList list = selectedNode.getChildNodes();

			for (int i = 0; i < list.getLength(); i++){
				//TODO
				Node moduleNode = (Node) list.item(i);
				String nodeName = moduleNode.getNodeName();
				if (nodeName.equals("module")){
					NamedNodeMap moduleAttributes = moduleNode.getAttributes();
					String moduleName = moduleAttributes.getNamedItem("name").getNodeValue();
					String imageName = moduleAttributes.getNamedItem("image").getNodeValue();
					int amount = Integer.parseInt(moduleAttributes.getNamedItem("amount").getNodeValue());
					int mineralCost = 0;
					int gasCost = 0;
					String moduleText = "";
					NodeList propertyList = moduleNode.getChildNodes();
					for (int j = 0; j < propertyList.getLength(); j++){
						Node property = (Node) propertyList.item(j);
						String propertyName = property.getNodeName();
						NamedNodeMap propertyAttributes = property.getAttributes();
						if (propertyName.equals("cost")){
							if (propertyAttributes.getNamedItem("mineral")!=null){
								mineralCost = Integer.parseInt(propertyAttributes.getNamedItem("mineral").getNodeValue());
							}
							if (propertyAttributes.getNamedItem("gas")!=null){
								gasCost = Integer.parseInt(propertyAttributes.getNamedItem("gas").getNodeValue());
							}
						}else if (propertyName.equals("text")){
							moduleText = property.getTextContent();
						}
					}
					for (int k = 0; k < amount; k++){
						StarcraftModule newModule = new StarcraftModule(this.moduleIdGenerator);
						newModule.setName(moduleName);
						newModule.setImage(imageName);
						if (mineralCost > 0){
							newModule.setMineralCost(mineralCost);
						}
						if (gasCost > 0){
							newModule.setGasCost(gasCost);
						}
						newModule.setText(moduleText);
						this.buyableModules.put(newModule.getId(), newModule);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public ArrayList<StarcraftModule> getBuyableModules() {
		ArrayList<StarcraftModule> result = new ArrayList<StarcraftModule>();
		for (int key:this.buyableModules.keySet()){
			result.add(this.buyableModules.get(key));
		}
		return result;
	}

	public ArrayList<StarcraftModule> getOwnedModules() {
		return ownedModules;
	}

}
