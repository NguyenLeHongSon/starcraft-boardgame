package gameEntities.playerItems;

import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.CardIdGenerator;

public class StarcraftModule extends BuyableItem {

	private static final long serialVersionUID = 1776589457446066616L;
	private int id;
	private String name;
	private String image;
	private String text;
	
	
	public StarcraftModule(CardIdGenerator generator){
		this.id = generator.getNextValue();
	}


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public JSONObject getModulesJS(String faction){
		JSONObject result = null;
		try {
			String tooltip = this.name + " module</br>" + this.text;
			result= new JSONObject()
					.put("image", this.getImage())
					.put("id", this.getId())
					.put("faction", faction)
					.put("tooltip", tooltip);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}

}
