package gameEntities.playerItems;

public class CardActivationItem extends BuyableItem {

	private static final long serialVersionUID = -3267620532734048461L;
	
	private String bonusName;
	private String type;
	private CombatCard combatCard;

	public String getBonusName() {
		return bonusName;
	}

	public void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CombatCard getCombatCard() {
		return combatCard;
	}

	public void setCombatCard(CombatCard combatCard) {
		this.combatCard = combatCard;
	}

}
