package gameEntities.playerItems;

import java.util.ArrayList;

import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.methods.GameTurnHandler;

public class PlayerEventCards implements java.io.Serializable {

	private static final long serialVersionUID = 8608271681253983123L;

	private StarcraftPlayer player;
	private EventCard startegyCard;
	private ArrayList<EventCard> eventCardList = new ArrayList<EventCard>();
	private ArrayList<EventCard> usableEventCards = new ArrayList<EventCard>();
	private ArrayList<EventCard> actionCards = new ArrayList<EventCard>();
	private int eventCardNumber = 0;
	private ArrayList<StarcraftUnit> eventUnits = new ArrayList<StarcraftUnit>();
	
	public void drawEventCardFromDeck(int cardId){
		for (EventCard eventCard:this.player.getGame().getEventCards1()){
			if (eventCard.getId() == cardId){
				this.player.getGame().getEventCards1().remove(eventCard);
				this.eventCardList.add(eventCard);
				break;
			}
		}
	}

	public EventCard getStartegyCard(){
		return this.startegyCard;
	}
	
	public PlayerEventCards(StarcraftPlayer player){
		this.player = player;
	}
	
	public ArrayList<EventCard> getActionCards(){
		return this.actionCards;
	}
	
	public ArrayList<EventCard> getEventCardList(){
		return this.eventCardList;
	}
	
	public ArrayList<EventCard> getUsableEventCards(){
		return this.usableEventCards;
	}
	
	public void drawAllEventCards(StarcraftGame game){
		while (game.getEventCards1().size() > 0){
			this.eventCardList.add(game.getEventCards1().get(0));
			game.getEventCards1().remove(0);
		}
	}
	
	public void drawEventCard(StarcraftGame game){
		if (game.getEventCards1().size() > 0){
			this.eventCardList.add(game.getEventCards1().get(0));
			game.getEventCards1().remove(0);
			GameTurnHandler.updateDrawnEventCardNumber(this.player);
		}
	}
	
	public void researchAndResourceTokenBonus(){
		this.player.addResourceToken("gas");
		this.player.addResourceToken("mineral");
		GameTurnHandler.updateResourceTokenDisplay(this.player.getName(), this.player.getGame());
		String cardName = "";
		if (this.player.getSpecies().equals("Zerg")){
			cardName = "parasite";
		}else if (this.player.getSpecies().equals("Terran")){
			cardName = "restoration";
		}else{
			cardName = "cloaking field";
		}
		if (this.player.getBuyableCards().containsKey(cardName)){
			CombatCard cardToAdd = this.player.getBuyableCards().get(cardName).get(0);
			this.player.getGlobalCards().put(cardName, cardToAdd);
			this.player.getBonusList().add(cardToAdd.getGlobalBonus());
			this.player.getBuyableCards().remove(cardName);
			GameTurnHandler.updateActiveCombatCards(this.player.getName(), this.player.getGame());
		}
	}
	
	public void applyBonuses(StarcraftGame game){
		for (EventCard card:this.actionCards){
			game.addSpecialTurn(this.player.getName(), card.getBonusName());
			game.addSpecialTurn(this.player.getName(), "emptyTurn");
			if (card.getBonusName().equals("buyBonusResearch")){
				game.addSpecialTurn(this.player.getName(), "buyBonusResearchTempBonus", 1);
				game.addSpecialTurn(this.player.getName(), "endExecutionTurn", 3);
			}else if (card.getBonusName().equals("exhaustResourceArea")){
				game.addSpecialTurn(this.player.getName(), "exhaustResourceAreaTempBonus", 1);
			}else if (card.getBonusName().equals("freeUnits")){
				this.eventUnits = card.getUnitList(this.player);
			}
		}
		this.actionCards = new ArrayList<EventCard>();
		this.eventCardList = new ArrayList<EventCard>();
		this.eventCardNumber = 0;
		GameTurnHandler.updateDrawnEventCardNumber(this.player);
	}
	
	public void useEventCard(int cardId, StarcraftGame game){
		EventCard card = null;
		for (EventCard eventCard:this.eventCardList){
			if (eventCard.getId() == cardId){
				card = eventCard;
				break;
			}
		}
		this.eventCardNumber++;
		if (card.getType().equals("strategy")){
			if (this.startegyCard != null){
				this.player.getBonusList().remove(this.startegyCard.getBonusName());
			}
			this.startegyCard = card;
			this.player.getBonusList().add(card.getBonusName());
			GameTurnHandler.updateActiveEventCardNumber(this.player.getName(), game);
		}else if (card.getType().equals("trigger")){
			this.usableEventCards.add(card);
			GameTurnHandler.updateActiveEventCardNumber(this.player.getName(), game);
		}else{
			this.actionCards.add(card);
		}
		this.eventCardList.remove(card);
	}
	
	public Boolean canChooseEventCard(){
		Boolean result = false;
		if (this.eventCardList.size() > 0){
			int limit = 1;
			if (player.getBonusList().contains("cardEventBonus")){
				limit++;
			}
			if (this.eventCardNumber < limit){
				result = true;
			}
		}
		return result;
	}

	public ArrayList<StarcraftUnit> getEventUnits() {
		return eventUnits;
	}
	
	public void sendFreeUnits(int x, int y, int z){
		for (StarcraftUnit unit:this.eventUnits){
			this.player.getGame().getGalaxy().addUnit(unit);
			unit.setCoordinates(new int[]{x, y, z}, this.player.getGame());
			unit.updateOldCoordinates();
			GameTurnHandler.updateUnitDisplay(unit.getId(), this.player.getGame());
		}
		this.eventUnits = new ArrayList<StarcraftUnit>();
	}

}
