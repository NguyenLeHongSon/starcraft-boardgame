package gameEntities.methods;

import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.gameMap.Planet;
import gameEntities.gameMap.PlanetArea;

public class ResourceHandler {

	public void addEndRestorationTurnButton(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				JSONObject addEndRestorationTurnButton = new JSONObject()
						.put("action", "addEndRestorationTurnButton");
				GlobalMethods.sendPlayerAction(player, addEndRestorationTurnButton);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**fonction mettant en place les évènements de restoration de zones ressource sur la galaxie**/
	public void addExhaustAreasEvent(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getTempBonusList().contains("exhaustArea2")
					|| starcraftPlayer.getTempBonusList().contains("exhaustArea1")){
				try {
					JSONArray coordinateArray = new JSONArray();
					for (String planetName:game.getGalaxy().getAllPlanets().keySet()){
						Planet planet = game.getGalaxy().getAllPlanets().get(planetName);
						for (PlanetArea area:planet.getAreaList()){
							Boolean areaIsImmune = false;
							for (String playerName:game.getPlayerList().keySet()){
								StarcraftPlayer otherPlayer = game.getPlayerList().get(playerName);
								if (otherPlayer.getBonusList().contains("RichesOfAiur")){
									if (otherPlayer.getAreaResourceList()
											.containsKey(Arrays.asList(planet.getX(), planet.getY(), area.getId()))){
										areaIsImmune = true;
										break;
									}
								}
							}
							if (!areaIsImmune && area.getExhaustion() < 2
									&& area.getMineralResources() + area.getGasResources() > 0){
								JSONObject coordinateJS = new JSONObject()
										.put("coordinate", String.valueOf(planet.getX()) + "."
												+ String.valueOf(planet.getY()))
										.put("areaId", area.getId());
								coordinateArray.put(coordinateJS);
							}
						}
					}
					if (coordinateArray.length() > 0){
						JSONObject addExhaustAreasEvent = new JSONObject()
								.put("action", "addExhaustAreasEvent")
								.put("coordinates", coordinateArray);
						GlobalMethods.sendPlayerAction(player, addExhaustAreasEvent);
					}else{
						endRestorationTurn(player, game);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else{
				endRestorationTurn(player, game);
			}
		}
	}
	
	/**fonction mettant en place les évènements de restoration de zones ressource sur la galaxie**/
	public void addRestoreAreasEvent(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getTempBonusList().contains("restoreArea2")
					|| starcraftPlayer.getTempBonusList().contains("restoreArea1")){
				try {
					JSONArray coordinateArray = new JSONArray();
					for (String planetName:game.getGalaxy().getAllPlanets().keySet()){
						Planet planet = game.getGalaxy().getAllPlanets().get(planetName);
						for (PlanetArea area:planet.getAreaList()){
							if (area.getExhaustion() == 1){
								JSONObject coordinateJS = new JSONObject()
										.put("coordinate", String.valueOf(planet.getX()) + "."
												+ String.valueOf(planet.getY()))
										.put("areaId", area.getId());
								coordinateArray.put(coordinateJS);
							}
						}
					}
					if (coordinateArray.length() > 0){
						JSONObject addRestoreAreasEvent = new JSONObject()
								.put("action", "addRestoreAreasEvent")
								.put("coordinates", coordinateArray);
						GlobalMethods.sendPlayerAction(player, addRestoreAreasEvent);
					}else{
						endRestorationTurn(player, game);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else{
				endRestorationTurn(player, game);
			}
		}
	}
	
	public void updateExhaustionDisplay(int[] coordinates, int exhaustion, StarcraftGame game){
		String stringCoordinate = Integer.toString(coordinates[0]) + "." + Integer.toString(coordinates[1]);
		try {
			JSONObject updateExhaustionDisplay = new JSONObject()
					.put("action", "updateExhaustionDisplay")
					.put("coordinates", stringCoordinate)
					.put("areaId", coordinates[2])
					.put("exhaustion", exhaustion);
			for (String playerName:game.getPlayerList().keySet()){
				GlobalMethods.sendPlayerAction(playerName, updateExhaustionDisplay);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**affiche les jetons de ressources que l'on peut rajouter au joueur**/
	public void displayResourcesTokenChoice(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				JSONObject displayResourcesToken = new JSONObject()
						.put("action", "displayResourcesToken");
				GlobalMethods.sendPlayerAction(player, displayResourcesToken);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void addCostReductionButton(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			GlobalMethods.clearByClass(player, "menuItem");
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getTempBonusList().contains("costReduction")){
				try {
					JSONObject displayCostReduction = new JSONObject()
							.put("action", "displayCostReduction");
					GlobalMethods.sendPlayerAction(player, displayCostReduction);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public void updateBaseWorkerDisplay(StarcraftPlayer player){
		try {
			if (player.getWorker() != null){
				JSONObject updateBaseWorkerDisplay = new JSONObject()
						.put("action", "updateBaseWorkerDisplay")
						.put("availableWorkers", player.getAvailableWorkers())
						.put("unavailableWorkers", player.getUnavailableWorkers());
				GlobalMethods.sendPlayerAction(player.getName(), updateBaseWorkerDisplay);
				StarcraftGame game = player.getGame();
				JSONObject updateBaseWorkerDisplay2 = new JSONObject()
						.put("action", "updateBaseWorkerDisplay2")
						.put("playerName", player.getName())
						.put("availableWorkers", player.getAvailableWorkers())
						.put("unavailableWorkers", player.getUnavailableWorkers());
				for (String playerName:game.getPlayerList().keySet()){
					if (!playerName.equals(player.getName())){
						GlobalMethods.sendPlayerAction(playerName, updateBaseWorkerDisplay2);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**affiche le nombre de travailleur dans toutes les zones contenant des travailleurs**/
	public void displayAllWorkersInGalaxy(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			StarcraftPlayer workerOwner = game.getPlayerList().get(playerName);
			for (List<Integer> coordinate:workerOwner.getAreaResourceList().keySet()){
				if (workerOwner.getAreaResourceList().get(coordinate).getWorkerAmount() > 0){
					displayWorkersOnArea(player, coordinate, workerOwner, game);
				}
			}
		}
	}
	
	/**affiche le nombre de travailleur dans la zone choisie**/
	public void displayWorkersOnArea(String player, List<Integer> coordinate, StarcraftPlayer workerOwner, StarcraftGame game){
		PlanetArea area = workerOwner.getAreaResourceList().get(coordinate);
		try {
			JSONObject displayWorkersOnArea = new JSONObject()
					.put("action", "displayWorkersOnArea")
					.put("species", workerOwner.getSpecies())
					.put("image", workerOwner.getWorker().getImage())
					.put("number", area.getWorkerAmount())
					.put("coordinate", String.valueOf(coordinate.get(0)) + "." + String.valueOf(coordinate.get(1)))
					.put("areaId", coordinate.get(2))
					.put("color", workerOwner.getPlayerColor())
					.put("tooltip", workerOwner.getWorker().getTooltip());
			GlobalMethods.sendPlayerAction(player, displayWorkersOnArea);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	public void displayBaseWorkers(StarcraftPlayer player){
		try {
			if (player.getWorker() != null){
				JSONObject displayBaseWorkers = new JSONObject()
						.put("action", "displayBaseWorkers")
						.put("species", player.getSpecies())
						.put("image", player.getWorker().getImage())
						.put("availableWorkers", player.getAvailableWorkers())
						.put("unavailableWorkers", player.getUnavailableWorkers());
				GlobalMethods.sendPlayerAction(player.getName(), displayBaseWorkers);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void fetchBaseWorkersInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			StarcraftPlayer currentPlayer = game.getPlayer(playerName);
			if (!playerName.equals(player)){
				displayBaseWorkers2(currentPlayer, player);
			}
		}
	}
	
	private void displayBaseWorkers2(StarcraftPlayer player, String playerName){
		try {
			if (player.getWorker() != null){
				JSONObject displayBaseWorkers = new JSONObject()
						.put("action", "displayBaseWorkers2")
						.put("playerName", player.getName())
						.put("species", player.getSpecies())
						.put("image", player.getWorker().getImage())
						.put("availableWorkers", player.getAvailableWorkers())
						.put("unavailableWorkers", player.getUnavailableWorkers());
				GlobalMethods.sendPlayerAction(playerName, displayBaseWorkers);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void displayWorkersOnBaseResources(StarcraftPlayer player){
		try {
			if (player.getWorker() != null){
				JSONObject displayWorkersOnBaseResources = new JSONObject()
						.put("action", "displayWorkersOnBaseResources")
						.put("species", player.getSpecies())
						.put("image", player.getWorker().getImage())
						.put("workerOnMineral", player.getWorkerOnMineral())
						.put("workerOnGas", player.getWorkerOnGas());
				GlobalMethods.sendPlayerAction(player.getName(), displayWorkersOnBaseResources);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void sendUpdateWorkersOnBaseResources(StarcraftPlayer player){
		StarcraftGame game = player.getGame();
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player.getName())){
				displayWorkersOnBaseResources2(player, playerName);
			}
		}
	}
	
	public void fetchWorkersOnBaseResources(StarcraftPlayer player){
		StarcraftGame game = player.getGame();
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player.getName())){
				displayWorkersOnBaseResources2(game.getPlayerList().get(playerName), player.getName());
			}
		}
	}
	
	private void displayWorkersOnBaseResources2(StarcraftPlayer player, String playerName){
		try {
			if (player.getWorker() != null){
				JSONObject displayWorkersOnBaseResources = new JSONObject()
						.put("action", "displayWorkersOnBaseResources2")
						.put("playerName", player.getName())
						.put("species", player.getSpecies())
						.put("image", player.getWorker().getImage())
						.put("workerOnMineral", player.getWorkerOnMineral())
						.put("workerOnGas", player.getWorkerOnGas());
				GlobalMethods.sendPlayerAction(playerName, displayWorkersOnBaseResources);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void displayPlayerResourceInfo(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getLeadershipcards().size() > 0){
			try {
				JSONObject displayPlayerResources = new JSONObject()
						.put("action", "displayPlayerResources")
						.put("mineral", starcraftPlayer.getMineralResources())
						.put("gas", starcraftPlayer.getGasResources());
				GlobalMethods.sendPlayerAction(player, displayPlayerResources);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fetchPlayerResourceInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayPlayerResourceInfo2(playerName, game, player);
			}
		}
	}
	
	private void displayPlayerResourceInfo2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getLeadershipcards().size() > 0){
			try {
				JSONObject displayPlayerResources = new JSONObject()
						.put("playerName", player)
						.put("action", "displayPlayerResources2")
						.put("mineral", starcraftPlayer.getMineralResources())
						.put("gas", starcraftPlayer.getGasResources());
				GlobalMethods.sendPlayerAction(playerName, displayPlayerResources);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void displayPlayerResourceToken(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getLeadershipcards().size() > 0){
			try {
				JSONObject displayPlayerResourceToken = new JSONObject()
						.put("action", "displayPlayerResourceToken")
						.put("mineral", starcraftPlayer.getMineralToken())
						.put("gas", starcraftPlayer.getGasToken());
				GlobalMethods.sendPlayerAction(player, displayPlayerResourceToken);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void sendUpdateResourceTokensInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayPlayerResourceToken2(player, game, playerName);
			}
		}
	}
	
	public void fetchResourceTokensInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayPlayerResourceToken2(playerName, game, player);
			}
		}
	}
	
	private void displayPlayerResourceToken2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getLeadershipcards().size() > 0){
			try {
				JSONObject displayPlayerResourceToken2 = new JSONObject()
						.put("action", "displayPlayerResourceToken2")
						.put("playerName", player)
						.put("mineral", starcraftPlayer.getMineralToken())
						.put("gas", starcraftPlayer.getGasToken());
				GlobalMethods.sendPlayerAction(playerName, displayPlayerResourceToken2);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public void addTokenResource(String playerName, String resourceType, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.addResourceToken(resourceType);
		displayPlayerResourceToken(playerName, game);
		sendUpdateResourceTokensInfo(playerName, game);
		if (starcraftPlayer.getResourcesTokenToChose() == 0){
			game.nextTurn();
			for (String player : game.getPlayerList().keySet()){
				GameTurnHandler.printNextTurnScreen(player, game);
			}
		}
	}
	
	public void endRestorationTurn(String playerName, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.resetTempBonuses();
		game.nextTurn();
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

	public void sendRestoreArea(String playerName, int x, int y, int areaId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		game.getGalaxy().getAreaAt(x, y, areaId).setExhaustion(0);
		this.updateExhaustionDisplay(new int[]{x,  y,  areaId}, 0, game);
		if (starcraftPlayer.getTempBonusList().contains("restoreArea1")){
			starcraftPlayer.getTempBonusList().remove("restoreArea1");
			addRestoreAreasEvent(playerName, game);
		}else if (starcraftPlayer.getTempBonusList().contains("restoreArea2")){
			starcraftPlayer.getTempBonusList().remove("restoreArea2");
			addRestoreAreasEvent(playerName, game);
		}else{
			game.nextTurn();
			for (String player : game.getPlayerList().keySet()){
				GameTurnHandler.printNextTurnScreen(player, game);
			}
		}
	}

	public void sendExhaustArea(String playerName, int x, int y, int areaId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		PlanetArea area = game.getGalaxy().getAreaAt(x, y, areaId);
		area.setExhaustion(area.getExhaustion() + 1);
		this.updateExhaustionDisplay(new int[]{x,  y,  areaId}, area.getExhaustion(), game);
		if (starcraftPlayer.getTempBonusList().contains("exhaustArea1")){
			starcraftPlayer.getTempBonusList().remove("exhaustArea1");
			this.addExhaustAreasEvent(playerName, game);
		}else if (starcraftPlayer.getTempBonusList().contains("exhaustArea2")){
			starcraftPlayer.getTempBonusList().remove("exhaustArea2");
			this.addExhaustAreasEvent(playerName, game);
		}else{
			game.nextTurn();
			for (String player : game.getPlayerList().keySet()){
				GameTurnHandler.printNextTurnScreen(player, game);
			}
		}
	}
}
