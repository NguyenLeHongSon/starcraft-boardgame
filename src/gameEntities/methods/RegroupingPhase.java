package gameEntities.methods;

import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.playerItems.CombatCard;

public class RegroupingPhase {
	
	/**affiche les cartes de la main du joueur**/
	public void showWinner(String player, StarcraftGame game){
		try {
			JSONObject showWinner = new JSONObject()
					.put("action", "showWinner")
					.put("name", game.getWinner());
			GlobalMethods.sendPlayerAction(player, showWinner);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**affiche les cartes de la main du joueur**/
	public void displayCardChoice(String player, StarcraftGame game){
		StarcraftPlayer activeplayer= game.getPlayer(player);
		for (int combatCardId:activeplayer.getCombatCardsInHand().keySet()){
			JSONObject combatCardJS = activeplayer.getCombatCardsInHand().get(combatCardId).getCardJS("displayCardChoice");
			GlobalMethods.sendPlayerAction(player, combatCardJS);
		}
	}
	
	/**ajoute les boutons de défausse de carte**/
	public void addDiscardHandTurnButton(String player, StarcraftGame game){
		JSONObject addDiscardHandTurnButton;
		try {
			addDiscardHandTurnButton = new JSONObject()
					.put("action", "addDiscardHandTurnButton");
			GlobalMethods.sendPlayerAction(player, addDiscardHandTurnButton);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void sendEndTurn(String player, StarcraftGame game){
		if (player.equals(game.getPlayerCurrentlyPlaying())){
			StarcraftPlayer starcraftPlayer = game.getPlayer(player);
			Boolean end = false;
			if (starcraftPlayer.getCardLimit() >= starcraftPlayer.getCombatCardsInHand().size()){
				end = true;
				GlobalMethods.clearObjectEvents(player);
			}
			JSONObject sendEndTurn;
			try {
				sendEndTurn = new JSONObject()
						.put("action", "checkEndTurn")
						.put("disabledButton", !end);
				GlobalMethods.sendPlayerAction(player, sendEndTurn);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	//Actions joueurs

	/****/
	public void discardCard(String playerName, int cardId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayer(playerName);
		CombatCard card = starcraftPlayer.getCombatCardsInHand().get(cardId);
		starcraftPlayer.getCombatCardsInHand().remove(cardId);
		starcraftPlayer.getDismissedCombatCards().add(card);
		try {
			JSONObject removeCardDisplay = new JSONObject()
					.put("action", "removeCardDisplay")
					.put("name", cardId);
			GlobalMethods.sendPlayerAction(playerName, removeCardDisplay);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		sendEndTurn(playerName, game);
		GameTurnHandler.callUpdateCardNumber(starcraftPlayer);
	}

	public void endDiscardHandTurn(String playerName, StarcraftGame game) {
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
		
	}
	
}
