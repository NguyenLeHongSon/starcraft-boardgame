package gameEntities.methods;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.GameConstants;
import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.playerItems.BuyableItem;
import gameEntities.playerItems.CombatCard;
import gameEntities.playerItems.StarcraftBuilding;
import gameEntities.playerItems.StarcraftModule;
import gameEntities.playerItems.StarcraftUnit;

/**classe gérant la construction**/
public class BuildTurnHandler {
	public void removeCostReduction(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (!starcraftPlayer.getTempBonusList().contains("costReduction")){
			try {
				JSONObject removeCostReduction = new JSONObject()
						.put("action", "removeCostReduction");
				GlobalMethods.sendPlayerAction(player, removeCostReduction);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void desactivateCostReduction(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getTempBonusList().contains("costReduction")){
			try {
				JSONObject desactivateCostReduction = new JSONObject()
						.put("action", "desactivateCostReduction");
				GlobalMethods.sendPlayerAction(player, desactivateCostReduction);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void activateCostReduction(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getTempBonusList().contains("costReduction") && starcraftPlayer.getBuyingOrder() != null){
			try {
				BuyableItem item = starcraftPlayer.getBuyingOrder().getItem();
				int requiredMineral = item.getMineralCost() - starcraftPlayer.getBuyingOrder().getSpentMineral();
				int requiredGas = item.getGasCost() - starcraftPlayer.getBuyingOrder().getSpentGas();
				Boolean requireMineral = false;
				Boolean requireGas = false;
				if (requiredMineral > 0){
					requireMineral = true;
				}
				if (requiredGas > 0){
					requireGas = true;
				}
				if (requireMineral || requireGas){
					JSONObject activateCostReduction = new JSONObject()
							.put("action", "activateCostReduction")
							.put("activateMineral", requireMineral)
							.put("activateGas", requireGas);
					GlobalMethods.sendPlayerAction(player, activateCostReduction);
				}else{
					desactivateCostReduction(player, game);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**vérifie si le joueur peut passer au tour suivant**/
	public void checkEndTurn(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			boolean disabledButton = true;
			if (starcraftPlayer.getBuyingOrder()==null){
				disabledButton = false;
			}
			try {
				//setOnOffCancelBuyOrder
				JSONObject checkEndTurn = new JSONObject()
						.put("action", "checkEndTurn")
						.put("disabledButton", disabledButton);
				GlobalMethods.sendPlayerAction(player, checkEndTurn);
				JSONObject setOnOffCancelBuyOrder = new JSONObject()
						.put("action", "setOnOffCancelBuyOrder")
						.put("disabledButton", !disabledButton);
				GlobalMethods.sendPlayerAction(player, setOnOffCancelBuyOrder);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void activateUnlockedBuildings(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getBuyingOrder() == null){
				if (starcraftPlayer.getUnitBuildLimit() > starcraftPlayer.getUnitBuilt()){
					try {
						JSONObject activateUnlockedBuildings = new JSONObject()
								.put("action", "activateUnlockedBuildings");
						GlobalMethods.sendPlayerAction(player, activateUnlockedBuildings);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public void activateUnlockedModules(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getBuyingOrder() == null){
				if (starcraftPlayer.canBuyModule()){
					try {
						JSONObject activateUnlockedModules = new JSONObject()
								.put("action", "activateUnlockedModules");
						GlobalMethods.sendPlayerAction(player, activateUnlockedModules);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**modifie l'alpha d'une unité dans la barre de construction d'unité**/
	public void setUnitAlpha(String player, String unitName, Double alpha){
		try {
		JSONObject setUnitAlpha = new JSONObject()
				.put("action", "setUnitAlpha")
				.put("name", unitName)
				.put("alpha", alpha);
		GlobalMethods.sendPlayerAction(player, setUnitAlpha);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void addBuildingUnitTurnButton(String player, StarcraftGame game){
		try {
			JSONObject addBuildingUnitTurnButton = new JSONObject()
					.put("action", "addBuildingUnitTurnButton");
			GlobalMethods.sendPlayerAction(player, addBuildingUnitTurnButton);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void displayUnlockedModules(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			JSONObject displayUnlockedModules = starcraftPlayer.getUnlockedModulesJS("displayUnlockedModules");
			if (displayUnlockedModules != null){
				GlobalMethods.sendPlayerAction(player, displayUnlockedModules);
			}
		}
	}
	
	public void displayUnlockedBuildings(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			JSONObject displayUnlockedBuildings = starcraftPlayer.getUnlockedBuildingsJS("displayUnlockedBuildings");
			if (displayUnlockedBuildings != null){
				GlobalMethods.sendPlayerAction(player, displayUnlockedBuildings);
			}
		}
	}
	
	/**affiche les unités que l'on peut construire**/
	public void displayUnlockedUnits(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			Boolean showUnits = true;
			if (starcraftPlayer.getBuyingOrder() != null){
				if (starcraftPlayer.getBuyingOrder().isReady()){
					showUnits = false;
				}
			}
			if (showUnits){
				JSONObject displayUnlockedUnits = starcraftPlayer.getUnlockedUnitsJS(game, "displayUnlockedUnits");
				GlobalMethods.sendPlayerAction(player, displayUnlockedUnits);
			}
		}
	}
	
	/**active les unités achetables**/
	public void activateUnlockedUnits(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getBuyingOrder() == null){
				JSONObject activateUnlockedUnits = starcraftPlayer.getUnlockedUnitsJS(game, "activateUnlockedUnits");
				GlobalMethods.sendPlayerAction(player, activateUnlockedUnits);
			}
		}
	}
	
	/**fonction mettant en place les évènements de récupération de ressource sur la galaxie**/
	public void addResourcePlacesEvent(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getBuyingOrder() != null){
				if (!starcraftPlayer.getBuyingOrder().isReady()){
					try {
						if (starcraftPlayer.getAreaResourceList().size() > 0){
							JSONObject addResourcePlacesEvent = new JSONObject()
									.put("action", "addResourcePlacesEvent");
							JSONArray coordinateArray = new JSONArray();
							for (List<Integer> coordinate:starcraftPlayer.getAreaResourceList().keySet()){
								JSONObject coordinateJS = new JSONObject()
										.put("coordinate", String.valueOf(coordinate.get(0)) + "."
								+ String.valueOf(coordinate.get(1)))
										.put("areaId", coordinate.get(2));
								coordinateArray.put(coordinateJS);
							}
							addResourcePlacesEvent.put("coordinates", coordinateArray);
							GlobalMethods.sendPlayerAction(player, addResourcePlacesEvent);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**fonction activant les différentes zones de ressources**/
	public void activateResourcePlaces(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getBuyingOrder() != null){
				if (!starcraftPlayer.getBuyingOrder().isReady()){
					try {
						BuyableItem item = starcraftPlayer.getBuyingOrder().getItem();
						if (item.getClass().getName().endsWith("StarcraftUnit")){
							StarcraftUnit unit = (StarcraftUnit) item;
							setUnitAlpha(player, unit.getName(), 0.6);
						}else if (item.getClass().getName().endsWith("StarcraftBuilding")){
							StarcraftBuilding building = (StarcraftBuilding) item;
							setUnitAlpha(player,
									Integer.toString(building.getNumber()) + "." + Integer.toString(building.getLevel()), 0.6);
						}else if (item.getClass().getName().endsWith("CombatCard")){
							CombatCard card = (CombatCard) item;
							setUnitAlpha(player, "card"+Integer.toString(card.getId()), 0.6);
						}
						
						if (starcraftPlayer.canSetWorkerOnBaseMineral()){
							JSONObject activateBaseMineral = new JSONObject()
									.put("action", "activateBaseMineral");
							GlobalMethods.sendPlayerAction(player, activateBaseMineral);
						}
						if (starcraftPlayer.canSetWorkerOnBaseGas()){
							JSONObject activateBaseGas = new JSONObject()
									.put("action", "activateBaseGas");
							GlobalMethods.sendPlayerAction(player, activateBaseGas);
						}
						if (starcraftPlayer.canUseMineralToken()){
							JSONObject activateMineralToken = new JSONObject()
									.put("action", "activateMineralToken");
							GlobalMethods.sendPlayerAction(player, activateMineralToken);
						}
						if (starcraftPlayer.canUseGasToken()){
							JSONObject activateGasToken = new JSONObject()
									.put("action", "activateGasToken");
							GlobalMethods.sendPlayerAction(player, activateGasToken);
						}
						
						activateCostReduction(player, game);
						
						int requiredMineral = item.getMineralCost() - starcraftPlayer.getBuyingOrder().getSpentMineral();
						int requiredGas = item.getGasCost() - starcraftPlayer.getBuyingOrder().getSpentGas();
						
						JSONObject showRequiredResources = new JSONObject()
								.put("action", "showRequiredResources")
								.put("gas", requiredGas)
								.put("mineral", requiredMineral);
						if (item.getClass().getName().endsWith("StarcraftUnit")){
							StarcraftUnit unit = (StarcraftUnit) item;
							if (unit.getUnitCostNumber() > 0){
								String unitImageCost = GameConstants.getUnitImage(unit.getUnitCost());
								int requiredUnits = 
										unit.getUnitCostNumber() - starcraftPlayer.getBuyingOrder().getSpentUnits().size();
								showRequiredResources.put("image", unitImageCost)
								.put("unitCost", requiredUnits)
								.put("color", starcraftPlayer.getPlayerColor())
								.put("name", "unitCost");
								//détermine les unités sacrifiables
								Set<Long> unitsToSacrifice = starcraftPlayer.getUsableResourceUnits(game);
								if (unitsToSacrifice.size() > 0){
									JSONArray unitArray = new JSONArray();
									for (long unitId:unitsToSacrifice){
										JSONObject unitJS = new JSONObject()
												.put("unitId", unitId);
										unitArray.put(unitJS);
									}
									JSONObject activateResourceUnits = new JSONObject()
											.put("action", "activateResourceUnits")
											.put("unitList", unitArray);
									GlobalMethods.sendPlayerAction(player, activateResourceUnits);
								}
							}
							
						}
						GlobalMethods.sendPlayerAction(player, showRequiredResources);
						
						Set<List<Integer>> coordinateList = starcraftPlayer.getUsableResourceAreas();
						
						if (coordinateList.size() > 0){
							JSONObject activateResourceAreas = new JSONObject()
									.put("action", "activateResourceAreas");
							JSONArray coordinateArray = new JSONArray();
							for (List<Integer> coordinate:coordinateList){
								JSONObject coordinateJS = new JSONObject()
										.put("coordinate", String.valueOf(coordinate.get(0))
												+ "." + String.valueOf(coordinate.get(1)))
										.put("areaId", coordinate.get(2));
								coordinateArray.put(coordinateJS);
							}
							activateResourceAreas.put("coordinates", coordinateArray);
							GlobalMethods.sendPlayerAction(player, activateResourceAreas);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	//TODO
	/*gestion des actions du joueur*/
	
	/**création d'une commande d'unité**/
	public void askBuyingUnitOrder(String playerName, String unitName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		StarcraftUnit unit = new StarcraftUnit(unitName, game.unitIdGenerator);
		unit.setStartingSituation("builtUnit");
		unit.setOwner(playerName);
		unit.setColor(starcraftPlayer.getPlayerColor());
		starcraftPlayer.setBuyingOrder(unit);
		//désactivation du bouton de fin de tour
		checkEndTurn(playerName, game);
		activateResourcePlaces(playerName, game);
		addResourcePlacesEvent(playerName, game);
	}
	
	/**création d'une commande d'unité**/
	public void askBuyingBuildingOrder(String playerName, int number, int level, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		StarcraftBuilding building = starcraftPlayer.getBuilding(number, level);
		starcraftPlayer.setBuyingOrder(building);
		//désactivation du bouton de fin de tour
		checkEndTurn(playerName, game);
		activateResourcePlaces(playerName, game);
		addResourcePlacesEvent(playerName, game);
	}
	
	/**création d'une commande de carte**/
	public void askBuyingCardOrder(String playerName, String cardName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		CombatCard card = starcraftPlayer.getBuyableCards().get(cardName).get(0);
		starcraftPlayer.setBuyingOrder(card);
		//désactivation du bouton de fin de tour
		checkEndTurn(playerName, game);
		activateResourcePlaces(playerName, game);
		addResourcePlacesEvent(playerName, game);
	}

	/**affecte un travailleur à une zone en vue de l'achat d'une unité**/
	public void setWorkerOnArea(String playerName, String coordinates, int areaId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		int separatorIndex = coordinates.indexOf('.');
		int xCoord  = Integer.parseInt(coordinates.substring(0, separatorIndex));
		int yCoord  = Integer.parseInt(coordinates.substring(separatorIndex + 1));
		starcraftPlayer.sendWorkerToArea(xCoord, yCoord, areaId);
		GameTurnHandler.updateAreaWorkerDisplay(Arrays.asList(xCoord, yCoord, areaId), starcraftPlayer, game);
		validateResourceSpending(playerName, game);
	}

	public void setWorkerBaseMineral(String playerName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.sendWorkerOnBaseMineral();
		GameTurnHandler.callDisplayWorkersOnBaseResources(starcraftPlayer);;
		validateResourceSpending(playerName, game);
	}
	
	public void setWorkerBaseGas(String playerName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.sendWorkerOnBaseGas();
		GameTurnHandler.callDisplayWorkersOnBaseResources(starcraftPlayer);
		validateResourceSpending(playerName, game);
	}
	
	public void useTokenResource(String playerName, String resourceType, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.useTokenResource(resourceType);
		GameTurnHandler.updateResourceTokenDisplay(playerName, game);
		validateResourceSpending(playerName, game);
	}
	
	public void sacrificeUnit(String playerName, long unitId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		StarcraftUnit unit = game.getGalaxy().getUnitList().get(unitId);
		starcraftPlayer.getBuyingOrder().getSpentUnits().add(unit);
		game.getGalaxy().removeUnit(unitId, game);
		GameTurnHandler.removeUnitDisplay(unitId, game);
		addResourcePlacesEvent(playerName, game);
		validateResourceSpending(playerName, game);
	}
	
	/****/
	public void validateResourceSpending(String playerName, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		if (starcraftPlayer.getBuyingOrder().isReady()){
			//l'achat est complété
			desactivateCostReduction(playerName, game);
			//cas où le joueur a acheté une unité
			if (starcraftPlayer.getBuyingOrder().getItem().getClass().getName().endsWith("StarcraftUnit")){
				StarcraftUnit unit = (StarcraftUnit) starcraftPlayer.getBuyingOrder().getItem();
				//dans le cas d'un travailleur
				if (unit.getType().equals("worker")){
					starcraftPlayer.setUnavailableWorkers(starcraftPlayer.getUnavailableWorkers() + 1);
					setUnitAlpha(playerName, unit.getName(), 1.0);
					starcraftPlayer.executeBuyingOrder();
					checkEndTurn(playerName, game);
					activateUnlockedUnits(playerName, game);
				}else{
					starcraftPlayer.addUnitToPlayerPool(game.getTurnPart(), unit);
					GameTurnHandler.placeBuiltUnit(playerName, game);
				}
			}else if (starcraftPlayer.getBuyingOrder().getItem().getClass().getName().endsWith("StarcraftBuilding")){
				starcraftPlayer.buildBuilding();
				starcraftPlayer.executeBuyingOrder();
				//on peut directement finir le tour dans ces cas là pour l'instant
				this.endTurn(game);
			}else if (starcraftPlayer.getBuyingOrder().getItem().getClass().getName().endsWith("CombatCard")){
				starcraftPlayer.buyCard(game);
				starcraftPlayer.executeBuyingOrder();
				this.endTurn(game);
			}else if (starcraftPlayer.getBuyingOrder().getItem().getClass().getName().endsWith("StarcraftModule")){
				starcraftPlayer.buyModule();;
				starcraftPlayer.executeBuyingOrder();
				this.endTurn(game);
			}else if (starcraftPlayer.getBuyingOrder().getItem().getClass().getName().endsWith("CardActivationItem")){
				starcraftPlayer.getSpecialActions().buyActivationCard();
				starcraftPlayer.executeBuyingOrder();
				this.endTurn(game);
			}
		}else{
			//on cherche les autres ressources nécessaires à la complétion de l'achat
			activateResourcePlaces(playerName, game);
		}
		GameTurnHandler.updateBaseWorkerDisplay(starcraftPlayer);
	}

	public void endBuildingUnitTurn(String playerName, StarcraftGame game) {
		for (long id:game.getGalaxy().getUnitList().keySet()){
			StarcraftUnit unit = game.getGalaxy().getUnitList().get(id);
			unit.updateOldCoordinates();
			unit.setStartingSituation(GameConstants.inGalaxySituation);
		}
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.setUnitBuilt(0);
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

	public void cancelBuyOrder(String playerName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		if (starcraftPlayer.getBuyingOrder() != null){
			String itemClass = starcraftPlayer.getBuyingOrder().getItem().getClass().getName();
			if (itemClass.endsWith("StarcraftUnit")){
				StarcraftUnit unit = (StarcraftUnit) starcraftPlayer.getBuyingOrder().getItem();
				setUnitAlpha(playerName, unit.getName(), 1.0);
			} else if (itemClass.endsWith("StarcraftBuilding")){
				StarcraftBuilding building = (StarcraftBuilding) starcraftPlayer.getBuyingOrder().getItem();
				String buildingName = Integer.toString(building.getNumber()) + "." + Integer.toString(building.getLevel());
				setUnitAlpha(playerName, buildingName, 1.0);
			} else if (itemClass.endsWith("CombatCard")){
				CombatCard card = (CombatCard) starcraftPlayer.getBuyingOrder().getItem();
				setUnitAlpha(playerName, "card"+Integer.toString(card.getId()), 1.0);
			} else if (itemClass.endsWith("StarcraftModule")){
				StarcraftModule module = (StarcraftModule) starcraftPlayer.getBuyingOrder().getItem();
				setUnitAlpha(playerName, "module"+Integer.toString(module.getId()), 1.0);
			}
			
			starcraftPlayer.cancelBuyingOrder(game);
			if (!itemClass.endsWith("CombatCard")){
				GameTurnHandler.displayCostReduction(playerName, game);
				desactivateCostReduction(playerName, game);
			}
		}
	}

	public void askBuyingModuleOrder(String playerName, int moduleId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		StarcraftModule module = starcraftPlayer.getStarcraftModule(moduleId);
		starcraftPlayer.setBuyingOrder(module);
		//désactivation du bouton de fin de tour
		checkEndTurn(playerName, game);
		activateResourcePlaces(playerName, game);
		addResourcePlacesEvent(playerName, game);
	}

	public void useCostReduction(String playerName, String resource, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.useCostReduction(resource);
		validateResourceSpending(playerName, game);
		removeCostReduction(playerName, game);
	}

	private void endTurn(StarcraftGame game){
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}
	
	public void cancelCardActivation(String playerName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.getSpecialActions().cancelActivationCard();
		game.addSpecialTurn(playerName, "useStartOfBattleCards", 1);
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

}
