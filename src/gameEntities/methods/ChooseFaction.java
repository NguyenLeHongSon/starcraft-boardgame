package gameEntities.methods;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import gameEntities.GameConstants;
import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.playerItems.StarcraftModule;
import gameEntities.playerItems.StarcraftUnit;

/**méthodes gérant le choix de la faction par les joueurs**/
public class ChooseFaction {
	
	public void updateOwnedModules(StarcraftPlayer player, StarcraftModule module){
		try {
			if (player.getWorker() != null){
				JSONObject updateOwnedModules = module.getModulesJS(player.getFaction());
				updateOwnedModules.put("action", "updateOwnedModules");
				GlobalMethods.sendPlayerAction(player.getName(), updateOwnedModules);
				StarcraftGame game = player.getGame();
				
				JSONObject updateOwnedModules2 =module.getModulesJS(player.getFaction());
				updateOwnedModules2.put("action", "updateOwnedModules2")
				.put("playerName", player.getName());
				for (String playerName:game.getPlayerList().keySet()){
					if (!playerName.equals(player.getName())){
						GlobalMethods.sendPlayerAction(playerName, updateOwnedModules2);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void showOwnedModules(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getOwnedModules() != null && starcraftPlayer.getOwnedModules().size() > 0){
			JSONObject showOwnedModules = starcraftPlayer.getOwnedModulesJS("showOwnedModules");
			GlobalMethods.sendPlayerAction(player, showOwnedModules);
		}
	}
	
	public void fetchOwnedModules(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				showOwnedModules2(playerName, game, player);
			}
		}
	}
	
	public void sendUpdateOwnedModules(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				showOwnedModules2(player, game, playerName);
			}
		}
	}
	
	private void showOwnedModules2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getOwnedModules() != null && starcraftPlayer.getOwnedModules().size() > 0){
			try {
				JSONObject showOwnedModules2 = starcraftPlayer.getOwnedModulesJS("showOwnedModules2");
				showOwnedModules2.put("playerName", player);
				GlobalMethods.sendPlayerAction(playerName, showOwnedModules2);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void updateUnlockedUnits(StarcraftPlayer player, String unitName){
		String unitImage = GameConstants.getUnitImage(unitName);
		try {
			if (player.getWorker() != null){
				JSONObject updateUnlockedUnits = new JSONObject()
						.put("action", "updateUnlockedUnits")
						.put("species", player.getSpecies())
						.put("image", unitImage);
				GlobalMethods.sendPlayerAction(player.getName(), updateUnlockedUnits);
				StarcraftGame game = player.getGame();
				JSONObject updateBaseWorkerDisplay2 = new JSONObject()
						.put("action", "updateUnlockedUnits2")
						.put("playerName", player.getName())
						.put("species", player.getSpecies())
						.put("image", unitImage);
				for (String playerName:game.getPlayerList().keySet()){
					if (!playerName.equals(player.getName())){
						GlobalMethods.sendPlayerAction(playerName, updateBaseWorkerDisplay2);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	/**affiche les unités de réserve**/
	public void reservedUnitsInfo(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getBonusList().contains("covertOperations")){
			JSONObject reservedUnitsInfo =
					starcraftPlayer.getUnitsFromPoolJS(game, "reservedUnitsInfo", GameConstants.moveUnitTurnName);
			GlobalMethods.sendPlayerAction(player, reservedUnitsInfo);
		}
		if (starcraftPlayer.getBonusList().contains("conclaveFleet")){
			JSONObject reservedUnitsInfo =
					starcraftPlayer.getUnitsFromPoolJS(game, "reservedUnitsInfo", "conclaveFleetTurn");
			GlobalMethods.sendPlayerAction(player, reservedUnitsInfo);
		}
	}
	
	public void fetchReservedUnits(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				reservedUnitsInfo2(playerName, game, player);
			}
		}
	}
	
	/**affiche les unités de réserve des ennemis**/
	private void reservedUnitsInfo2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getBonusList().contains("covertOperations")){
			try {
				JSONObject reservedUnitsInfo2 =
						starcraftPlayer.getUnitsFromPoolJS(game, "reservedUnitsInfo2", GameConstants.moveUnitTurnName);
				reservedUnitsInfo2.put("playerName", player);
				GlobalMethods.sendPlayerAction(playerName, reservedUnitsInfo2);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (starcraftPlayer.getBonusList().contains("conclaveFleet")){
			try {
				JSONObject reservedUnitsInfo2 =
						starcraftPlayer.getUnitsFromPoolJS(game, "reservedUnitsInfo2", "conclaveFleetTurn");
				reservedUnitsInfo2.put("playerName", player);
				GlobalMethods.sendPlayerAction(playerName, reservedUnitsInfo2);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void addReserveUnitDisplay(String player, StarcraftGame game, StarcraftUnit unit){
		StarcraftPlayer starcraftPlayer = game.getPlayer(player);
		try {
			JSONObject addReserveUnitDisplay = new JSONObject()
					.put("action", "addReserveUnitDisplay")
					.put("species", starcraftPlayer.getSpecies())
					.put("unitId", unit.getId())
					.put("image", unit.getImage());
			GlobalMethods.sendPlayerAction(player, addReserveUnitDisplay);
			
			JSONObject addReserveUnitDisplay2 = new JSONObject()
					.put("action", "addReserveUnitDisplay2")
					.put("species", starcraftPlayer.getSpecies())
					.put("unitId", unit.getId())
					.put("image", unit.getImage())
					.put("playerName", player);
			for (String playerName:game.getPlayerList().keySet()){
				if (!playerName.equals(player)){
					GlobalMethods.sendPlayerAction(playerName, addReserveUnitDisplay2);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void removeReservedUnitDisplay(String player, StarcraftGame game, long unitId){
		try {
			JSONObject removeReserveUnitDisplay = new JSONObject()
					.put("action", "removeReserveUnitDisplay")
					.put("unitId", unitId);
			GlobalMethods.sendPlayerAction(player, removeReserveUnitDisplay);
			
			JSONObject removeReserveUnitDisplay2 = new JSONObject()
					.put("action", "removeReserveUnitDisplay2")
					.put("unitId", unitId)
					.put("playerName", player);
			for (String playerName:game.getPlayerList().keySet()){
				if (!playerName.equals(player)){
					GlobalMethods.sendPlayerAction(playerName, removeReserveUnitDisplay2);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**affiche les unités que l'on peut construire**/
	public void showUnlockedUnitsInfo(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getUnlockedUnits().size() > 0){
			JSONObject displayUnlockedUnits = starcraftPlayer.getUnlockedUnitsJS2(game, "showUnlockedUnitsInfo");
			GlobalMethods.sendPlayerAction(player, displayUnlockedUnits);
		}
	}
	
	public void fetchUnlockedUnits(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				showUnlockedUnitsInfo2(playerName, game, player);
			}
		}
	}
	
	/**affiche les unités que l'on peut construire**/
	private void showUnlockedUnitsInfo2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getUnlockedUnits().size() > 0){
			JSONObject displayUnlockedUnits = starcraftPlayer.getUnlockedUnitsJS2(game, "showUnlockedUnitsInfo2");
			try {
				displayUnlockedUnits.put("playerName", player);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			GlobalMethods.sendPlayerAction(playerName, displayUnlockedUnits);
		}
	}
	
	public void updateCardNumber(StarcraftPlayer player){
		try {
			JSONObject updateCardNumber = new JSONObject()
					.put("action", "updateCardNumber")
					.put("cardNumber", player.getCombatCardsInHand().size());
			GlobalMethods.sendPlayerAction(player.getName(), updateCardNumber);
			StarcraftGame game = player.getGame();
			for (String playerName:game.getPlayerList().keySet()){
				if (!playerName.equals(player.getName())){
					JSONObject updateCardNumber2 = new JSONObject()
							.put("action", "updateCardNumber2")
							.put("playerName", player.getName())
							.put("cardNumber", player.getCombatCardsInHand().size());
					GlobalMethods.sendPlayerAction(playerName, updateCardNumber2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**affiche les choix possible de faction**/
	public void printFactionChoice(String playerName, StarcraftGame game){

		//enlève l'ancien affichage des choix possibles de faction
		GlobalMethods.clearByClass(playerName, "factionChoices");
		// fait la liste de toutes les factions déjà choisies
		Set<String> chosenFactionList = new HashSet<String>();
		for (String player:game.getPlayerList().keySet()){
			if (game.getPlayerList().get(player).getFaction()!=null){
				String factionAdded = game.getPlayerList().get(player).getFaction();
				chosenFactionList.add(factionAdded);
			}
		}
		try {
			URL resources = getClass().getClassLoader().getResource("../../starcraftResources/factionChoices.xml");
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("faction");
			for (int i = 0; i < nList.getLength(); i++){
				Element node = (Element) nList.item(i);
				String factionName = node.getAttribute("name");
				String speciesName = node.getAttribute("species");
				String image = node.getAttribute("image");
				String factionColor = node.getAttribute("color");
				if (!chosenFactionList.contains(factionName)){
					JSONObject factionChoice = new JSONObject()
							.put("action", "printFactionChoice")
							.put("factionName", factionName)
							.put("speciesName", speciesName)
							.put("image", image)
							.put("factionColor", factionColor);
					GlobalMethods.sendPlayerAction(playerName, factionChoice);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**affiche les différentes factions choisies par les joueurs dans la barre indiquant les tours**/
	public void printChosenFactions(String player, StarcraftGame game){
		GlobalMethods.clearByClass(player, "activeFactionChoices");
		for (String playerName : game.getTurnList()){			
			printChosenFactionByPlayer(player, game, playerName);
		}
	}
	
	public void chooseFirstPlayer(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			for (String playerName : game.getTurnList()){			
				displayFirstPlayerChoice(player, game, playerName);
			}
		}
	}
	
	private void displayFirstPlayerChoice(String player, StarcraftGame game, String choosingPlayer){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(choosingPlayer);
		if (starcraftPlayer.getFaction()!=null){
			String factionName = starcraftPlayer.getFaction();
			String speciesName = starcraftPlayer.getSpecies();
			String factionImage = starcraftPlayer.getFactionImage();
			try {
				JSONObject factionChoice = new JSONObject()
						.put("action", "displayFirstPlayerChoice")
						.put("playerName", choosingPlayer)
						.put("factionName", factionName)
						.put("speciesName", speciesName)
						.put("factionImage", factionImage)
						.put("factionColor", starcraftPlayer.getPlayerColor());
				GlobalMethods.sendPlayerAction(player, factionChoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	

	/**affiche une faction choisie par un joueur dans la barre indiquant les tours**/
	public void printChosenFactionByPlayer(String player, StarcraftGame game, String choosingPlayer){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(choosingPlayer);
		if (starcraftPlayer.getFaction()!=null){
			String factionName = starcraftPlayer.getFaction();
			String speciesName = starcraftPlayer.getSpecies();
			String factionImage = starcraftPlayer.getFactionImage();
			try {
				JSONObject factionChoice = new JSONObject()
						.put("action", "playerFactionChoice")
						.put("playerName", choosingPlayer)
						.put("factionName", factionName)
						.put("speciesName", speciesName)
						.put("factionImage", factionImage)
						.put("conquest", starcraftPlayer.getConquestPoint());
				GlobalMethods.sendPlayerAction(player, factionChoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**affiche la partie 'faction' du champs d'information('your game board') sur la situation du joueur**/
	public void printPlayerFactionInfo(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getFaction()!=null){
			String factionName = starcraftPlayer.getFaction();
			String speciesName = starcraftPlayer.getSpecies();
			String factionImage = starcraftPlayer.getFactionImage();
			String factionColor = starcraftPlayer.getPlayerColor();
			try {
				JSONObject factionChoice = new JSONObject()
						.put("action", "playerFactionInfo")
						.put("playerName", player)
						.put("factionName", factionName)
						.put("speciesName", speciesName)
						.put("factionImage", factionImage)
						.put("factionColor", factionColor)
						.put("conquest", starcraftPlayer.getConquestPoint());
				GlobalMethods.sendPlayerAction(player, factionChoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fetchPlayerFactionInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				printPlayerFactionInfo2(playerName, game, player);
			}
		}
	}
	
	private void sendUpdatePlayerFactionInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				printPlayerFactionInfo2(player, game, playerName);
			}
		}
	}
	
	/**fonction complémentaire informant les autres joueurs de ce qu'a fait le joueur sélectionné**/
	private void printPlayerFactionInfo2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getFaction()!=null){
			String factionName = starcraftPlayer.getFaction();
			String speciesName = starcraftPlayer.getSpecies();
			String factionImage = starcraftPlayer.getFactionImage();
			String factionColor = starcraftPlayer.getPlayerColor();
			if (!player.equals(playerName)){
				try {
					JSONObject printPlayerFactionInfo2 = new JSONObject()
							.put("action", "printPlayerFactionInfo2")
							.put("playerName", player)
							.put("factionName", factionName)
							.put("speciesName", speciesName)
							.put("factionImage", factionImage)
							.put("factionColor", factionColor)
							.put("conquest", starcraftPlayer.getConquestPoint());
					GlobalMethods.sendPlayerAction(playerName, printPlayerFactionInfo2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setupPlayerInfoOptions(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!player.equals(playerName)){
				try {
					JSONObject setupPlayerInfoOptions = new JSONObject()
							.put("action", "setupPlayerInfoOptions")
							.put("playerName", playerName);
					GlobalMethods.sendPlayerAction(player, setupPlayerInfoOptions);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**Ajoute à la partie info un indicateur des cartes de combats piochées**/
	public void printCombatCardInfo(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getSpecies()!=null){
			try {
				JSONObject factionChoice = new JSONObject()
						.put("action", "displayCardNumber")
						.put("color", starcraftPlayer.getPlayerColor())
						.put("cardNumber", starcraftPlayer.getCombatCardsInHand().size());
				GlobalMethods.sendPlayerAction(player, factionChoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fetchCombatCardInfo(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				printCombatCardInfo2(playerName, game, player);
			}
		}
	}
	
	private void printCombatCardInfo2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getSpecies()!=null){
			if (!player.equals(playerName)){
				try {
					JSONObject displayCardNumber = new JSONObject()
							.put("action", "displayCardNumber2")
							.put("playerName", player)
							.put("color", starcraftPlayer.getPlayerColor())
							.put("cardNumber", starcraftPlayer.getCombatCardsInHand().size());
					GlobalMethods.sendPlayerAction(playerName, displayCardNumber);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**Ajoute à la partie info un indicateur des cartes de combats actives**/
	public void displayActiveCardNumber(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getSpecies()!=null){
			try {
				JSONObject displayActiveCardNumber = new JSONObject()
						.put("action", "displayActiveCardNumber")
						.put("cardNumber", starcraftPlayer.getGlobalCards().size());
				GlobalMethods.sendPlayerAction(player, displayActiveCardNumber);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fetchActiveCardNumber(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayActiveCardNumber2(playerName, game, player);
			}
		}
	}
	
	public void sendUpdateActiveCardNumber(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayActiveCardNumber2(player, game, playerName);
			}
		}
	}
	
	/**Ajoute à la partie info un indicateur des cartes de combats actives**/
	private void displayActiveCardNumber2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getSpecies()!=null){
			try {
				JSONObject factionChoice = new JSONObject()
						.put("action", "displayActiveCardNumber2")
						.put("playerName", player)
						.put("cardNumber", starcraftPlayer.getGlobalCards().size());
				GlobalMethods.sendPlayerAction(playerName, factionChoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**Réception des actions possibles du joueur dans l'étape de sélection des factions**/
	
	/**le joueur choisis la faction**/
	public void choosePlayerFaction(String playerName, String speciesName, String factionName, StarcraftGame game){
		StarcraftPlayer activeplayer= game.getPlayer(playerName);
		if (activeplayer.getFaction() == null){
			activeplayer.setSpecies(speciesName);
			activeplayer.setFaction(factionName);
			//trouve l'image correspondant à la faction
			String image = "";
			String playerColor = "";
			try {
				URL resources = getClass().getClassLoader().getResource("../../starcraftResources/factionChoices.xml");
		    	File fXmlFile = new File(resources.toURI());
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("faction");
				int i = 0;
				while (i < nList.getLength() && image.equals("")){
					Element node = (Element) nList.item(i);
					String nodeFactionName = node.getAttribute("name");
					if (factionName.equals(nodeFactionName)){
						image = node.getAttribute("image");
						playerColor = node.getAttribute("color");
					}else{
						i++;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			activeplayer.setFactionImage(image);
			activeplayer.setPlayerColor(playerColor);
			activeplayer.initializeCombatCardDeck(game.cardIdGenerator);
			activeplayer.initializeBuyableModules(game.cardIdGenerator);
			//les actions du joueurs actifs sont terminées, on passe donc au tout suivant
			game.nextTurn();
			
			// on affiche la faction choisie par le joueur
			printPlayerFactionInfo(playerName, game);
			sendUpdatePlayerFactionInfo(playerName, game);
			printCombatCardInfo(playerName, game);
			showUnlockedUnitsInfo(playerName, game);
			// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
			for (String player : game.getPlayerList().keySet()){
				if (game.getCurrentTurnNumber() == 0){
					//enlève l'ancien affichage des choix possibles de faction
					GlobalMethods.clearByClass(player, "factionChoices");
				}else{
					//on enlève la faction choisie des choix possibles
					//(les noms des attributs sont en minuscules car ils sont en minuscules dans le html)
					GlobalMethods.deleteElement(player, "//div[@class = 'factionChoices' "
							+ "and @speciesname='"+ speciesName +"' and"
							+ " @factionname='"+ factionName +"']");
				}
				GameTurnHandler.printNextTurnScreen(player, game);
			}
		}
	}


	public void displayCombatCards(String playerName, StarcraftGame game) {
		StarcraftPlayer activeplayer= game.getPlayer(playerName);
		for (int combatCardId:activeplayer.getCombatCardsInHand().keySet()){
			JSONObject combatCardJS = activeplayer.getCombatCardsInHand().get(combatCardId).getCardJS("displayCardInHand");
			GlobalMethods.sendPlayerAction(playerName, combatCardJS);
		}
	}

	public void displayActiveCards(String playerName, StarcraftGame game) {
		StarcraftPlayer activeplayer= game.getPlayer(playerName);
		for (String combatCardName:activeplayer.getGlobalCards().keySet()){
			JSONObject combatCardJS = activeplayer.getGlobalCards().get(combatCardName).getCardJS("displayCardInHand");
			GlobalMethods.sendPlayerAction(playerName, combatCardJS);
		}
	}

	public void displayActiveCards2(String playerName, String cardOwner, StarcraftGame game) {
		StarcraftPlayer activeplayer= game.getPlayer(cardOwner);
		for (String combatCardName:activeplayer.getGlobalCards().keySet()){
			JSONObject combatCardJS = activeplayer.getGlobalCards().get(combatCardName).getCardJS("displayCardInHand");
			GlobalMethods.sendPlayerAction(playerName, combatCardJS);
		}
	}

	public void sendFirstPlayerChoice(String playerName, String firstPlayer, StarcraftGame game) {
		game.changeFirstPlayer(firstPlayer);
		GlobalMethods.clearByClass(playerName, "factionChoices");
		game.nextTurn();
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

}
