package gameEntities.methods;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.playerItems.CombatCard;

public class HandleResearchCards {
	
	private String cardName;
	
	/**fonction demandant au navigateur le bonus choisi par le joueur**/
	public void askResearchBonus(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getActiveSpecialOrderBonus() || starcraftPlayer.getTempBonusList().contains("buyBonusResearch")){
			try {
				JSONObject askResearchBonus = new JSONObject()
						.put("action", "askResearchBonus");
				GlobalMethods.sendPlayerAction(player, askResearchBonus);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void displayResearchBonus(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getUnitBuildLimit() > starcraftPlayer.getUnitBuilt()){
				if (starcraftPlayer.getActiveSpecialOrderBonus()){
					try {
						JSONObject displayResearchBonus = new JSONObject()
								.put("action", "displayResearchBonus");
						GlobalMethods.sendPlayerAction(player, displayResearchBonus);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}else if (starcraftPlayer.getTempBonusList().contains("buyBonusResearch")){
					try {
						JSONObject displayResearchBonus = new JSONObject()
								.put("action", "displayResearchBonus2");
						GlobalMethods.sendPlayerAction(player, displayResearchBonus);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				
			}
		}
	}
	
	public void activateBuyableCards(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getBuyingOrder() == null){
				if (starcraftPlayer.getUnitBuildLimit() > starcraftPlayer.getUnitBuilt()){
					try {
						JSONObject activateBuyableCards = new JSONObject()
								.put("action", "activateBuyableCards");
						GlobalMethods.sendPlayerAction(player, activateBuyableCards);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public void displayBuyableCards(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getUnitBuildLimit() > starcraftPlayer.getUnitBuilt()){
				try {
					JSONArray cardListJS = new JSONArray();
					for (String cardName:starcraftPlayer.getBuyableCards().keySet()){
						JSONObject cardJS = starcraftPlayer.getBuyableCards().get(cardName).get(0).getCardJS("");
						cardListJS.put(cardJS);
					}
					JSONObject displayBuyableCards = new JSONObject()
							.put("action", "displayBuyableCards")
							.put("cardList", cardListJS);
					if (starcraftPlayer.getBuyableCards().size() > 0){
						GlobalMethods.sendPlayerAction(player, displayBuyableCards);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**montre les cartes rechargeables**/
	public void displayCardsToRecharge(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				ArrayList<CombatCard> cardsToRecharge = game.getGalaxy().getStarcraftBattle().getAllValidRechargeCards(player);
				JSONArray cardListJS = new JSONArray();
				for (CombatCard card:cardsToRecharge){
					JSONObject cardJS = card.getCardJS("");
					cardListJS.put(cardJS);
				}
				JSONObject displayCardsToRecharge = new JSONObject()
						.put("action", "displayCardsToRecharge")
						.put("cardList", cardListJS);
				if (cardsToRecharge.size() > 0){
					GlobalMethods.sendPlayerAction(player, displayCardsToRecharge);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public void rechargeCard(String playerName, int cardId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		CombatCard cardToRecharge = null;
		for (CombatCard card:starcraftPlayer.getDismissedCombatCards()){
			if (card.getId() == cardId){
				starcraftPlayer.getCombatCardsInHand().put(cardId, card);
				cardToRecharge = card;
				break;
			}
		}
		
		starcraftPlayer.getDismissedCombatCards().remove(cardToRecharge);
		GameTurnHandler.callUpdateCardNumber(starcraftPlayer);
		starcraftPlayer.setHasRecharge(false);
	}

	public void applyResearchBonus(String playerName, String bonusName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		if (bonusName.equals("eventCardBonus")){
			starcraftPlayer.getEventCards().drawEventCard(game);
			starcraftPlayer.buyAndDrawCombatCard(this.cardName, true);
		}else if (bonusName.equals("combatCardBonus")){
			starcraftPlayer.buyAndDrawCombatCard(this.cardName, false);
		}else if (bonusName.equals("noBonus")){
			starcraftPlayer.buyAndDrawCombatCard(this.cardName, true);
		}
		this.cardName = null;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
}
