package gameEntities.methods;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.websocket.Session;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import gameEntities.StarcraftGame;

public class GlobalMethods {
	private static final Map<String, Session> playerSessions = Collections.synchronizedMap(new HashMap<String, Session>());
	
	public static Map<String, Session> getPlayerSessions(){
		return playerSessions;
	}
	
	public static void addPlayerSession(String playerName, Session session){
		playerSessions.put(playerName, session);
	}
	
	public static void removePlayerSession(String playerName){
		playerSessions.remove(playerName);
	}
	
	public static void surrenderStarcraftGame(String player, StarcraftGame game){
		game.surrenderStarcraftGame(player);
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			game.nextTurn();
		}
		for (String playerName : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(playerName, game);
		}
	}
	
	/** Fonction envoyant à l'interface Web les informations nécessaires pour
	 * déterminer le joueur actif **/
	public static void printPlayerTurn(String player, StarcraftGame game){
		String isCurrentPlayerTurn = "";
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			isCurrentPlayerTurn= "true";
		}else{
			isCurrentPlayerTurn= "false";
		}
		JSONObject startGame;
		try {
			startGame = new JSONObject()
					.put("action", "playerTurnUpdate")
					.put("isCurrentPlayerTurn", isCurrentPlayerTurn)
					.put("currentPlayer", game.getPlayerCurrentlyPlaying());
			sendPlayerAction(player, startGame);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static void addEndNormalTurnButton(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				JSONObject addEndNormalTurnButton = new JSONObject()
						.put("action", "addEndNormalTurnButton");
				sendPlayerAction(player, addEndNormalTurnButton);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Fonction envoyant à l'interface Web les informations nécessaires pour
	 * déterminer le joueur actif **/
	public static void sendHelpMessage(String player, StarcraftGame game){
		String xPathRequest = "/root/turn[@name =\"" + game.getTurnPart() + "\"]/";
		String prefix = "";
		String color = "#F0F8FF";
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			xPathRequest += "active";
			color = "#00E500";
		}else{
			xPathRequest += "alternative";
			prefix = game.getPlayerCurrentlyPlaying();
		}
		URL resources = GlobalMethods.class.getClassLoader().getResource("../../starcraftResources/helpMessages.xml");
		try {
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile(xPathRequest);
			Object o = expr.evaluate(doc, XPathConstants.NODE);
			if (o != null){
				Node selectedNode = (Node) o;
				String message = prefix + selectedNode.getTextContent();
				message = message.replace("\\n", "<br/>");
				JSONObject sendHelpMessage = new JSONObject()
						.put("action", "sendHelpMessage")
						.put("message", message)
						.put("color", color);
				sendPlayerAction(player, sendHelpMessage);
			}else{
				JSONObject sendHelpMessage = new JSONObject()
						.put("action", "sendHelpMessage")
						.put("message", "")
						.put("color", color);
				sendPlayerAction(player, sendHelpMessage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void clearByClass(String player, String className){
		JSONObject deleteClassView;
		try {
			deleteClassView = new JSONObject()
					.put("action", "clearViewByClass")
					.put("name", className);
			sendPlayerAction(player, deleteClassView);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static void clearObjectEvents(String player){
		JSONObject clearObjectEvents;
		try {
			clearObjectEvents = new JSONObject()
					.put("action", "clearObjectEvents");
			sendPlayerAction(player, clearObjectEvents);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void startTimeMeasure(String player){
		JSONObject startTimeMeasure;
		try {
			startTimeMeasure = new JSONObject()
					.put("action", "startTimeMeasure");
			sendPlayerAction(player, startTimeMeasure);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void endTimeMeasure(String player){
		JSONObject endTimeMeasure;
		try {
			endTimeMeasure = new JSONObject()
					.put("action", "endTimeMeasure");
			sendPlayerAction(player, endTimeMeasure);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static void clearActionStage(String player){
		JSONObject clearActionStage;
		try {
			clearActionStage = new JSONObject()
					.put("action", "clearActionStage");
			sendPlayerAction(player, clearActionStage);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void deleteElement(String player, String xpath){
		try {
			JSONObject deleteElement = new JSONObject()
					.put("action", "deleteElement")
					.put("xpath", xpath);
			sendPlayerAction(player, deleteElement);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void printGameBoard(String player){
		try {
			JSONObject updateBoardView = new JSONObject()
					.put("action", "updateBoardView");
			sendPlayerAction(player, updateBoardView);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static void upgradeActionMenu(String player){	
		try {
			JSONObject addCanvas = new JSONObject()
					.put("action", "upgradeActionMenu");
			sendPlayerAction(player, addCanvas);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}

	public static void sendPlayerAction(String player, JSONObject object){
		Session session = playerSessions.get(player);
		if (session!=null){
			try {
				session.getBasicRemote().sendText(object.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Boolean hasConnectedPlayers(StarcraftGame game){
		Boolean result = false;
		if (game != null){
			for (String playerName:game.getPlayerList().keySet()){
				if (playerSessions.containsKey(playerName)){
					result = true;
					break;
				}
			}
		}
		return result;
	}
	
	public static void skipTurn(String playerName, StarcraftGame game) {
		game.getPlayer(playerName).getEventCards().applyBonuses(game);
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}
	
}
